

# Развернуть локально
```

docker-compose up -d
docker exec -it php /bin/sh -c "cp .env.example .env"
docker exec -it php /bin/sh -c "composer install"
docker exec -it php /bin/sh -c "npm cache verify && npm install"
docker exec -it php /bin/sh -c "php artisan config:cache"
docker exec -it php /bin/sh -c "php artisan key:generate"
docker exec -it php /bin/sh -c "php artisan migrate && php artisan db:seed"
```

# Основные Сценарии 

*COMPANY #1*
- регистрация
- добавить сотрудника 
- share employee - /car/create + button
	a) 
	get offer from hh - change status ???
	approve - /accept/booking_id
	show contact company - show modal ???
	b) 
	get close offer - show modal ???
	get education link - show modal ???


*COMPANY #2*
- получить письмо на почту
- перейти по ссылке
- увидеть профиль сотрудника на вакансию
- подтвердить интерес
- получить контакт компании 1


# Entity

*cars* = сотрудник
*car_brand* = профессия/должность
*car_type* = индустрия/специализация
*car_color* = образование
*city* = город в котором сотрудник работает
*feature_name* = навыки
*car_description* = записка от работодателя

*car_data* = certificates
*car_unlock_type* = direct/through
*extra* = extra_skills_features = comp/priner
*extra_time* = time period
*cars_images* = portfolio
*fuel_type* = part_time/full_time
*car_model* = junior/senior(Qualification Levels)


# Нерешенные Entity

*car_transmission* = задний/передний
*car_gear* = experience - механика/автомат
*car_features* = ?
*car_bans* ?
*mileage_inclusive_cars* - need for checkout

# Structure of project

- database/migration - миграции таблиц
- database/seeds - данные в таблицы
- resourses/views - фронт php шаблоны
- resourses/assets/js - фронт vue шаблоны
- resourses/lang - переводы 
- routes - эндпоинты(основные лежат в api.php)
- app/models - сущности
- app/http/controllers - логика
	- api/auth - авторизация
	- CatalogController
	- CarCardController
	- PublicProfileController
	- AddCarController
	- web/HomePageController - лэндинг

# Эндпоинты

*/car/create* - создать сотрудника
*carslist/{search?}* - 
*carcard/{id}*
*car/models*
*catalog*
*filter*
*/car/filter_rent*

*mycars*
*mycars/id - GetMyCarsController@getMyCar*
*my-car/booking/{booking_id}*
*mycar/id - EditMyCarController@index*

*trips*
*trips/id*
*accept/{booking_id}*
*cancel-transfer/{booking_id}*
*pass-car/{booking_id}*
*payment/{booking_id}*

*getMyCarLocation/{id}*
*confirm/{id}*
*confirm-payment/{id}*
*isActive*

# Задачи

- deploy 
- show education on profile
- show list of cv
- show badge on car profile
- change skills table?
- filter show skills

replace

show cars landing HomePageController

replace s3 move to localstorage(save default image while save car)

courses 
extra - rent extra
cars_feature/feature_name - skills pivot setup model


when create car
- MileageInclusiveCars
- CarsBans

booking
- all extra tables

payment
- all extra tables



# extra
- get domain https://www.startupweekend.neustar/
- get hosting google cloud