<?php

namespace App\Http\Controllers\Common;

use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;
use App\Services\PersonalImagesService;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\User;
use Validator;
use App\Models\Token;
use App\Models\PersonalUser;
use Illuminate\Database\QueryException;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\Hash;
use Propaganistas\LaravelPhone\PhoneNumber;

/**
 * Контроллер работы с пользователями
 */
class UserController extends Controller
{

    /**
     * Сохранение файлов персональных атрибутов юзера
     * @param  User         $user     юзер, чьи атрибуты меняем
     * @param  PersonalUser $personal модель атрибутов keys
     * @param  Collection   $files    кол-ция файлов [key => val]
     * @return Collection
     */
    protected function savePasport(User $user, PersonalUser $personal, Collection $files): Collection
    {
        $personal_service = new PersonalImagesService($user);

        collect(
            $personal->only(
                $files->keys()->all()
        ))->mapWithKeys(
            function ($item, $key) use ($user) {

                return [
                    $key => md5(sprintf(
                        "%s%s%s",
                        $key,
                        $user->id,
                        $user->email
                    ))
                ];
            }
        )->each(function ($value, $key) use ($personal, $personal_service, $files) {
            if ($personal_service->save($value, $files->get($key))) {
                $personal->{$key} = $value;
            }
        });

        $result = collect(['result' => false]);
        try {
            $result->put(
                'result',
                $personal->save()
            );
        } catch (QueryException $e) {
            $result->put('code', "1");
        } finally {

            return $result;
        }
    }

    public function setPasport(Request $request)
    {
        $files = $request->only(['passport_face', 'passport_visa']);

        $validator = Validator::make(
            ['fiels' => $files],
            [
                'fiels.passport_face' => 'file|mimes:jpeg,bmp,png',
                'fiels.passport_visa' => 'file|mimes:jpeg,bmp,png',
                'fiels' => 'min:1'
            ]
        );

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = Auth::user();

        $personal = $user->personalUser ?:
            $user->personalUser()->create();

        $user->personalUser()->associate($personal);
        $user->save();

        return response()->json(
            $this->savePasport($user, $personal, collect($files))
        );
    }

    public function setDrivingLicence(Request $request)
    {
        $files = $request->only(['driver_lisense_first', 'driver_license_second']);
        $validator = Validator::make(
            ['fiels' => $files],
            [
                'fiels.driver_lisense_first' => 'file|mimes:jpeg,bmp,png',
                'fiels.driver_license_second' => 'file|mimes:jpeg,bmp,png',
                'fiels' => 'min:1'
            ]
        );

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = Auth::user();

        $personal = $user->personalUser ?:
            $user->personalUser()->create();

        $user->personalUser()->associate($personal);

        return response()->json(
            $this->savePasport($user, $personal, collect($files))
        );
    }

    public function updateSelfie(Request $request, Authenticatable $user)
    {
        $files = $request->only(['selfie']);
        $validator = Validator::make(
            ['fiels' => $files],
            [
                'fiels.selfie' => 'file|mimes:jpeg,bmp,png',
                'fiels' => 'min:1'
            ]
        );

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $personal = $user->personalUser ?:
            $user->personalUser()->create();

        $user->personalUser()->associate($personal);

        return response()->json(
            $this->savePasport($user, $personal, collect($files))
        );
    }

    public function sendSms(Request $request, Authenticatable $user)
    {
        $validator = Validator::make(
            $request->only(['phone']),
            [
                'phone' => 'required|phone:RU'
            ]
        );

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $token = Token::create([
            'phone' => PhoneNumber::make($request->get('phone'), 'RU')->formatE164(),
            'user_id' => $user->id
        ]);

        return response()->json(['result' => true]);
    }
}
