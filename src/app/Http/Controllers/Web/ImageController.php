<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Common\ImageController as Controller;
use App\Models\{Car, CarImage};
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Validator;

/**
 * Контроллер работы с фото авто
 */
class ImageController extends Controller
{

     /**
     * Добавляем фото документов
     *
     * @param  Request $request запрос
     * @return JsonResponse     сформированный ответ
     */
    public function addPhotoDocsCar(Request $request, int $car_id)
    {
        $car = Auth::user()->cars()->findOrFail($car_id);
        $type = $request->id;

        $validate = Validator::make(
            array_merge(
                ['CarId' => $car_id],
                $request->all()
            ),
            self::getValidateRolesCarImage()
        );

        if ($validate->fails()) {
            return response()->json($validate->errors(), 500);
        }

        return response()->json($this->savePtsImage($car, $request, $type));
    }

    public function osagoImage(Request $request, int $car_id)
    {
        $car = Auth::user()->cars()->findOrFail($car_id);
        $type = $request->id;

        $validate = Validator::make(
            array_merge(
                ['CarId' => $car_id],
                $request->all()
            ),
            self::getValidateRolesCarImage()
        );

        if ($validate->fails()) {
            return response()->json($validate->errors(), 500);
        }

        return response()->json($this->saveOsagoKaskoImage($car, $request, 'osago_image'));
    }

    public function kaskoImage(Request $request, int $car_id)
    {
        $car = Auth::user()->cars()->findOrFail($car_id);
        $type = $request->id;

        $validate = Validator::make(
            array_merge(
                ['CarId' => $car_id],
                $request->all()
            ),
            self::getValidateRolesCarImage()
        );

        if ($validate->fails()) {
            return response()->json($validate->errors(), 500);
        }

        return response()->json($this->saveOsagoKaskoImage($car, $request, 'kasko_image'));
    }

    

    /**
     * Добавляем фото авто
     *
     * @param  Request $request запрос
     * @return JsonResponse     сформированный ответ
     */
    public function addPhotoCar(Request $request, int $car_id)
    {
        // $car = Auth::user()->cars()->findOrFail($car_id);
        $car = Car::find($car_id);

        $validate = Validator::make(
            array_merge(
                ['CarId' => $car_id],
                $request->all()
            ),
            self::getValidateRolesCarImage()
        );

        if ($validate->fails()) {
            return response()->json($validate->errors(), 500);
        }


        return response()->json($this->saveCarImage($car, $request));
    }

    /**
     * Сохраняем файл из запроса
     *
     * @param  Car        $car     Модель авто
     * @param  Request    $request запрос
     * @return Collection          результат сохранения
     */

    protected function saveCarImage(Car $car, Request $request)
    { 
        $hash = static::generatePhoto($car, Auth::user());

        Storage::disk('hot')->put(
            $hash,
            $request->file('photo')->get(),
            array('ACL'=>'public-read')
        );
        
        $result = collect(['result' => $car]);
        // проверяем что файл действительно есть на шаре
        if (Storage::disk('hot')->exists($hash)) {
            
            $id = CarImage::insertGetId(
                ['name' => $hash, 'car_id' => $car->id, 'created_at' => now(), 'updated_at' => now()]
            );

            $result->put(
                'result',
                $car->save()
            )->put(
                'carinc',
                $id
            )->put(
                'carId',
                $request->input('car_id')
            );

            return $result;
        } else {
            $result->put('code', "2")->put('test', $query);
            return $result;
        }

        // } catch (QueryException $e) {
        //     $result->put('code', "1")->put('error');
        // } finally {
        //     return $result;
        // }
    
    }

    /**
     * Добавляем фото паспорта лицевая часть
     *
     * @param  Request $request запрос
     * @return JsonResponse     сформированный ответ
     */
    public function addPhotoPassportFace(Request $request, int $car_id)
    {
        $car = Auth::user()->cars()->findOrFail($car_id);
        

        $validate = Validator::make(
            array_merge(
                ['CarId' => $car_id],
                $request->all()
            ),
            self::getValidateRolesCarImage()
        );

        if ($validate->fails()) {
            return response()->json($validate->errors(), 500);
        }

        return response()->json($this->savePassport($car, $request, 'passport_face'));
    }

    /**
     * Добавляем фото паспорта страница прописки
     *
     * @param  Request $request запрос
     * @return JsonResponse     сформированный ответ
     */
    public function addPhotoPassportRegistry(Request $request, int $car_id)
    {
        $car = Auth::user()->cars()->findOrFail($car_id);

        $validate = Validator::make(
            array_merge(
                ['CarId' => $car_id],
                $request->all()
            ),
            self::getValidateRolesCarImage()
        );

        if ($validate->fails()) {
            return response()->json($validate->errors(), 500);
        }

        return response()->json($this->savePassport($car, $request, 'passport_visa'));
    }

    /**
     * Добавляем фото паспорта страница прописки
     *
     * @param  Request $request запрос
     * @return JsonResponse     сформированный ответ
     */
    public function addPhotoSelfie(Request $request, int $car_id)
    {
        //$car = Auth::user()->cars()->findOrFail($car_id);
        $car = Car::find($car_id);
        $validate = Validator::make(
            array_merge(
                ['CarId' => $car_id],
                $request->all()
            ),
            self::getValidateRolesCarImage()
        );

        if ($validate->fails()) {
            return response()->json($validate->errors(), 500);
        }

        return response()->json($this->savePassport($car, $request, 'selfie'));
    }

}
