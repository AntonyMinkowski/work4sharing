<?php

namespace App\Http\Controllers\Web\UserProfile;

use App\Http\Controllers\Controller;
use App\Models\{Car,
    CarBans,
    CarImage,
    CarsBans,
    CarsColor,
    CarsDeposit,
    CarsDescriptions,
    CarsGear,
    CarType,
    CarUnlockType,
    City,
    DeliveryCars,
    Extra,
    ExtraCars,
    ExtraTime,    
    Course,
    CourseCars,
    CourseTime,
    MileageInclusive,
    MileageInclusiveCars,
    ModelsCar,
    RentPlace,
    RentTime,
    RentTimeCars};
use App\User;
use Helpers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Stevebauman\Location\Exceptions\DriverDoesNotExistException;
use Stevebauman\Location\Location;
use Validator;

class EditMyCarController extends Controller
{
    public $locationGetter;

    public function __construct()
    {
        try {
            $this->locationGetter = new Location();
        } catch (DriverDoesNotExistException $e) {
        }
    }

    public function index(Request $request, $id)
    {

        $car = Car::getCarToUserOrStaff($id);

        //dd(RentTimeCars::all());
        $user = User::find(Auth::id());
        $unlock_type = CarUnlockType::all();
        $images = CarImage::where('car_id', $id)->get();
        $rent_place = RentPlace::all();
        // $delivery_cars = DeliveryCars::where('car_id', $id)->get();
        $mileage_inclusive = MileageInclusive::all();
        $rent_time = RentTime::all();
        $car_type_car = CarType::where('id', $car->car_type)->get();
        $car_type = CarType::all();
        $city = City::orderBy('name', 'desc')->get();
        $extra = Extra::all();
        $extra_cars = ExtraCars::where('cars_id', $id)->get();
        $extra_time = ExtraTime::all();        
        $course = Course::all();
        $course_cars = CourseCars::where('cars_id', $id)->get();
        $course_time = CourseTime::all();
        $car_bans = CarBans::all();
        // $cars_bans = CarsBans::where('cars_id', $id)->get();
        $gear = CarsGear::all();
        $colors = CarsColor::all();
        $location = $this->locationGetter->get();
        $lng = $car->lng ?? $location->longitude;
        $lat = $car->lat ?? $location->latitude;
        // $emptyAddress = false;
        // if (!$car->lng or !$car->lat) {
        //     $emptyAddress = true;
        // }
        // $osagoImage = null;
        // $kaskoImage = null;
        // if ($car->CarsData['osago_image']) {
        //     $osagoImage = Storage::disk('ice')->temporaryUrl($car->CarsData['osago_image'], now()->addMinutes(5));
        // }

        // if ($car->CarsData['kasko_image']) {
        //     $kaskoImage = Storage::disk('ice')->temporaryUrl($car->CarsData['kasko_image'], now()->addMinutes(5));
        // }

        return view('user.profile.edit-my-car', [
            'car' => $car,
            'images' => $images,
            'rent_place' => $rent_place,
            // 'delivery_cars' => $delivery_cars,
            'mileageInclusive' => $mileage_inclusive,
            'rentTime' => $rent_time,
            'carType' => $car_type,
            'carTypeCar' => $car_type_car,
            'city' => $city,
            'extra' => $extra,
            'extra_cars' => $extra_cars,
            'extra_time' => $extra_time,            
            'course' => $course,
            'course_cars' => $course_cars,
            'course_time' => $course_time,
            'car_bans' => $car_bans,
            // 'cars_bans' => $cars_bans,
            'unlock_type' => $unlock_type,
            'gear' => $gear,
            'colors' => $colors,
            'user' => $user,
            // 'personal_user' => $user->personalUser,
            'lng' => $lng,
            'lat' => $lat,
            // 'empty' => $emptyAddress,
            // 'osagoImage' => $osagoImage,
            // 'kaskoImage' => $kaskoImage
        ]);
    }

    public function description(Request $request)
    {
        $validate = Validator::make(
            $request->all(),
            [
                'cars_id' => 'required',
                'description' => 'required|string',
            ]
        );

        if ($validate->fails()) {
            return response()->json($validate->errors(), 500);
        }

        $car = Car::find($request->cars_id);

        if ($car->description_id == null) {
           $id = CarsDescriptions::create($request->except('_token'));
        } else{
            CarsDescriptions::where('id', $car->description_id)->update([
                'description' => $request->description
            ]);
            $id = CarsDescriptions::find($car->description_id);
        }

        $car->update(['description_id' => $id->id]);
        Helpers::writeLog($car->id, 'cars', Auth::user()->name . " обновил описание");
        return $id;
    }

    public function editCostDay(Request $request, $car_id)
    {
        Helpers::writeLog($car_id, 'cars', Auth::user()->name . " обновил цену");
        
        $overmileFee = 0;
        // if ($request->cost_day < 750000) {
        //     $overmileFee = 
        // } else if ($request->cost_day < 750000 && ) {
        //     $overmileFee = 
        // } else if () {
        //     $overmileFee = 
        // } else if () {
        //     $overmileFee = 
        // } else if () {
        //     $overmileFee = 
        // } else if () {
        //     $overmileFee = 
        // } else if () {
        //     $overmileFee = 
        // } else if () {
        //     $overmileFee = 
        // } else if () {
        //     $overmileFee = 
        // }

        return Car::where('id', $car_id)
            ->update([
                'cost_day' => $request->cost_day
            ]);    
    }

    public function editCarType(Request $request, $car_id)
    {
        Helpers::writeLog($car_id, 'cars', Auth::user()->name . " обновил цену");
        return Car::where('id', $car_id)
            ->update([
                'car_type' => $request->cartype
            ]);    
    }

    public function editSeats(Request $request, $car_id)
    {
        Helpers::writeLog($car_id, 'cars', Auth::user()->name . " обновил кол пассажиров");
        return Car::where('id', $car_id)
            ->update([
                'seat' => $request->seat
            ]);
    }

    public function editPower(Request $request, $car_id)
    {
        Helpers::writeLog($car_id, 'cars', Auth::user()->name . " обновил информацию об мощности двигателя");
        return Car::where('id', $car_id)
            ->update([
                'power' => $request->power
            ]);
    }

    public function editCity(Request $request, $car_id)
    {
        Helpers::writeLog($car_id, 'cars', Auth::user()->name . " обновил информацию об стоимости автомобиля");
        return Car::where('id', $car_id)
            ->update([
                'city' => $request->city
            ]);
    }

    public function editAssessedValue(Request $request, $car_id)
    {
        Helpers::writeLog($car_id, 'cars', Auth::user()->name . " обновил информацию об стоимости автомобиля");
        return Car::where('id', $car_id)
            ->update([
                'assessed_value' => $request->assessed_value
            ]);
    }

    public function editVolume(Request $request, $car_id)
    {
        Helpers::writeLog($car_id, 'cars', Auth::user()->name . " обновил информацию об объеме двигателя");
        return Car::where('id', $car_id)
            ->update([
                'engine_volume' => $request->volume
            ]);
    }

    public function editMileageInclusive(Request $request, $car_id)
    {
        $car = Car::getCarToUserOrStaff($car_id);
        MileageInclusiveCars::where('id', $car->mileageInclusive['id'])
            ->update($request->except('_token'));
        Helpers::writeLog($car->id, 'cars', Auth::user()->name . " включенный обновил пробег");
    }

    public function editRentTime(Request $request, $car_id)
    {
        $car = Car::getCarToUserOrStaff($car_id);

        /* @var $carRentTime \App\Models\RentTimeCars */
        $carRentTime = RentTimeCars::where('id', $car->rentTime['id'])->first();

        // Если не существует минимальное и максимальное время сдачи для машины
        // То создаем новый объект
        if (!$carRentTime) {
            $carRentTime = new RentTimeCars(
                [
                    'car_id' => $car_id,
                    'min_rent_time' => $request->min_rent_time * 86400,
                    'max_rent_time' => $request->max_rent_time * 86400,
                ]
            );
            $carRentTime->save();
        } else { // Если есть обновляем
            $carRentTime->update([
                'min_rent_time' => $request->min_rent_time * 86400,
                'max_rent_time' => $request->max_rent_time * 86400,
            ]);

        }
        Helpers::writeLog($car->id, 'cars', Auth::user()->name . " обновил минимальное и максимальное время аренды");


    }

    public function editGear(Request $request, $car_id)
    {
        $car = Car::getCarToUserOrStaff($car_id);
        Helpers::writeLog($car->id, 'cars', Auth::user()->name . " обновил информацию о КПП");
        $car->update(['gear_id' => $request->gear_id]);
    }

    public function editCarModel(Request $request, $car_id)
    {
        $car = Car::getCarToUserOrStaff($car_id);
        Helpers::writeLog($car->id, 'cars', Auth::user()->name . " обновил модель");
        /* @var $model ModelsCar */
        $model = ModelsCar::findOrFail($request->model_id);
        if ($model->is_other and $request->model_name) {
            $car->update(['model_name' => $request->model_name]);
        } else {
            $car->update(['model_name' => '']);
        }
        $car->update(['model_id' => $request->model_id]);
    }

    public function editCarBrand(Request $request, $car_id)
    {
        $car = Car::getCarToUserOrStaff($car_id);
        Helpers::writeLog($car->id, 'cars', Auth::user()->name . " обновил брэнд");
        $car->update(['brand_id' => $request->brand_id]);
    }

    public function editTransmission(Request $request, $car_id)
    {
        $car = Car::getCarToUserOrStaff($car_id);
        Helpers::writeLog($car->id, 'cars_transmissions', Auth::user()->name . " обновил информацию о приводе");
        $car->update(['transmission_id' => $request->transmission_id]);
        return 200;
    }

    public function editCoordinates(Request $request, $car_id)
    {
        $car = Car::getCarToUserOrStaff($car_id);
        Helpers::writeLog($car->id, 'cars_transmissions', Auth::user()->name . " обновил информацию о местоположении");
        $car->update(['lat' => $request->lat, 'lng' => $request->lng]);
        return 200;
    }

    public function editFuelType(Request $request, $car_id)
    {
        $car = Car::getCarToUserOrStaff($car_id);
        Helpers::writeLog($car->id, 'fuel_types', Auth::user()->name . " обновил информацию о топливе машины");
        $car->update(['fuel_type_id' => $request->fuel_type_id]);
        $car->fuel_type_id = $request->fuel_type_id;
        $car->save();
        return 200;
    }

    public function editDeposit(Request $request, $car_id)
    {
        $car = Car::getCarToUserOrStaff($car_id);
        Helpers::writeLog($car->id, 'cars_deposits', Auth::user()->name . " обновил информацию о депозите");
        /* @var $carsDeposit \App\Models\CarsDeposit */
        $carsDeposit = $car->carsDeposit;
        if (!$carsDeposit) {
            $carsDeposit = new CarsDeposit([
                'car_id' => $car_id,
                'price' => $request->price
            ]);
            $carsDeposit->save();
        } else {
            $carsDeposit->update([
                'price' => $request->price
            ]);
        }

    }

    public function deleteImage(Request $request)
    {
        Helpers::writeLog($request->image_id, 'car_images', Auth::user()->name . " удалил изображение");
        CarImage::where('id', $request->image_id)->delete();
    }

    public function setHeadImageCar (Request $request) {
        //Получаем список всех изображений
        $images = CarImage::where('car_id', $request->car_id)->get();
        //Проходимся цыклом и записываем всем значение avatar 0
        $reuslt = 0;
        foreach ($images as $image=>$value) {
            $value->avatar = 0;
            $reuslt = $request->image_id;
            if ($value->id == $request->image_id) {
                $value->avatar = 1;
                $reuslt = 1;
            }
            $value->save();
        }
        Helpers::writeLog($request->image_id, 'car_images', Auth::user()->name . " обновил главное изображение изображение");
        return $reuslt;
        //Задаем значение 1 тому изображению который мы получили $request
        // return CarImage::where('id', $request->image_id)->update(['avatar' => 1]);
    }

    public function editConditionDelivery(Request $request)
    {

        $validate = Validator::make(
            $request->all(),
            [
                'cost' => 'required',
                'place' => 'required',
            ]
        );

        if ($validate->fails()) {
            return response()->json($validate->errors(), 500);
        }


        if ($request->data_id == 0) {
            $element = DeliveryCars::create($request->toArray());
        } else{
            $element = DeliveryCars::where('id', $request->data_id)->update([
                'cost' => $request->cost,
                'place' => $request->place
            ]);
            $element = DeliveryCars::find($request->data_id);
        }
        $place = RentPlace::find($element->place)->name;
        $collect = collect([
            'place' => $place, 
            'cost' => $element->cost,
            'id' => $element->id
        ]);
        Helpers::writeLog($element->id, 'rent_place', Auth::user()->name . " обновил условия доставки");
        return $collect;
    }

    public function extra(Request $request)
    {
        $validate = Validator::make(
            $request->all(),
            [
                'cars_id' => 'required|integer',
                'active' => 'required',
                'extra_id' => 'required|integer',
                'extra_time_id' => 'required|integer',
                'extra_cost' => 'required|integer'
            ]
        );

        if ($validate->fails()) {
            return response()->json($validate->errors(), 500);
        }
        $id = null;
        if ($request->extra_cars == null) {
            $id = ExtraCars::create($request->except('_token', 'extra_cars'));
        } else{
            ExtraCars::where('id', $request->extra_cars)->delete();
            $id = ExtraCars::find($request->extra_cars);
        }
        Helpers::writeLog($request->cars_id, 'extra_cars', Auth::user()->name . " updated extra features");

       return response()->json($request->extra_cars);
    }

    public function course(Request $request)
    {
        $validate = Validator::make(
            $request->all(),
            [
                'cars_id' => 'required|integer',
                'active' => 'required',
                'course_id' => 'required|integer',
                'course_time_id' => 'required|integer',
                'course_cost' => 'required|integer'
            ]
        );

        if ($validate->fails()) {
            return response()->json($validate->errors(), 500);
        }
        $id = null;
        if ($request->courses_cars == null) {
            $id = CourseCars::create($request->except('_token', 'courses_cars'));
        } else{
            CourseCars::where('id', $request->courses_cars)->delete();
            $id = CourseCars::find($request->courses_cars);
        }
        Helpers::writeLog($request->cars_id, 'courses_cars', Auth::user()->name . " update education courses");

       return response()->json($request->courses_cars);
    }

    public function ban(Request $request)
    {
        $validate = Validator::make(
            $request->all(),
            [
                'cars_id' => 'required|integer',
            ]
        );

        if ($validate->fails()) {
            return response()->json($validate->errors(), 500);
        }

        $data = $request->except(['_token', 'cars_id']);
        CarsBans::where('cars_id', $request->cars_id)->update(['active' => 0]);
        foreach ($data as $key => $value) {
            CarsBans::where('id', $key)->update(['active' => $value]);
        }
        Helpers::writeLog($request->cars_id, 'cars_bans', Auth::user()->name . " обновил запреты машины");


        return $request;
    }

    public function location(Request $request, $car_id)
    {
        $car = Car::find($car_id);

        $car->update($request->all());
        Helpers::writeLog($request->cars_id, 'cars', Auth::user()->name . " обновил локацию машины");


        return $request;
    }

    public function type(Request $request, $car_id)
    {
        $car = Car::find($car_id);
        $car->update(['unlock_type_id' => (int)$request->type]);
        Helpers::writeLog($request->cars_id, 'cars', Auth::user()->name . " обновил способы передачи машины");
    }

    public function editKaskoOsagoDate(Request $request, $car_id)
    {
        Car::find($car_id)->update($request->except('_token'));
    }
    
    public function editGos(Request $request, $car_id)
    {
        Car::find($car_id)->update($request->except('_token'));
    }


}
