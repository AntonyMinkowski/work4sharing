<?php

namespace App\Http\Controllers\Web\UserProfile;

use App\Mail\NotificationsEmail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Mail;
use Auth;
use Session;
use Validator;
use App\User;
use App\Models\{
    BookingDate,
    BookingPhoto,
    BookingFiel,
    BookingMileage,
    BookingStatus,
    BookingRefusal,
    PaymentBooking
};
use Carbon\Carbon;
use PDF;

class MyTripsController extends Controller
{
    public function index()
    {
        $user_id = Auth::id();

        $activeCars = BookingDate::where('user_id', $user_id)->where('action', 1)->with('cars')->get();
        if ($activeCars !== [] && isset($activeCars[0])) {
            $i = 0;
            foreach ($activeCars as $item) {
                $image_car = DB::table('cars_images')
                    ->select('cars_images.name as img_url')
                    ->where('cars_images.car_id', $item->car_id)
                    ->first();

                $status = BookingStatus::where('id', $item->status)->pluck('name')->first();

                $paymentBooking = PaymentBooking::where('booking_id', $item->id)->first();
                

                $activeCars[$i]['datefrom'] = Carbon::parse($item->datefrom)->format('Y-m-d H:i');
                $activeCars[$i]['dateto'] = Carbon::parse($item->dateto)->format('Y-m-d H:i');
                $activeCars[$i]['statusName'] = $status;
                $activeCars[$i]['totalCost'] = $paymentBooking['total_amount_booking'];

                $user = Auth::user();
                if ($user->validate == 1 && $item->status == 22) {
                    $activeCars[$i]['statusName'] = 'Ожидание подтверждение владельца';
                    $booking_date = BookingDate::where('id', $item->id)->first();
                    $booking_date->status = 6;
                    $booking_date->save();
                }

                if (isset($image_car->img_url)) {
                    $activeCars[$i]['img_url'] =  $image_car->img_url;
                }
            $i++;
            }
        }

        $bookedCars = BookingDate::where('user_id', $user_id)->where('action', 0)->with('cars')->get();
        if ($bookedCars !== [] && isset($bookedCars[0])) {
            $i = 0;
            foreach ($bookedCars as $item) {
                $image_car = DB::table('cars_images')
                    ->select('cars_images.name as img_url')
                    ->where('cars_images.car_id', $item->car_id)
                    ->first();

                $statusName = BookingStatus::where('id', $item->status)->pluck('name')->first();
                $paymentBooking = PaymentBooking::where('booking_id', $item->id)->first();
                $bookedCars[$i]['datefrom'] = Carbon::parse($item->datefrom)->format('Y-m-d H:i');
                $bookedCars[$i]['dateto'] = Carbon::parse($item->dateto)->format('Y-m-d H:i');
                $bookedCars[$i]['statusName'] =  $statusName;
                $bookedCars[$i]['totalCost'] = $paymentBooking->total_amount_booking;

                $user = Auth::user();
                if ($user->validate == 1 && $item->status == 22) {
                    $bookedCars[$i]['statusName'] = 'Ожидание подтверждение владельца';
                    $booking_date = BookingDate::where('id', $item->id)->first();
                    $booking_date->status = 6;
                    $booking_date->save();
                }

                if (isset($image_car->img_url)) {
                    $bookedCars[$i]['img_url'] =  $image_car->img_url;
                }
            $i++;
            }
        }

        $historyCars = BookingDate::where('user_id', $user_id)->where('action', 2)->with('cars')->get();
        if ($historyCars !== [] && isset($historyCars[0])) {
            $i = 0;
            foreach ($historyCars as $item) {
                $image_car = DB::table('cars_images')
                    ->select('cars_images.name as img_url')
                    ->where('cars_images.car_id', $item->car_id)
                    ->first();

                $statusName = BookingStatus::where('id', $item->status)->pluck('name')->first();

                $paymentBooking = PaymentBooking::where('booking_id', $item->id)->first();


                $historyCars[$i]['datefrom'] = Carbon::parse($item->datefrom)->format('Y-m-d H:i');
                $historyCars[$i]['dateto'] = Carbon::parse($item->dateto)->format('Y-m-d H:i');
                $historyCars[$i]['statusName'] =  $statusName;
                $historyCars[$i]['totalCost'] = $paymentBooking->total_amount_booking;
                
                $user = Auth::user();
                if ($user->validate == 1 && $item->status == 22) {
                    $historyCars[$i]['statusName'] = 'Ожидание подтверждение владельца';
                    $booking_date = BookingDate::where('id', $item->id)->first();
                    $booking_date->status = 6;
                    $booking_date->save();
                }
                
                if (isset($image_car->img_url)) {
                    $historyCars[$i]['img_url'] =  $image_car->img_url;
                }
            $i++;
            }
        }

        return view('trips.index', [
            'activeCars' => $activeCars,
            'bookedCars' => $bookedCars,
            'historyCars' => $historyCars
        ]);
    }

    public function card($booking_id)
    {
        $booked = BookingDate::with('delivery.deliveryCars.rentPlace')->where('id', $booking_id)->with('cars')->first();
        $bookedPhoto = BookingPhoto::where('booking_id', $booking_id)->where('state', 1)->get();
        $bookedPhotoLandlord = BookingPhoto::where('booking_id', $booking_id)->where('state', 2)->get();
        $statusName = BookingStatus::where('id', $booked->status)->pluck('name')->first();

        $booking_fiel = BookingFiel::where('booking_id', $booked->id)->first();
        $booking_mileage = BookingMileage::where('booking_id', $booked->id)->first();

        $user = User::find($booked->user_id);

        $booked->booking_fiel =  $booking_fiel;
        $booked->booking_mileage =  $booking_mileage;

        $booked['datefrom'] = Carbon::parse($booked->datefrom)->format('Y-m-d H:i');
        $booked['dateto'] = Carbon::parse($booked->dateto)->format('Y-m-d H:i');

        $car = $booked->cars;

        $images = DB::table('cars_images')
            ->select('cars_images.name as img_url')
            ->where('cars_images.car_id', $car->id)
            ->get();

        ((new Carbon($booked->datefrom))->format('Y-m-d') == Carbon::now()->format('Y-m-d')) ? $buttonGet = 1 : $buttonGet = 0;


        return view('trips.card', [
            'booked' => $booked,
            'bookedPhoto' => $bookedPhoto,
            'bookedPhotoLandlord' => $bookedPhotoLandlord,
            'car' => $car,
            'statusName' => $statusName,
            'user' => $user,
            'images' => $images,
            'buttonGet' => $buttonGet
        ]);
    }

    public function cancelTransfer($booking_id)
    {
        if ($booking_id) {

            $bookingDate = BookingDate::where('id', $booking_id)->first();

            if ($bookingDate->user_id == Auth::id()) {

                $bookingDate->status = 11;
                $bookingDate->save();

                $user_car = User::find($bookingDate->user_id);

                if ($user_car && $user_car->email_notif === 1) {
        
                    $subject = 'Отмена передачи авто';
                    $textMessage = '<p style="outline: none!important;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box;font-size: 14px;color: #000;text-align: center;width: 275px;line-height: 1.5;margin: 32px auto 0;padding-bottom: 36px;color: #000"> 
                    Арендатор отменил передачу авто.
                    </p>';
        
                    Mail::to($user_car->email)
                        ->send(new NotificationsEmail($subject, $textMessage));
                }
            
            } else {
                abort(404);
            }
        }

        return back();
    }
   
    public function passCar($booking_id)
    {
        if ($booking_id) {

            $bookingDate = BookingDate::where('id', $booking_id)->first();

            if ($bookingDate->user_id == Auth::id()) {

                $bookingDate->status = 15;
                $bookingDate->save();

                $user_car = User::find($bookingDate->user_id);

                if ($user_car && $user_car->email_notif === 1) {
        
                    $subject = 'Передача авто';
                    $textMessage = '<p style="outline: none!important;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box;font-size: 14px;color: #000;text-align: center;width: 275px;line-height: 1.5;margin: 32px auto 0;padding-bottom: 36px;color: #000"> 
                    Арендатор просит завершить аренду и вернуть вам автомобиль. Свяжитесь с ним для уточнения деталей передачи.
                    </p>';
        
                    Mail::to($user_car->email)
                        ->send(new NotificationsEmail($subject, $textMessage));
                }
            
            } else {
                abort(404);
            }
        }

        return back();
    }

    public function accept($booking_id)
    {
        if ($booking_id) {

            $bookingDate = BookingDate::where('id', $booking_id)->first();

            if ($bookingDate->user_id == Auth::id()) {

                $bookingDate->status = 21;
                $bookingDate->action = 2;
                $bookingDate->save();
            
            } else {
                abort(404);
            }
        }

        return back();
    }

    public function payment($booking_id)
    {
        if ($booking_id) {

            $bookingDate = BookingDate::where('id', $booking_id)->first();

            if ($bookingDate->user_id == Auth::id()) {

                $bookingDate->status = 11;
                $bookingDate->save();
            
            } else {
                abort(404);
            }
        }

        return back();
    }

    public function rescission($booking_id, Request $request)
    {
        if ($booking_id && $request->rescission_description) {

            $booking_refusal = new BookingRefusal;
            $booking_refusal->booking_id = $booking_id;
            $booking_refusal->renter = 1;
            $booking_refusal->description = $request->rescission_description;
            $booking_refusal->save();

            $booking_date = BookingDate::where('id', $booking_id)->first();
            $booking_date->datefrom = '2009-01-01 00:00';
            $booking_date->dateto = '2009-01-01 00:00';
            $booking_date->status = 5;
            $booking_date->save();
        }

        return back();
    }

    public function addCarInterior($booking_id, Request $request)
    {
        if ($request->hasFile('trips_car_interior')) {

            $validate = Validator::make(
                $request->all(),
                [
                    'trips_car_interior' => 'required|file|mimes:jpeg,bmp,png',
                ]
            );
    
            if ($validate->fails()) {
                return response()->json($validate->errors(), 500);
            }

            $hash = $this->createHashBooking($booking_id, Auth::id());

            Storage::disk('hot')->put(
                $hash,
                $request->file('trips_car_interior')->get(),
                array('ACL'=>'public-read')
            );
            if (Storage::disk('hot')->exists($hash)) {
                $booking_photo = new BookingPhoto;
                $booking_photo->booking_id = $booking_id;
                $booking_photo->name = $hash;
                $booking_photo->state = 1;
                $booking_photo->save();
            }
        } else {
            return response()->json('trips_car_interior not found', 500);
        }
    }

    public function addCarFuelResidue($booking_id, Request $request)
    {
        if ($request->hasFile('trips_fuel_residue')) {

            $validate = Validator::make(
                $request->all(),
                [
                    'trips_fuel_residue' => 'required|file|mimes:jpeg,bmp,png',
                ]
            );
    
            if ($validate->fails()) {
                return response()->json($validate->errors(), 500);
            }

            $hash = $this->createHashBooking($booking_id, Auth::id());

            $booking_fiel = BookingFiel::where('booking_id', $booking_id)->first();
            if ($booking_fiel !== [] && isset($booking_fiel->booking_id)) {
                Storage::disk('hot')->put(
                    $hash,
                    $request->file('trips_fuel_residue')->get(),
                    array('ACL'=>'public-read')
                );
                if (Storage::disk('hot')->exists($hash)) {
                    $booking_fiel->fiel_photo_before = $hash;
                    $booking_fiel->save();
                }
            } else {
                Storage::disk('hot')->put(
                    $hash,
                    $request->file('trips_fuel_residue')->get(),
                    array('ACL'=>'public-read')
                );
                if (Storage::disk('hot')->exists($hash)) {
                    $booking_fiel = new BookingFiel;
                    $booking_fiel->booking_id = $booking_id;
                    $booking_fiel->fiel_photo_before = $hash;
                    $booking_fiel->save();
                }
            }
        } else {
            return response()->json('trips_fuel_residue not found', 500);
        }
    }

    public function addCarMileagePhoto($booking_id, Request $request)
    {
        if ($request->hasFile('trips_run')) {

            $validate = Validator::make(
                $request->all(),
                [
                    'trips_run' => 'required|file|mimes:jpeg,bmp,png',
                ]
            );
    
            if ($validate->fails()) {
                return response()->json($validate->errors(), 500);
            }

            $hash = $this->createHashBooking($booking_id, Auth::id());
            
            $booking_mileage = BookingMileage::where('booking_id', $booking_id)->first();
            if ($booking_mileage !== [] && isset($booking_mileage->booking_id)) {
                Storage::disk('hot')->put(
                    $hash,
                    $request->file('trips_run')->get(),
                    array('ACL'=>'public-read')
                );
                if (Storage::disk('hot')->exists($hash)) {
                    $booking_mileage->mileage_photo_before = $hash;
                    $booking_mileage->save();
                }
            } else {
                Storage::disk('hot')->put(
                    $hash,
                    $request->file('trips_run')->get(),
                    array('ACL'=>'public-read')
                );
                if (Storage::disk('hot')->exists($hash)) {
                    $booking_mileage = new BookingMileage;
                    $booking_mileage->booking_id = $booking_id;
                    $booking_mileage->mileage_photo_before = $hash;
                    $booking_mileage->save();
                }
            }
        } else {
            return response()->json('trips_fuel_residue not found', 500);
        }
    }

    public function addCarMileageInput($booking_id, Request $request)
    {
        if ($request && $booking_id) {
            $booking_mileage = BookingMileage::where('booking_id', $booking_id)->first();
            $booking_mileage->mileage_before = $request->mileage;
            $booking_mileage->save();
        }

        return \Redirect::back()->with('response', 'Ваш пробег засчитан');
    }

    public function createHashBooking ($booking_id, $user_id)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randstring = '';

        for ($i = 0; $i < 20; $i++) {
            $randstring .= $characters[rand(0, strlen($characters) - 1)];
        }

        return ($booking_id + $user_id) . $randstring;
    }
    public function paymentFromTrip($booking_id)
    {
        $paymentBooking = PaymentBooking::where('booking_id', $booking_id)->first();
        return redirect()->route('payment')->with( [
            'paymentBooking' => $paymentBooking,
        ] );
    }
    public function pdf(Request $request)
    {
        $pdf = PDF::loadView('layouts.pdf', $request->all());
        return $pdf->stream();
    }
}
