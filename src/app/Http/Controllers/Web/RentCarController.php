<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class RentCarController extends Controller
{
    public function bookingCar() {
        $user_id = $_GET['user_id'];
        $car_id = $_GET['car_id'];
        $from = $_GET['from'];
        $to = $_GET['to'];

        $query = DB::table('cars')
            ->where('cars.id', $car_id)
            ->select('cars.datefrom', 'cars.dateto')
            ->get();

        if ($query[0]->datefrom < $from) {
            if ($query[0]->dateto > $to) {

            } else {
                return response()->json([
                    'error' => 'start_booking',
                    'message' => trans('message.13')
                ]);
            }
        } else {
            return response()->json([
                'error' => 'start_booking',
                'message' => trans('message.14')
            ]);
        }
        
    }

    public function rentCarUser () {

    }

    public function validateRentCarOwner() {

    }
}
