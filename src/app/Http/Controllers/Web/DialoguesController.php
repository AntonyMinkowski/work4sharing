<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Events\Message;
use Illuminate\Support\Facades\DB;
use App\Models\ChatRoom;
use Auth;

class DialoguesController extends Controller
{
    public function index() {

        $myCars = DB::table('cars')
            ->where('cars.user_id', Auth::id())
            ->pluck('cars.id');

        $bookingMyCars = DB::table('booking_date')
            ->whereIn('booking_date.car_id', $myCars)
            ->where('booking_date.status', '6')
            ->where('booking_date.action', '!=', '2')
            ->select('booking_date.car_id', 'booking_date.user_id')
            ->get();

        $bookingAuth = DB::table('booking_date')
            ->where('booking_date.user_id', Auth::id())
            ->where('booking_date.status', '6')
            ->where('booking_date.action', '!=', '2')
            ->select('booking_date.car_id', 'booking_date.user_id')
            ->get();

        /* 
        * Приходится делать два цикла, что бы сопоставить booking_date с chat_room, 
        * если сделать все одним запросом предавая массивы id bookingMyCars и bookingAuth,
        * в итоге могут быть выбраны лишние записи
        */
        $chat_rooms = [];
        if ($bookingMyCars !== []) {
            foreach ($bookingMyCars as $bookingMyCar) {
                $chat_room = DB::table('chat_room')
                    ->where('chat_room.car_id', $bookingMyCar->car_id)
                    ->where('chat_room.user_id', $bookingMyCar->user_id)
                    ->select('chat_room.id', 'chat_room.car_id', 'chat_room.user_id')
                    ->first();

                if ($chat_room !== []) {
                    $chat_rooms[] = $chat_room; 
                }       
            }       
        }       
        if ($bookingAuth !== []) {
            foreach ($bookingAuth as $user) {
                $chat_room = DB::table('chat_room')
                    ->where('chat_room.user_id', $user->user_id)
                    ->where('chat_room.car_id', $user->car_id)
                    ->select('chat_room.id', 'chat_room.car_id', 'chat_room.user_id')
                    ->first();

                if ($chat_room !== []) {
                    $chat_rooms[] = $chat_room; 
                }
            }       
        }       

        $chatCars = [];
        if ($chat_rooms !== []) {
            foreach ($chat_rooms as $chat_room) {
                if (isset($chat_room->id) && isset($chat_room->car_id) && isset($chat_room->user_id)) {
                    $chatCar = DB::table('cars')
                        ->join('models_cars', 'cars.model_id', '=', 'models_cars.id')
                        ->join('brands_cars', 'cars.brand_id', '=', 'brands_cars.id')
                        ->select('cars.id', 'cars.user_id', 'brands_cars.name as brand', 'models_cars.name as model')
                        ->where('cars.id', $chat_room->car_id)
                        ->first();

                    if (isset($chat_room->id) && !$chat_room->id) {
                        $room_messages = DB::table('messages')
                            ->where('messages.room_id', $chat_room->id)
                            ->select('messages.id', 'messages.content', 'messages.room_id', 'messages.user_id', 'messages.created_at')
                            ->get();

                        $chatCar->messages = $room_messages;
                    }

                    if (isset($chatCar->id) && !$chatCar->id) {
                        $carImages = DB::table('cars_images')
                            ->where('cars_images.car_id', $chatCar->id)
                            ->select( 'cars_images.name', 'cars_images.id', 'cars_images.avatar' )
                            ->first();

                        $chatCar->images = $carImages;
                    }
                    
                    if (isset($chatCar->user_id) && !$chatCar->user_id) {
                        $interlocutor = DB::table('users')
                            ->where('users.id', $chatCar->user_id)
                            ->select('users.name')
                            ->first();

                        $chatCar->interlocutor = $interlocutor;
                    }

                    if (isset($chatCar->id) && !$chat_room->id) {
                        $chatCar->room_id = $chat_room->id;
                    }

                    $chatCars[] = $chatCar;
                }
            }
        }   

        return view (
            'dialogues.dialogues', [
                'chatCars' => $chatCars,
                'user' => Auth::user(),
            ]
        );
    }

    public function messages(Request $request) {
        Message::dispatch($request->all());
    }
}
