<?php

namespace App\Http\Controllers\Web;

use App\Models\{BookingDate, BookingExtra, Car, CarImage, DeliveryCars, ExtraCars, PaymentBooking, PersonalUser, BookingDelivery};
use App\User;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Cookie;
use test\Mockery\Stubs\Animal;
use Illuminate\Support\Facades\Hash;


class CheckoutController extends ImageController
{

    public function goToCheckout(Request $request, $car_id)
    {
        //Сохраняем в сессию данные 
        session([
            'to' => $request->to,
            'from' => $request->from,
            'location_delivery' => $request->location_delivery
        ]);

        return redirect()->route('checkout', $car_id);
    }

    public function index($car_id)
    {
        Cookie::queue('car_id', $car_id, 20);
        if (session('to') and session('from'))
            
        $toDate = Carbon::createFromFormat('d-m-Y H:i', session('to'));
        $fromDate = Carbon::createFromFormat('d-m-Y H:i', session('from'));
        $query = Car::where('cars.id', $car_id);
        if (!Car::checkForAvailability($query, clone $fromDate, clone $toDate)->first()) {
            return redirect()->back()->with([
                'message' => trans('message.6'),
                'status' => 'unavailable'
            ]);
        }
        /** @var integer $numberOfDays */
        $numberOfDays = $toDate->diffInDays($fromDate);
        /** @var Animal $numberOfHours */
        $numberOfHours = $toDate->diffInHours($fromDate);
        /* Количество дней округляем вверх */
        $numberOfDays = $numberOfHours % 24 === 0 ? $numberOfDays : $numberOfDays + 1;

        $includedDistance = Car::calculateIncludedDistance(Car::find($car_id), $fromDate, $toDate);
        if ($numberOfDays <= 0) {
            return redirect()->back();
        }
        $user = User::find(Auth::id());
        //Получаем коллекцию машины
        /* @var $car Car
         */
        $car = Car::find($car_id);
        $carImage = CarImage::where('car_id', $car_id)->first();


        $check_data = array();

        $check_data['cost_day'] = $car['cost_day'];

        //Сумма за день умноженая на количество забронированых дней
        $sum = $car['cost_day'] * $numberOfDays;
        $rentSum = $sum;
        $rentPlace = [];
        if (session('location_delivery') != NULL) {
            $rentPlace = DB::table('rent_place')
                ->select('rent_place.id', 'rent_place.name')
                ->where('rent_place.id', session('location_delivery'))
                ->first();
        }

        $RentSum = $rentSum;


        //Цена каско 0.1% от суммы авто за каждый день, умножаем количество дней и только если у машины нет своего КАСКО
        $kaskoCost = 0;
        $kaskoCostDay = 0;
        if (!$car['kasko']) {
            $kaskoCostDay = ($car['assessed_value'] / 100 * 0.1);
            $kaskoCost = round(($car['assessed_value'] / 100 * 0.1) * $numberOfDays, 0, PHP_ROUND_HALF_UP);
        }

        $CostOneDay = $car['cost_day'] + $kaskoCostDay;

        $sum += $kaskoCost;
        $RentSum += $kaskoCost;

        if ($car->kasko == 1) {
            $bookingSum = $RentSum / 100 * 15;
            $index = 15;
        } else {
            $bookingSum = $RentSum / 100 * 25;
            $index = 25;
        }



        $check_data['RentSum'] = $RentSum;

                        //Получаем цену 
                        $deliveryCost = DeliveryCars::where('car_id', $car_id)->where('place', session('location_delivery'))->pluck('cost');
                        if (isset($deliveryCost[0])) {
                            $deliveryCost = $deliveryCost[0];
                            $sum = $sum + $deliveryCost;
                        } else {
                            $deliveryCost = 0;
                        }


                        $calculatedDeposit = $car->getCalculatedDeposit();
                        $sum += $calculatedDeposit;
        
        $КesidualAmount = $sum - $bookingSum;


        $info_order = [
            'DateFrom' => date('d.m.Y', strtotime(session('from'))),
            'TimeFrom' => date('h:i', strtotime(session('from'))),
            'DateTo' => date('d.m.Y', strtotime(session('to'))),
            'TimeTo' => date('h:i', strtotime(session('to'))),
            'sum' =>  $sum,
            'RentSum' =>  $RentSum,
            'bookingSum' =>  $bookingSum,
            'КesidualAmount' =>  $КesidualAmount,
            'kaskoCost' =>  $kaskoCost,
            'number_days' => $numberOfDays,
            'delivery_cost' => $deliveryCost,
            'deposit' => $calculatedDeposit,
            'CostOneDay' => $CostOneDay,
            'cost_day' => $car['cost_day'],
            'kaskoCostDay' => $kaskoCostDay,
            'included_distance' => $includedDistance,
            'index' => $index,
            'delivery_place' => session('location_delivery')
        ];
        


        session([
            'sum' =>  $sum,
            'RentSum' =>  $RentSum,
            'bookingSum' =>  $bookingSum,
            'КesidualAmount' =>  $КesidualAmount,
            'kaskoCost' =>  $kaskoCost,
            'kaskoCostDay' =>  $kaskoCostDay,
            'number_days' => $numberOfDays,
            'index' => $index,
        ]);



        return view(
            'rent-auto.checkout', [
            'car' => $car,
            'CarImage' => $carImage->name,
            'info_order' => $info_order,
            'isValidate' => Auth::user()['validate'],
            'user' => $user,
            'rentPlace' => $rentPlace,
            'check_data' => $check_data,
            'userPersonal' => PersonalUser::find($user['personal_user_id']),
            'extras' => ExtraCars::where('cars_id', $car->id)->get(),
        ]);
    }

    public function goToRegister()
    {
        Cookie::queue('checkout', true, 20);
        return redirect('/register');
    }
    
    public function sendValidateSms(Request $request)
    {


        $phone = $request->phone;

        $phone = str_replace("+", "", $phone);
        $phone = str_replace(" ", "", $phone);
        $phone = str_replace("(", "", $phone);
        $phone = str_replace(")", "", $phone);
        $phone = str_replace("-", "", $phone);
        if (strlen($phone) == 10) {
            $phone = "7" . $phone;
        } else if (substr($phone, 0, 1) == "8") {
            $phone = "7" . substr($phone, 1, strlen($phone));
        }


        if (User::where('phone', $phone)->first()) {
            return 0;
        } else {

            $car_id = Cookie::get('car_id');
            Cookie::queue('checkout', true, 10);
            $passwd = static::createPasswd();
            $personal_user = PersonalUser::create();
            $user = new User([
                'name' => 'Мое имя',
                'email' => $phone,
                'password' => Hash::make($passwd),
                'personal_user_id' => $personal_user->id,
                'app_notif' => 1,
                'email_notif' => 1,
                'sms_notif' => 1,
            ]);
            $user->save();
            Auth::loginUsingId($user->id);


        $user->update([
            'phone' => $phone
        ]);

        $code = rand(100000, 1000000);
        setcookie('code', $code);
        $login = 'marussia_f1_team';
        $pass = 'SOAUTO_SMS';

        $endpoint = 'https://smsc.ru/sys/send.php';
        $client = new \GuzzleHttp\Client();

        $msg = "$code – твой код подтверждения, для подключения к сообществу соавторов.";

        $response = $client->request('GET', $endpoint, ['query' => [
            'login' => $login, 
            'psw' => $pass,
            'phones' => $request->phone,
            'mes' => $msg
        ]]);

        if ($response->getStatusCode() == 200) {
            return response()->json([
                'message' => trans('message.7') . $request->phone . trans('message.8'),
                'code' => $code
            ]);
        } else{
            return response()->json([
                'message' => trans('message.9')
            ]);
        }    
    } 
        
    }

    public function validatePhone(Request $request)
    {   
        $user = User::find(Auth::id());
        Log::debug(Auth::id());

        if ($request->code == (int)$_COOKIE['code']) {
            $user->update([
                'confirmed' => 1
            ]);
            return response()->json([
                'message' => trans('message.10'),
            ]);
        } else{
            return response()->json([
                'message' => trans('message.11'),
            ]);
        }

        $login = 'marussia_f1_team';
        $pass = 'SOAUTO_SMS';

        $endpoint = 'https://smsc.ru/sys/send.php';
        $client = new \GuzzleHttp\Client();

        $phone = "79375362451, 79026557095";

        $response = $client->request('GET', $endpoint, ['query' => [
            'login' => $login, 
            'psw' => $pass,
            'phones' => $phone,
            'mes' => trans('message.12'),
        ]]);
    }
    

    public function booking(Request $request, $car_id)
    {
        Cookie::forget('checkout');
        Cookie::forget('car_id');
        $user = User::find(Auth::id());
        $car = DB::table('cars')
            ->join('brands_cars', 'cars.brand_id', 'brands_cars.id')
            ->join('models_cars', 'cars.model_id', 'models_cars.id')
            ->where('cars.id', $car_id)
            ->select('cars.*', 'brands_cars.name as brand_name', 'models_cars.name as model_name')
            ->first();
    
        $user_car = User::find($car->user_id);

        $status = 1;
        //Статусы при бронированиии
        // if ($user->personalUser->validate && $car->unlock_type_id == 2) {
        //     $status = 3; // Забронировано
        // } elseif($user->personalUser->validate && $car->unlock_type_id == 1){
        //     $status = 6; //Подтверждение владельца
        // } elseif(!$user->personalUser->validate) {
        //     $status = 22; //Ваши данные на проверке
        // }


        $from = date_create($request->from);
        $datefrom = date_format($from, 'Y-m-d h:i:s');

        $to = date_create($request->to);
        $dateto = date_format($to, 'Y-m-d h:i:s');
        //Создаем новый обьект бронирования
        $booking_date = BookingDate::create([
            'user_id' => $user->id,
            'car_id' => $car_id,
            'datefrom' => $datefrom,
            'dateto' => $dateto,
            'action' => 0,
            'status' => $status,
            'count_day' => $request->count_day,
            'mileage' => $request->mileage,
            'deposit' => $request->deposit,
            'delivery_place' => $request->delivery_place != 0 ? $request->delivery_place : null,
            'kasko' => session('kaskoCost')
        ]);

        // $extras_sum = 0;
        //Создаем новый обьект extra
        // if (isset($request->extras)) {
        //     foreach (json_decode($request->extras) as $key => $value) {
        //         BookingExtra::create([
        //             'booking_id' => $booking_date->id,
        //             'extra_id' => $value->id,
        //             'cost' => json_decode($value->cost)
        //         ]);    
        //         $extras_sum += json_decode($value->cost);
        //     }
        // }

        //Создаем новый обьект оплаты бронирования
        $paymentBooking = PaymentBooking::create([
            'booking_id' => $booking_date->id,
            'amount_booking' => session('rentSum'),
            'total_amount_booking' => session('bookingSum')
        ]);
        //Создаем новый обьект доставки
        // if ($request->delivery_place) {
        //     $deliveryCars = DeliveryCars::where('place', $request->delivery_place)->where('car_id', $car_id)->first();
        //     BookingDelivery::create([
        //         'booking_id' => $booking_date->id,
        //         'delivery_cars_id' => $deliveryCars->id,
        //         'cost' => $deliveryCars->cost
        //     ]);
        // }   

        $login = 'marussia_f1_team';
        $pass = 'SOAUTO_SMS';

        $endpoint = 'https://smsc.ru/sys/send.php';
        $client = new \GuzzleHttp\Client();

        $msg = "Новая бронь!!!";
        $phone = "79044088892, 79173380616, 79375362451, 79026557095, 79199829802";

        $response = $client->request('GET', $endpoint, ['query' => [
            'login' => $login, 
            'psw' => $pass,
            'phones' => $phone,
            'mes' => $msg
        ]]);


        return redirect()->route('payment')->with( [
            'paymentBooking' => $paymentBooking,
        ] );





        
    }

    protected static function createNames (): string
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randstring = '';

        for ($i = 0; $i < 10; $i++) {
            $randstring .= $characters[rand(0, strlen($characters) - 1)];
        }

        return $randstring;
    }


    protected static function createPasswd (): string
    {
        $characters = '!@#$%^&*()_+=-*/0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randstring = '';

        for ($i = 0; $i < 10; $i++) {
            $randstring .= $characters[rand(0, strlen($characters) - 1)];
        }

        return $randstring;
    }
}
