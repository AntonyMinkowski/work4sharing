<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\{RentPlace, City, Car, CarImage};

class HomePageController extends Controller
{
    public function index()
    {
      $cars = Car::find([1, 2, 3, 4, 5]);
      $images = Carimage::all();
      return view('home', [
        // 'rentPlace' => RentPlace::all(),
        'city' => City::orderBy('name', 'asc')->get(),
        'cars' => $cars,
        'images' => $images
        ]
      );
    }
}


