<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\{Car};
use App\Services\NormalizeKeysToModel;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

/**
 * Контроллер работы с авто
 */
class CarsController extends Controller
{
    /**
     * Получаем кастомные поля для заполнения перед сохранением авто
     *
     * @param  Request $request [description]
     * @return JsonResponse     [description]
     */
    public function getCustomFieldsCars(Request $request)
    {
        return response()->json(
            $this->getProperty()->map(function ($model) {
                return collect($model)->map(function ($item) {
                    return collect($item)->only(['id', 'name'])->toArray();
                });
            })
        );
    }

    /**
     * Создание авто пользователем
     *
     * @param  Request $request
     * @return JsonResponse
     */
    public function createCar(Request $request)
    {
        $validate = Validator::make(
            $request->all(),
            self::getValidateRolesCar()
        );

        if ($validate->fails()) {
            return response()->json($validate->errors(), 500);
        }

        $normolize = NormalizeKeysToModel::class;
        $params = collect(
            $request->only(array_keys(self::getValidateRolesCar()))
        )->mapWithKeys(function ($item, $key) use ($normolize) {
            return [$normolize::normalizeToCar($key) => $item];
        });

        $car = Auth::user()->cars()->findOrFail($result->get('carId'));

        $car->update(['register_step' => 1]);

        return response()->json($this->saveCar($params));
    }

    public function addCarFeature(Request $request)
    {
        $validate = Validator::make(
            $request->all(),
            self::getValidateRolesCarFeature()
        );

        if ($validate->fails()) {
            return response()->json($validate->errors(), 500);
        }

        $normolize = NormalizeKeysToModel::class;
        $params = collect(
            $request->only(
                'air',
                'bluetooth',
                'audioInput',
                'usbCharger',
                'childSeat',
                'cruiseControl',
                'gps'
            )
        )->mapWithKeys(function ($item, $key) use ($normolize) {
            return [$normolize::normalizeToCar($key) => $item];
        });

        return response()->json(
            $this->saveCarFeature(
                $params,
                Car::find($request->get('CarId'))
            )
        );
    }
}
