<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Car;
use Illuminate\Http\Request;
use Carbon\Carbon;

class CarCardController extends Controller
{
    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCarInfo($id)
    {
        return response()->json(Car::selectOrError($id));
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getFeatures($id)
    {
        /* @var $car  Car */
        $car = Car::find($id);
        return response()->json($car->getSelectedFeatures());
    }

    public function deliveryPlaces($id)
    {
        /* @var $car  Car */
        $car = Car::find($id);
        return response()->json($car->deliveryPlaces());
    }

    public function deliveryPlacesGrouped($id)
    {
        /* @var $car  Car */
        $car = Car::find($id);
        return response()->json($car->deliveryPlacesGrouped());
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getExtraFeatures($id)
    {
        /* @var $car  Car */
        $car = Car::find($id);
        return response()->json($car->extraFeatures());
    }
    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCourseFeatures($id)
    {
        /* @var $car  Car */
        $car = Car::find($id);
        return response()->json($car->courseFeatures());
    }
    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getVacationFeatures($id)
    {
        /* @var $car  Car */
        $car = Car::find($id);
        return response()->json($car->vacationFeatures());
    }

    public function getSimilarAutos(Request $request, $id)
    {
        return (new CatalogController())->SimilarAutos($request);
    }


    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkForAvailability(Request $request, $id)
    {
        $result = [];
        $fromDate = Car::getFromDate($request->from);
        $toDate = Car::getToDate($fromDate, $request->to);
        $query = Car::where('cars.id', $id);
        if (Car::checkForAvailability($query, $fromDate, $toDate)->first()) {
            $result['available'] = true;
        } else {
            $result['available'] = false;
        }

        $result['reason'] = Car::checkMinMaxRentTime($id, $fromDate, $toDate);

        return response()->json($result);
    }

    public function currentBookedDates($id)
    {
        /* @var $car  Car */
        $car = Car::find($id);
        return response($car->bookingDatesByDays());
    }

    public function currentRentDates($id)
    {
        /* @var $car  Car */
        $car = Car::find($id);
        return response($car->rentDatesByDays());
    }


    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getExtraData($id)
    {
        /* @var $car  Car */
        $car = Car::find($id);
        return response()->json([
            "rent_dates" => $car->rentDatesByDays(),
            "booking_dates" => $car->bookingDatesByDays(),
            "extra_features" => $car->extraFeatures(),
            "delivery_places" => $car->deliveryPlaces(),
            "features" => $car->getSelectedFeatures()
        ]);
    }

    public function getIncludedDistance(Request $request, $id)
    {
        $car = Car::find($id);
        $fromDate = Carbon::createFromFormat('d-m-Y h:i', $request->from);
        $toDate = Carbon::createFromFormat('d-m-Y h:i', $request->to);

        return Car::calculateIncludedDistance($car, $toDate, $fromDate);
    }
}
