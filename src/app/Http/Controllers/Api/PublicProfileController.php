<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Car;
use App\User;
use Illuminate\Http\JsonResponse;

class PublicProfileController extends Controller
{
    /**
     * @param $id
     * @return JsonResponse
     */
    public function allInfo($id)
    {
        /* @var $user User */
        $user = User::findOrFail($id);
        return response()->json([
            'user' => User::getUserInfo($id),
            'booking_amount' => $user->countBookingDates(),
            'cars' => $this->getUsersAuto($id,false)
        ]);
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function getBookingAmount($id)
    {
        /* @var $user User */
        $user = User::findOrFail($id);
        return response()->json($user->countBookingDates());
    }

    /**
     * @param $id
     * @param bool $json
     * @return JsonResponse
     */
    public function getUsersAuto($id, $json = true)
    {
        /* @var $user User */
        $user = User::findOrFail($id);
        $result = Car::getProfileInfo($user->cars())->get()->map([\Helpers::class, 'updateLink']);
        if ($json)
            return response()->json($result);
        return $result;
    }

}
