<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\UserController as Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Collection;
use App\User;
use App\Models\PersonalUser;
use Validator;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\Hash;
use Propaganistas\LaravelPhone\PhoneNumber;
use Illuminate\Http\JsonResponse;

/**
 * Контроллер работы с пользователями
 */
class UserController extends Controller
{
    /**
     * Получаем профиль
     *
     * @return Response      Коллекция полей пользователя
     */
    protected function getProfile(Request $request)
    {
        $user = Auth::user();
        return response()->json([
            'user' => $user,
            'verified' => $user->validate,
            'personal' => collect($user->personalUser)->only([
                'passport_face',
                'passport_visa',
                'driver_lisense_first',
                'driver_license_second'
            ])->map(function ($value) {
                return boolval($value);
            })
        ]);
    }

    public function updatePhone(Request $request, Authenticatable $user)
    {
        $validator = Validator::make(
            $request->only(['phone']),
            [
                'phone' => 'required|phone:RU',
                'sms_code' => 'required|string'
            ]
        );

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $token = $user->tokens()->apply(
            PhoneNumber::make($request->get('phone'), 'RU')->formatE164()
        )->first();

        if ($token && Hash::check($request->get('sms_code'), $token->hash)) {
            $user->phone = $request->get('phone');
            $user->save();
            $token->used = true;

            return response()->json(['result' => true]);
        } elseif ($token) {
            // стоит ли удалять
            // $token->delete();
        }

        return response()->json([
            'result' => false, 
            'message' => trans('message.4')
        ], 400);
    }

    /**
     * подтверждение почты
     * @param  Request         $request запрос подтверждения почты
     * @param  Authenticatable $user    авторизованный пользователь
     * @return JsonResponse             результат сохранения
     */
    public function confirmedEmail(Request $request, Authenticatable $user): JsonResponse
    {
        $user->confirmed = true;
        $user->save();

        return response()->json(['result' => true]);
    }
}
