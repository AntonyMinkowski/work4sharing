<?php

namespace App\Http\Controllers\Staff;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Models\Car;

class ValidatedCarsController extends Controller
{
    public function index()
    {
        $cars = Car::getValidatedCars();
        foreach($cars as $item) {
            $item->osago_image = empty($item->osago_image) ? "" : Storage::disk('ice')->temporaryUrl($item->osago_image, now()->addMinutes(5));
            $item->kasko_image = empty($item->kasko_image) ? "" : Storage::disk('ice')->temporaryUrl($item->kasko_image, now()->addMinutes(5));
        }
        $route = CheckAutoController::getRoutes();
        return view('staff.validated-cars', compact('cars', 'route'));
    }
}
