<?php

namespace App\Http\Controllers\Staff;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class StartController extends Controller
{
    public function start() {

        $route = DB::table('staff_module')
            ->select('staff_module.route', 'staff_module.name')
            ->join('staff_access', 'staff_access.module_id', '=', 'staff_module.id')
            ->where('staff_access.user_id', Auth::id())
            ->get();

        return view('staff.staff', ['route' => $route]);
    }
}
