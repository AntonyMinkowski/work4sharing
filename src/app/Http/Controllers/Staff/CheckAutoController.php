<?php

namespace App\Http\Controllers\Staff;

use App\Http\Controllers\Controller;
use App\Http\Requests\FilterRequest;
use App\Events\CarValidateEvent;
use App\Mail\NotificationsEmail;
use App\Models\{BrandsCar, Car, ModelsCar};
use App\User;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Mail;
use Validator;

class CheckAutoController extends Controller
{
    public function start(FilterRequest $request)
    {
        $fromDate = $request->from_date;
        $toDate = $request->to_date;
        $brandId = $request->brand;
        $modelId = $request->model;
        $models = null;

        $route = CheckAutoController::getRoutes();

        $selectColumns = ['cars.id', 'cars.vin', 'cars.validate_refusal_id', 'users.name as userName', 'personal_users.passport_face', 'personal_users.passport_visa', 'brands_cars.name AS brand', 'models_cars.name AS model', 'cars_data.osago_image', 'cars_data.kasko_image'];

        $cars = DB::table('cars')
            ->leftJoin('brands_cars', 'cars.brand_id', '=', 'brands_cars.id')
            ->leftJoin('models_cars', 'cars.model_id', '=', 'models_cars.id')
            ->leftJoin('users', 'cars.user_id', '=', 'users.id')
            ->leftJoin('cars_data', 'cars.car_data_id', '=', 'cars_data.id')
            ->leftJoin('personal_users', 'users.personal_user_id', '=', 'personal_users.id')
            ->whereNull('cars.validate');


        if ($brandId) {
            $cars = $cars->where('cars.brand_id', $brandId);
            $models = $this->getModels($brandId, false);
        }
        if ($modelId) {
            $cars = $cars->where('cars.model_id', $modelId);
        }

        if ($fromDate or $toDate) {
            $this->getCarsBetweenFromToDates($cars, $fromDate, $toDate, $selectColumns);
        } else {
            $result = $this->getNewFromAndToDates(clone $cars);
            $fromDate = $result['fromDate'];
            $toDate = $result['toDate'];
        }

        //dd($cars);
        $cars = $cars->select($selectColumns)->get();
        //dd($cars);
        
        $cars_refusal = DB::table('cars_validate_refusal')
            ->select('cars_validate_refusal.id', 'cars_validate_refusal.name')
            ->get();

        foreach($cars as $item) {
            $item->passport_face = empty($item->passport_face) ? "" : Storage::disk('ice')->temporaryUrl($item->passport_face, now()->addMinutes(5));
            $item->passport_visa = empty($item->passport_visa) ? "" : Storage::disk('ice')->temporaryUrl($item->passport_visa, now()->addMinutes(5));
            $item->osago_image = empty($item->osago_image) ? "" : Storage::disk('ice')->temporaryUrl($item->osago_image, now()->addMinutes(5));
            $item->kasko_image = empty($item->kasko_image) ? "" : Storage::disk('ice')->temporaryUrl($item->kasko_image, now()->addMinutes(5));
        }


        return view('staff.check-auto', ['route' => $route, 'cars' => $cars, 'cars_refusal' => $cars_refusal, 'brandsCars' => BrandsCar::orderBy('name')->get(),
            'fromDate' => $fromDate, 'toDate' => $toDate, 'models' => $models, 'brandId' => $brandId, 'modelId' => $modelId]);
    }

    public function setCarRefusalId(Request $request, $car_id) {


        $validate = Validator::make(
            $request->all(),
            [
                'car_refusal_id' => 'required|integer',
            ]
        );

        if ($validate->fails()) {
            return response()->json($validate->errors(), 500);
        }

        $car = Car::where('id', $car_id)->first();
        $car->validate_refusal_id = $request->car_refusal_id;
        if ($car->validate_refusal_id == 15) {
            $car->validate = 1;
            //Сдаем авто в пулл если авто валидировано
            event(new CarValidateEvent($car));
        }
        $car->save();

        $refusal_name = DB::table('cars_validate_refusal')
            ->where('id', $request->car_refusal_id)
            ->pluck('name')
            ->first();

        $user_car = User::find($car->user_id);
        if ($user_car && $user_car->email_notif === 1) {
            $textMessage = '<h3 style="color: #000">'. $car->brand_name . ' ' . $car->model_name .'</h3>
            <p style="outline: none!important;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box;font-size: 14px;color: #000;text-align: center;width: 275px;line-height: 1.5;margin: 32px auto 0;padding-bottom: 36px;color: #000">' . 
            $refusal_name .
            '</p>';
            $subject = $refusal_name;

            Mail::to($user_car->email)
                ->send(new NotificationsEmail($subject, $textMessage));
        }

        if ($user_car && $user_car->sms_notif === 1) {
            $login = 'marussia_f1_team';
            $pass = 'SOAUTO_SMS';
            $endpoint = 'https://smsc.ru/sys/send.php';
            $client = new \GuzzleHttp\Client();

            $response = $client->request('GET', $endpoint, ['query' => [
                'login' => $login, 
                'psw' => $pass,
                'phones' => $user_car->phone,
                'mes' => $refusal_name
            ]]);
        }

        return response()->json();
    }

    public function getModels($brand_id, $toJson = true)
    {
        $models = ModelsCar::where('brand_id', $brand_id)->orderBy('name')->get();
        if ($toJson) {
            return response()->json($models);
        } else {
            return $models;
        }


    }

    public function getCarsBetweenFromToDates(Builder &$query, $from, $to, &$selectColumns)
    {
        $selectColumns[] = 'cars.created_at';
        if ($from) {
            $query = $query->whereDate('cars.created_at', '>=', $from);
        }
        if ($to) {
            $query = $query->whereDate('cars.created_at', '<=', "$to 23:59:59");
        }

    }

    public function getNewFromAndToDates(Builder $query, $tablePrefix = 'cars')
    {
        $result = $query->selectRaw("min($tablePrefix.created_at) as from_date, max($tablePrefix.created_at) as to_date")->first();
        return ['fromDate' => $result->from_date ? explode(' ', $result->from_date)[0] : '', 'toDate' => $result->to_date ? explode(' ', $result->to_date)[0] : ''];
    }

    public static function getRoutes()
    {
        return DB::table('staff_module')
            ->select('staff_module.route', 'staff_module.name')
            ->join('staff_access', 'staff_access.module_id', '=', 'staff_module.id')
            ->where('staff_access.user_id', Auth::id())
            ->get();
    }




}
