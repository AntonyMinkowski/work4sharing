<?php


use Illuminate\Support\Facades\Auth;

class Helpers
{
    public static function writeLog($model_id, $table, $message = null)
    {
        $log = new \App\Log([
            'changed_model_id' => $model_id,
            'table_name' => $table,
            'message' => $message,
            'user_id' => Auth::id()
        ]);
        $log->save();

    }

    public static function replaceNullToEmptyString($array)
    {
        $result = [];
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $result[$key] = self::replaceNullToEmptyString($value);
            } elseif (is_null($value)) {
                $result[$key] = "";
            } else {
                $result[$key] = $value;
            }
        }
        return $result;
    }

    public static function updateLink($car)
    {

        $avatars = $car->carImages->filter(function ($value, $key) {
            return $value->avatar === 1;
        });
        if ($avatars->count()) {
            $car->image = env('AWS_URL', 'https://hb.bizmrg.com') . "/" . env('AWS_BUCKET_HOT', 'soautomedia') . "/" . $avatars->first()->name;
        }

        return $car;
    }
}