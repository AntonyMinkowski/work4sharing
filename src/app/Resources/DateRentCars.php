<?php

namespace App\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DateRentCars extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'from' => $this->from,
            'to' => $this->to,
        ];
    }
}
