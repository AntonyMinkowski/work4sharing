<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * Пробег включительно. сущность mileage_incusive с атрибутами:
 * id (inc)
 * name (text)
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @mixin \Illuminate\Database\Eloquent\Builder
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MileageInclusive newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MileageInclusive newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MileageInclusive query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MileageInclusive whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MileageInclusive whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MileageInclusive whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MileageInclusive whereUpdatedAt($value)
 */
class MileageInclusive extends Model
{
    protected $table = 'mileage_inclusive';
}
