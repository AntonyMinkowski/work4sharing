<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\FuelType
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FuelType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FuelType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FuelType query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FuelType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FuelType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FuelType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FuelType whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FuelType whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class FuelType extends Model
{
    protected $guarded = [];
}
