<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CarUnlockType
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarUnlockType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarUnlockType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarUnlockType query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarUnlockType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarUnlockType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarUnlockType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarUnlockType whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CarUnlockType extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];
    /** @var string имя таблицы */
    protected $table = 'car_unlock_type';
}
