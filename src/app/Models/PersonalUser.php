<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class PersonalUser
 *
 * @package App\Models
 * @property boolean $own_car_oferta - 1. В сущность personal_users добавить атрибуты:ownCarOferta (bool)
 * @property int $id
 * @property string|null $passport_face
 * @property string|null $passport_visa
 * @property string|null $driver_lisense_first
 * @property string|null $driver_license_second
 * @property string|null $selfie
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $passport_seria
 * @property string|null $passport_nomer
 * @property string|null $passport_issued_by
 * @property string|null $passport_issued
 * @property string|null $driver_licence_num
 * @property int|null $validate
 * @property int|null $status
 * @property-read \App\Models\CustomerVerifyStatus $customerVerifyStatus
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PersonalUser newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PersonalUser newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PersonalUser query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PersonalUser whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PersonalUser whereDriverLicenceNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PersonalUser whereDriverLicenseSecond($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PersonalUser whereDriverLisenseFirst($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PersonalUser whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PersonalUser whereOwnCarOferta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PersonalUser wherePassportFace($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PersonalUser wherePassportIssued($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PersonalUser wherePassportIssuedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PersonalUser wherePassportNomer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PersonalUser wherePassportSeria($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PersonalUser wherePassportVisa($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PersonalUser whereSelfie($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PersonalUser whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PersonalUser whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PersonalUser whereValidate($value)
 * @mixin \Eloquent
 */
class PersonalUser extends Model
{
    protected $table = 'personal_users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'passport_face',
        'passport_visa',
        'id',
        'passport_seria',
        'passport_nomer',
        'passport_issued_by',
        'passport_issued',
        'driver_licence_num',
        'driver_lisense_first',
        'driver_license_second',
        'selfie'
    ];

    public function user()
    {
        return $this->hasOne('App\User', 'personal_user_id', 'id');
    }

    public function customerVerifyStatus()
    {
        return $this->hasOne('App\Models\CustomerVerifyStatus', 'id', 'personal_user_id');
    }
}
