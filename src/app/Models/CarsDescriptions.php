<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CarsDescriptions
 *
 * @property int $id
 * @property int|null $cars_id
 * @property string|null $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarsDescriptions newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarsDescriptions newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarsDescriptions query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarsDescriptions whereCarsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarsDescriptions whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarsDescriptions whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarsDescriptions whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarsDescriptions whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CarsDescriptions extends Model
{
    protected $fillable = [
        'cars_id',
        'description'
    ];
}
