<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\StsImage
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StsImage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StsImage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StsImage query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StsImage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StsImage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StsImage whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StsImage whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class StsImage extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];
}
