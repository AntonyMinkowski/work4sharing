<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 4. Создать сущность time_alert_own_rent с атрибутами:
 * id (inc)
 * name (text)
 * time (int)
 *
 * @property integer $id
 * @property string  $name
 * @property integer $time timestamp
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Car[] $cars
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TimeAlertOwnRent newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TimeAlertOwnRent newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TimeAlertOwnRent query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TimeAlertOwnRent whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TimeAlertOwnRent whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TimeAlertOwnRent whereTime($value)
 * @mixin \Eloquent
 */
class TimeAlertOwnRent extends Model
{
    protected $table = 'time_alert_own_rent';
    //
    // Переопределение типов свойств
    protected $casts = [
        'time' => 'timestamp',
    ];

    public function cars()
    {
        return $this->hasMany(Car::class, 'time_alert');
    }
}
