<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SelectionCar
 *
 * @property-read \App\Models\SelectionCar $selectioncars
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SelectionCar newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SelectionCar newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SelectionCar query()
 * @mixin \Eloquent
 */
class SelectionCar extends Model
{
    protected $fillable = [
        'name'
    ];

    public function selectioncars()
    {
        return $this->belongsTo('App\Models\SelectionCar', 'models_selectioncar_id_foreign', 'secel_id');
    }
}
