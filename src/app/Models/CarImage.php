<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CarImage
 *
 * @property int $id
 * @property int $car_id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $avatar
 * @property-read \App\Models\Car $car
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarImage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarImage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarImage query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarImage whereAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarImage whereCarId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarImage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarImage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarImage whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarImage whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CarImage extends Model
{
    protected $table = 'cars_images';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'car_id',
        'avatar'
    ];

    public function car()
    {
        return $this->belongsTo('App\Models\Car');
    }
}
