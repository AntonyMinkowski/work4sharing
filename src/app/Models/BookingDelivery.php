<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\BookingDelivery
 *
 * @property int $id
 * @property int|null $booking_id
 * @property int|null $delivery_cars_id
 * @property int|null $count
 * @property int|null $cost
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingDelivery newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingDelivery newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingDelivery query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingDelivery whereBookingId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingDelivery whereCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingDelivery whereCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingDelivery whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingDelivery whereDeliveryCarsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingDelivery whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingDelivery whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class BookingDelivery extends Model
{
    protected $fillable = [
      'booking_id',
      'delivery_cars_id',
      'cost'
    ];

    public function deliveryCars()
    {
      return $this->belongsTo(DeliveryCars::class, 'delivery_cars_id', 'id');
    }

}
