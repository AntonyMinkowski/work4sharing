<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CourseTime
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CourseTime newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CourseTime newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CourseTime query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CourseTime whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CourseTime whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CourseTime whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CourseTime whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CourseTime extends Model
{
    protected $table = 'course_time';
}
