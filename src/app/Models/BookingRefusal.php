<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\BookingRefusal
 *
 * @property int $id
 * @property int $booking_id
 * @property int|null $renter
 * @property int|null $own
 * @property string|null $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingRefusal newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingRefusal newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingRefusal query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingRefusal whereBookingId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingRefusal whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingRefusal whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingRefusal whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingRefusal whereOwn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingRefusal whereRenter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingRefusal whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class BookingRefusal extends Model
{
    protected $table = 'booking_refusal';

    protected $fillable = [
      'booking_id',
      'renter',
      'own',
      'description'
    ];
}
