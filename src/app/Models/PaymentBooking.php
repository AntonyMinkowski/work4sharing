<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PaymentBooking
 *
 * @property int $id
 * @property int|null $booking_id
 * @property int|null $amount_booking
 * @property int|null $total_amount_booking
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $result
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentBooking newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentBooking newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentBooking query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentBooking whereAmountBooking($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentBooking whereBookingId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentBooking whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentBooking whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentBooking whereResult($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentBooking whereTotalAmountBooking($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentBooking whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class PaymentBooking extends Model
{
    protected $fillable = [
        'booking_id',
        'amount_booking',
        'total_amount_booking',
        'result'
    ];
}
