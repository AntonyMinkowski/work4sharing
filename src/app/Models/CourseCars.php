<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CourseCars
 *
 * @property int $id
 * @property int|null $cars_id
 * @property int|null $course_id
 * @property int|null $course_time_id
 * @property int|null $course_cost
 * @property int|null $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\course|null $course
 * @property-read \App\Models\courseTime|null $time
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CourseCars newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CourseCars newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CourseCars query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CourseCars whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CourseCars whereCarsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CourseCars whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CourseCars wherecourseCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CourseCars wherecourseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CourseCars wherecourseTimeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CourseCars whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CourseCars whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class courseCars extends Model
{
    protected $table = 'courses_cars';

    protected $fillable = [
        'cars_id',
        'active',
        'course_id',
        'course_time_id',
        'courses_cars',
        'course_cost'
    ];

    public function course()
    {
        return $this->belongsTo(Course::class, 'course_id');
    }
    public function time()
    {
        return $this->belongsTo(CourseTime::class, 'course_time_id');
    }
}
