<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ReRunPrice
 *
 * @property int $id
 * @property int $car_price
 * @property float $coef
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReRunPrice newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReRunPrice newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReRunPrice query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReRunPrice whereCarPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReRunPrice whereCoef($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReRunPrice whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReRunPrice whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReRunPrice whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ReRunPrice extends Model
{
    //
}
