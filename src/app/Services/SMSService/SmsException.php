<?php

namespace App\Services\SMSService;

/**
 *
 */
class SmsException extends \Exception
{

    function __construct($message = '', $code)
    {
        parent::__construct($message, $code);
    }
}
