<?php

namespace App\Services\SMSService;

use App\Services\SMSService\AbstractSms;

/**
 *
 */
class Sms extends AbstractSMS
{
    protected $phones;
    protected $mes;

    public function setPhone(string $phone)
    {
        $this->phones = $phone;
    }

    public function setMessage(string $message = '')
    {
        $message = trim($message);
        if (mb_strlen($message, 'utf8') > 1000) {
            $message = substr($message, 0, 1000);
        }

        $this->mes = $message;
    }

    public function getPhone(): string
    {
        return $this->phones;
    }

    public function getMessage(): string
    {
        return $this->mes;
    }
}
