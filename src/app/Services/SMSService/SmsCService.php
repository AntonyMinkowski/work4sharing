<?php

namespace App\Services\SMSService;

use App\Services\SMSService\InterfaceSms;
use App\Services\SMSService\InterfaceSmsService;
use App\Services\SMSService\SmsCClient;
use Psr\Http\Message\ResponseInterface;

/**
 *
 */
class SmsCService implements InterfaceSmsService
{
    private $client;
    private $response;

    public function __construct(SmsCClient $client)
    {
        $this->client = $client;

    }

    public function sendSms(InterfaceSms $sms): bool
    {
        $response = $this->client->send($sms);
        $this->setResponse($response);

        $this->handler();

        return true;
    }

    public function getStatus(): array
    {
        return $this->getResponse();
    }

    private function setResponse(ResponseInterface $response)
    {
        $this->response = json_decode(
            $response->getBody()->getContents(),
            true
        );
    }

    public function getResponse()
    {

        return $this->response;
    }

    private function handler()
    {
        $response = $this->getResponse();
        if (isset($response['error_code']) && $response['error_code'] > 0) {
            throw new SmsException($response['error'], $response['error_code']);
        }
    }

}
