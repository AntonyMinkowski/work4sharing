<?php

namespace App\Services\SMSService;

use App\Services\SMSService\InterfaceSms;

/**
 *
 */
interface InterfaceSmsService
{
    /**
     * Метод отправки смс
     * @param  SMS $value сообщение для отправки
     * @return bool          результат отправки
     */
    public function sendSms(InterfaceSMS $value): bool;

    public function getStatus();
}
