<?php

namespace App\Listeners;

use App\User;
use App\Models\Token;
use App\Events\TokenSaved;
use Illuminate\Support\Facades\Hash;
use App\Services\SMSService\Sms;
use App\Services\SMSService\SmsCService;
/**
 *
 */
class SendSmsToken
{

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function handle(TokenSaved $event)
    {
        /** @var $user User  */
        $user = $event->token->user;
        $sms = new Sms;
        $sms->setPhone($event->token->phone);
        $sms->setMessage(
            sprintf(
                '%s, '. trans('message.23'). ' %d',
                $user->name,
                $event->code
            )
        );
        resolve(SmsCService::class)->sendSms($sms);
    }
}
