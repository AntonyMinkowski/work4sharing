<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarsDescriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars_descriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cars_id')->unsigned()->nullable();
            $table->text('description')->nullable();
            $table->timestamps();
        });

        // Schema::disableForeignKeyConstraints();
        // Schema::table('cars_descriptions', function (Blueprint $table) {
        //     $table->foreign('cars_id')->references('id')->on('cars');
        // });
        // Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('cars_descriptions');
    }
}
