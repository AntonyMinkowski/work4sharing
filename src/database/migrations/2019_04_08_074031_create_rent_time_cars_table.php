<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRentTimeCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rent_time_cars', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('car_id')->unsigned();
            $table->integer('min_rent_time')->nullable();
            $table->integer('max_rent_time')->nullable();
        });


        // Schema::disableForeignKeyConstraints();
        // Schema::table('rent_time_cars', function (Blueprint $table) {
        //     $table->foreign('car_id')->references('id')->on('cars');
        // });
        // Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('rent_time_cars');
    }
}
