<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses_cars', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cars_id')->unsigned()->nullable();
            $table->integer('course_id')->unsigned()->nullable();
            $table->integer('course_time_id')->unsigned()->nullable();
            $table->integer('course_cost')->nullable();
            $table->boolean('active')->nullable();
            $table->timestamps();
        });

        // Schema::disableForeignKeyConstraints();
        // Schema::table('courses_cars', function (Blueprint $table) {
        //     $table->foreign('cars_id')->references('id')->on('cars');
        //     $table->foreign('course_id')->references('id')->on('course');
        //     $table->foreign('course_time_id')->references('id')->on('course_time');
            
        // });
        // Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('courses_cars');
    }
}
