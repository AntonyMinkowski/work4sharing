<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarsBansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars_bans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cars_id')->unsigned()->nullable();
            $table->integer('bans_id')->unsigned()->nullable();
            $table->boolean('active')->nullable();
            $table->timestamps();
        });

        Schema::disableForeignKeyConstraints();
        Schema::table('cars_bans', function (Blueprint $table) {
            $table->foreign('cars_id')->references('id')->on('cars');
            $table->foreign('bans_id')->references('id')->on('car_bans');
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars_bans');
    }
}
