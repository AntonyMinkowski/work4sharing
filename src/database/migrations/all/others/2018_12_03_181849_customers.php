<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Customers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->Integer('passport_seriya');
            $table->Integer('passport_number');
            $table->Integer('driverLicense_number');
            $table->Date('birthday');
            $table->Integer('passport_image_id');
            $table->Integer('driver_license_image_id');
            $table->Integer('photoFaceWithDriverLicense_image_id');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
