<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToPersonalUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('personal_users', function (Blueprint $table) {
            $table->integer('passport_seria')->nullable();
            $table->integer('passport_nomer')->nullable();
            $table->text('passport_issued_by')->nullable();
            $table->text('passport_issued')->nullable();
            $table->text('driver_licence_num')->nullable();
            $table->integer('validate')->nullable();
            $table->integer('status')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('personal_users', function (Blueprint $table) {
            $table->dropColumn('passport_seria');
            $table->dropColumn('passport_nomer');
            $table->dropColumn('passport_issued_by');
            $table->dropColumn('passport_issued');
            $table->dropColumn('driver_licence_num');
            $table->dropColumn('validate');
            $table->dropColumn('status');
        });
    }
}
