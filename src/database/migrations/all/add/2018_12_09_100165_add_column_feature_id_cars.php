<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnFeatureIdCars extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('cars')) {
            if (!Schema::hasColumn('cars', 'feature_id') && Schema::hasColumn('cars_feature', 'id')) {
                Schema::table('cars', function (Blueprint $table) {
                    $table->unsignedInteger('feature_id')->nullable();
                });
                Schema::table('cars', function (Blueprint $table) {
                    $table->foreign('feature_id')->references('id')->on('cars_feature');
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('cars', 'feature_id')) {
            Schema::table('cars', function (Blueprint $table) {
                $table->dropForeign(['feature_id']);
                $table->dropColumn('feature_id');
            });
        }
    }
}
