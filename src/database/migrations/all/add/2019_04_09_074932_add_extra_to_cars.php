<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExtraToCars extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cars', function (Blueprint $table) {
            $table->integer('cars_extra_id')->unsigned()->nullable();
            
        });

        Schema::disableForeignKeyConstraints();
        Schema::table('cars', function (Blueprint $table) {
            $table->foreign('cars_extra_id')->references('id')->on('extra_cars');
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cars', function (Blueprint $table) {
            $table->dropForeign(['cars_extra_id']);
            $table->dropColumn('cars_extra_id');
        });
    }
}
