<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMileageAssessedColorsToCars extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cars', function (Blueprint $table) {
            $table->string('mileage')->nullable();
            $table->string('assessed_value')->nullable();
            $table->integer('color_id')->unsigned()->nullable();
        });

        Schema::disableForeignKeyConstraints();
        Schema::table('cars', function (Blueprint $table) {
            $table->foreign('color_id')->references('id')->on('cars_colors');
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cars', function (Blueprint $table) {
            $table->dropForeign(['color_id']);
            $table->dropColumn('color_id');
            $table->dropColumn('mileage');
            $table->dropColumn('assessed_value');
        });
    }
}
