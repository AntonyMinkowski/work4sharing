<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameColumnSeetCars extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('cars')) {
            if (Schema::hasColumn('cars', 'seet') && !Schema::hasColumn('cars', 'seat')) {
                Schema::table('cars', function (Blueprint $table) {
                    $table->renameColumn('seet', 'seat');
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('cars')) {
            if (!Schema::hasColumn('cars', 'seet') && Schema::hasColumn('cars', 'seat')) {
                Schema::table('cars', function (Blueprint $table) {
                    $table->renameColumn('seat', 'seet');
                });
            }
        }
    }
}
