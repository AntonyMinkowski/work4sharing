<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameColumnYearCars extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('cars')) {
            if (Schema::hasColumn('cars', 'year') && !Schema::hasColumn('cars', 'year_of_issue')) {
                Schema::table('cars', function (Blueprint $table) {
                    $table->renameColumn('year', 'year_of_issue');
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('cars')) {
            if (!Schema::hasColumn('cars', 'year') && Schema::hasColumn('cars', 'year_of_issue')) {
                Schema::table('cars', function (Blueprint $table) {
                    $table->renameColumn('year_of_issue', 'year');
                });
            }
        }
    }
}
