<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Cars extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //employee
        Schema::create('cars', function (Blueprint $table) {
            $table->increments('id');
            $table->Integer('register_step')->nullable();

    //current JOB

            //company
            $table->unsignedTinyInteger('user_id');
            //profession
            $table->unsignedTinyInteger('brand_id');
            //Qualification level
            $table->unsignedTinyInteger('model_id');
            //education
            $table->unsignedTinyInteger('color_id')->nullable(); 
            //direct/through connection
            $table->unsignedTinyInteger('unlock_type_id')->nullable();
            //cover letter
            $table->unsignedTinyInteger('description_id')->nullable();
            //industry
            $table->unsignedTinyInteger('car_type')->nullable();
            //remote / office 
            $table->unsignedTinyInteger('transmission_id')->nullable();
            //experience on current position
            $table->Integer('year_of_issue')->nullable();

    //general INFO

            //certificates ?
            $table->unsignedTinyInteger('car_data_id')->nullable();
            //work permit ? / experience in general
            $table->Integer('mileage')->nullable();
            //contract
            $table->unsignedTinyInteger('fuel_type_id')->nullable();
            //
            $table->unsignedTinyInteger('seat')->nullable();
            //
            $table->unsignedTinyInteger('doors')->nullable();
            //
            $table->unsignedTinyInteger('gear_id')->nullable();

            //employee must sign
            $table->boolean('oferta')->nullable();

            $table->Integer('cost_month');
            $table->Integer('cost_day');
            $table->Integer('cost_hour');
            $table->Integer('city');

            $table->boolean('available')->nullable();
            $table->boolean('validate')->nullable();

            $table->rememberToken();
            $table->timestamps();

            // $table->Integer('pts_image_id');
            // $table->Integer('sts_image_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
}
