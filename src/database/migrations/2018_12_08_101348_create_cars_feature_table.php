<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarsFeatureTable extends Migration
{
    /**
     * Добавляем таблицу дополнительных свойств авто
     *
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('cars_feature')) {
            Schema::create('cars_feature', function (Blueprint $table) {
                $table->increments('id');
                $table->text('en_name')->nullable();
                $table->text('gn_name')->nullable();
                $table->text('desc')->nullable();
                $table->timestamps();
                // $table->boolean('air')->default(false);
                // $table->boolean('bluetooth')->default(false);
                // $table->boolean('audio_input')->default(false);
                // $table->boolean('usb_charger')->default(false);
                // $table->boolean('child_seat')->default(false);
                // $table->boolean('cruise_control')->default(false);
                // $table->boolean('gps')->default(false);
            });
        }

        // Schema::create('feature_name', function (Blueprint $table) {
        //     $table->increments('id');
        //     $table->string('en_name');
        //     $table->string('gn_name');
        //     $table->text('desc')->nullable();
        //     $table->timestamps();
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars_feature');
        // Schema::dropIfExists('feature_name');
    }
}
