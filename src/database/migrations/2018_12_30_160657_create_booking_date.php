<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingDate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_date', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            $table->string('car_id');
            $table->datetime('datefrom');
            $table->datetime('dateto');
            $table->integer('status')->nullable();
            $table->integer('action')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_date');
    }
}
