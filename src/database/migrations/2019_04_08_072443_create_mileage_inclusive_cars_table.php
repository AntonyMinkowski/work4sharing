<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMileageInclusiveCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mileage_inclusive_cars', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->unsignedTinyInteger('car_id')->unique();
            $table->unsignedTinyInteger('day');
            $table->unsignedTinyInteger('week');
            $table->unsignedTinyInteger('month');
        });

        // Schema::disableForeignKeyConstraints();

        // Schema::table('mileage_inclusive_cars', function (Blueprint $table) {
        //     $table->foreign('car_id')->references('id')->on('cars');
            // $table->foreign('day')->references('id')->on('mileage_inclusive');
            // $table->foreign('week')->references('id')->on('mileage_inclusive');
            // $table->foreign('month')->references('id')->on('mileage_inclusive');
        // });
        
        // Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('mileage_inclusive_cars');
    }
}
