<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeaturesCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('features_cars', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedTinyInteger('cars_id')->nullable();
            $table->unsignedTinyInteger('feature_id')->nullable();
            $table->timestamps();
        });

        // Schema::disableForeignKeyConstraints();
        // Schema::table('features_cars', function (Blueprint $table) {
        //     $table->foreign('cars_id')->references('id')->on('cars');
        //     $table->foreign('feature_id')->references('id')->on('features');
        // });
        // Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('features_cars');
    }
}
