<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');

            // prefered work type
            $table->unsignedInteger('gear_type')->nullable();

            $table->text('description')->nullable();

            $table->string('avatar')->nullable();
            $table->string('country')->nullable();
            $table->string('city')->nullable();
            $table->string('name');
            $table->string('email')->unique();
            $table->string('phone')->unique()->nullable();
            $table->string('email_verified_at')->nullable();
            $table->string('password');

            $table->boolean('isActive')->nullable();
            $table->boolean('confirmed')->default(false);
            $table->boolean('isAdmin')->nullable();
            $table->boolean('isOwner')->nullable();
            $table->boolean('isCustomer')->nullable();
            $table->boolean('validate')->nullable();

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
