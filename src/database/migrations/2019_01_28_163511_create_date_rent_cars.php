<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDateRentCars extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('date_rent_cars', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('car_id');
            $table->datetime('from');
            $table->datetime('to');
            $table->boolean('active')->nullable();
            $table->timestamps();
        });

        // Schema::disableForeignKeyConstraints();
        // Schema::table('date_rent_cars', function (Blueprint $table) {
        //     $table->foreign('car_id')->references('id')->on('cars');
        // });
        // Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('date_rent_cars');
    }
}
