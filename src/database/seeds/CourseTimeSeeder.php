<?php

use Illuminate\Database\Seeder;
use App\Models\CourseTime;

class CourseTimeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CourseTime::insert([
            'name' => '1 day',
        ]);
        CourseTime::insert([
            'name' => 'hire period',
        ]);
    }
    
}
