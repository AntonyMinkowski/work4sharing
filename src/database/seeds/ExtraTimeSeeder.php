<?php

use Illuminate\Database\Seeder;
use App\Models\ExtraTime;

class ExtraTimeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ExtraTime::insert([
            'name' => '1 day',
        ]);
        ExtraTime::insert([
            'name' => 'hire period',
        ]);
    }
    
}
