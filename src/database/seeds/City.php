<?php
                        
            use Illuminate\Database\Seeder;
            use App\Models\City as CityModel;
            
class City extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
      [
      "name"=>"Aachen",
      "state"=>"Nordrhein-Westfalen"
    ],[
      "name"=>"Augsburg",
      "state"=>"Bayern"
    ],[
      "name"=>"Bergisch Gladbach",
      "state"=>"Nordrhein-Westfalen"
    ],[
      "name"=>"Berlin",
      "state"=>"Berlin"
    ],[
      "name"=>"Bielefeld",
      "state"=>"Nordrhein-Westfalen"
    ],[
      "name"=>"Bochum",
      "state"=>"Nordrhein-Westfalen"
    ],[
      "name"=>"Bonn",
      "state"=>"Nordrhein-Westfalen"
    ],[
      "name"=>"Bottrop",
      "state"=>"Nordrhein-Westfalen"
    ],[
      "name"=>"Braunschweig",
      "state"=>"Niedersachsen"
    ],[
      "name"=>"Bremen",
      "state"=>"Bremen"
    ],[
      "name"=>"Bremerhaven",
      "state"=>"Bremen"
    ],[
      "name"=>"Chemnitz",
      "state"=>"Sachsen"
    ],[
      "name"=>"Cottbus",
      "state"=>"Brandenburg"
    ],[
      "name"=>"Darmstadt",
      "state"=>"Hessen"
    ],[
      "name"=>"Dortmund",
      "state"=>"Nordrhein-Westfalen"
    ],[
      "name"=>"Dresden",
      "state"=>"Sachsen"
    ],[
      "name"=>"Duisburg",
      "state"=>"Nordrhein-Westfalen"
    ],[
      "name"=>"Düsseldorf",
      "state"=>"Nordrhein-Westfalen"
    ],[
      "name"=>"Erfurt",
      "state"=>"Thüringen"
    ],[
      "name"=>"Erlangen",
      "state"=>"Bayern"
    ],[
      "name"=>"Essen",
      "state"=>"Nordrhein-Westfalen"
    ],[
      "name"=>"Frankfurt am Main",
      "state"=>"Hessen"
    ],[
      "name"=>"Freiburg im Breisgau",
      "state"=>"Baden-Württemberg"
    ],[
      "name"=>"Fürth",
      "state"=>"Bayern"
    ],[
      "name"=>"Gelsenkirchen",
      "state"=>"Nordrhein-Westfalen"
    ],[
      "name"=>"Göttingen",
      "state"=>"Niedersachsen"
    ],[
      "name"=>"Hagen",
      "state"=>"Nordrhein-Westfalen"
    ],[
      "name"=>"Halle (Saale)",
      "state"=>"Sachsen-Anhalt"
    ],[
      "name"=>"Hamburg",
      "state"=>"Hamburg"
    ],[
      "name"=>"Hamm",
      "state"=>"Nordrhein-Westfalen"
    ],[
      "name"=>"Hannover",
      "state"=>"Niedersachsen"
    ],[
      "name"=>"Heidelberg",
      "state"=>"Baden-Württemberg"
    ],[
      "name"=>"Heilbronn",
      "state"=>"Baden-Württemberg"
    ],[
      "name"=>"Herne",
      "state"=>"Nordrhein-Westfalen"
    ],[
      "name"=>"Hildesheim",
      "state"=>"Niedersachsen"
    ],[
      "name"=>"Ingolstadt",
      "state"=>"Bayern"
    ],[
      "name"=>"Jena",
      "state"=>"Thüringen"
    ],[
      "name"=>"Karlsruhe",
      "state"=>"Baden-Württemberg"
    ],[
      "name"=>"Kassel",
      "state"=>"Hessen"
    ],[
      "name"=>"Kiel",
      "state"=>"Schleswig-Holstein"
    ],[
      "name"=>"Koblenz",
      "state"=>"Rheinland-Pfalz"
    ],[
      "name"=>"Krefeld",
      "state"=>"Nordrhein-Westfalen"
    ],[
      "name"=>"Köln",
      "state"=>"Nordrhein-Westfalen"
    ],[
      "name"=>"Leipzig",
      "state"=>"Sachsen"
    ],[
      "name"=>"Leverkusen",
      "state"=>"Nordrhein-Westfalen"
    ],[
      "name"=>"Ludwigshafen am Rhein",
      "state"=>"Rheinland-Pfalz"
    ],[
      "name"=>"Lübeck",
      "state"=>"Schleswig-Holstein"
    ],[
      "name"=>"Magdeburg",
      "state"=>"Sachsen-Anhalt"
    ],[
      "name"=>"Mainz",
      "state"=>"Rheinland-Pfalz"
    ],[
      "name"=>"Mannheim",
      "state"=>"Baden-Württemberg"
    ],[
      "name"=>"Moers",
      "state"=>"Nordrhein-Westfalen"
    ],[
      "name"=>"Mönchengladbach",
      "state"=>"Nordrhein-Westfalen"
    ],[
      "name"=>"Mülheim an der Ruhr",
      "state"=>"Nordrhein-Westfalen"
    ],[
      "name"=>"München",
      "state"=>"Bayern"
    ],[
      "name"=>"Münster",
      "state"=>"Nordrhein-Westfalen"
    ],[
      "name"=>"Neuss",
      "state"=>"Nordrhein-Westfalen"
    ],[
      "name"=>"Nürnberg",
      "state"=>"Bayern"
    ],[
      "name"=>"Oberhausen",
      "state"=>"Nordrhein-Westfalen"
    ],[
      "name"=>"Offenbach am Main",
      "state"=>"Hessen"
    ],[
      "name"=>"Oldenburg",
      "state"=>"Niedersachsen"
    ],[
      "name"=>"Osnabrück",
      "state"=>"Niedersachsen"
    ],[
      "name"=>"Paderborn",
      "state"=>"Nordrhein-Westfalen"
    ],[
      "name"=>"Pforzheim",
      "state"=>"Baden-Württemberg"
    ],[
      "name"=>"Potsdam",
      "state"=>"Brandenburg"
    ],[
      "name"=>"Recklinghausen",
      "state"=>"Nordrhein-Westfalen"
    ],[
      "name"=>"Regensburg",
      "state"=>"Bayern"
    ],[
      "name"=>"Remscheid",
      "state"=>"Nordrhein-Westfalen"
    ],[
      "name"=>"Reutlingen",
      "state"=>"Baden-Württemberg"
    ],[
      "name"=>"Rostock",
      "state"=>"Mecklenburg-Vorpommern"
    ],[
      "name"=>"Saarbrücken",
      "state"=>"Saarland"
    ],[
      "name"=>"Salzgitter",
      "state"=>"Niedersachsen"
    ],[
      "name"=>"Siegen",
      "state"=>"Nordrhein-Westfalen"
    ],[
      "name"=>"Solingen",
      "state"=>"Nordrhein-Westfalen"
    ],[
      "name"=>"Stuttgart",
      "state"=>"Baden-Württemberg"
    ],[
      "name"=>"Trier",
      "state"=>"Rheinland-Pfalz"
    ],[
      "name"=>"Ulm",
      "state"=>"Baden-Württemberg"
    ],[
      "name"=>"Wiesbaden",
      "state"=>"Hessen"
    ],[
      "name"=>"Wolfsburg",
      "state"=>"Niedersachsen"
    ],[
      "name"=>"Wuppertal",
      "state"=>"Nordrhein-Westfalen"
    ],[
      "name"=>"Würzburg",
      "state"=>"Bayern"
      ]
            // ['name' => 'г. Москва'],
            // ['name' => 'г. Санкт-Петербург'],
            // ['name' => 'г. Новосибирск'],
            // ['name' => 'г. Екатеринбург'],
            // ['name' => 'г. Нижний Новгород'],
            // ['name' => 'г. Казань'],
            // ['name' => 'г. Челябинск'],
            // ['name' => 'г. Омск'],
            // ['name' => 'г. Самара'],
            // ['name' => 'г. Ростов-на-Дону'],
            // ['name' => 'г. Уфа'],
            // ['name' => 'г. Красноярск'],
            // ['name' => 'г. Пермь'],
            // ['name' => 'г. Воронеж'],
            // ['name' => 'г. Волгоград'],
            // ['name' => 'г. Краснодар'],
            // ['name' => 'г. Саратов'],
            // ['name' => 'г. Тюмень'],
            // ['name' => 'г. Тольятти'],
            // ['name' => 'г. Ижевск'],
            // ['name' => 'г. Барнаул'],
            // ['name' => 'г. Ульяновск'],
            // ['name' => 'г. Иркутск'],
            // ['name' => 'г. Хабаровск'],
            // ['name' => 'г. Ярославль'],
            // ['name' => 'г. Владивосток'],
            // ['name' => 'г. Махачкала'],
            // ['name' => 'г. Томск'],
            // ['name' => 'г. Оренбург'],
            // ['name' => 'г. Кемерово'],
            // ['name' => 'г. Новокузнецк'],
            // ['name' => 'г. Рязань'],
            // ['name' => 'г. Астрахань'],
            // ['name' => 'г. Набережные Челны'],
            // ['name' => 'г. Пенза'],
            // ['name' => 'г. Липецк'],
            // ['name' => 'г. Киров'],
            // ['name' => 'г. Чебоксары'],
            // ['name' => 'г. Тула'],
            // ['name' => 'г. Калининград'],
            // ['name' => 'г. Балашиха'],
            // ['name' => 'г. Курск'],
            // ['name' => 'г. Севастополь'],
            // ['name' => 'г. Улан-Удэ'],
            // ['name' => 'г. Ставрополь'],
            // ['name' => 'г. Сочи'],
            // ['name' => 'г. Тверь'],
            // ['name' => 'г. Магнитогорск'],
            // ['name' => 'г. Иваново'],
            // ['name' => 'г. Брянск'],
            // ['name' => 'г. Белгород'],
            // ['name' => 'г. Сургут'],
            // ['name' => 'г. Владимир'],
            // ['name' => 'г. Нижний Тагил'],
            // ['name' => 'г. Архангельск'],
            // ['name' => 'г. Чита'],
            // ['name' => 'г. Симферополь'],
            // ['name' => 'г. Калуга'],
            // ['name' => 'г. Смоленск'],
            // ['name' => 'г. Волжский'],
            // ['name' => 'г. Саранск'],
            // ['name' => 'г. Курган'],
            // ['name' => 'г. Череповец'],
            // ['name' => 'г. Орёл'],
            // ['name' => 'г. Вологда'],
            // ['name' => 'г. Якутск'],
            // ['name' => 'г. Владикавказ'],
            // ['name' => 'г. Подольск'],
            // ['name' => 'г. Грозный'],
            // ['name' => 'г. Мурманск'],
            // ['name' => 'г. Тамбов'],
            // ['name' => 'г. Стерлитамак'],
            // ['name' => 'г. Петрозаводск'],
            // ['name' => 'г. Кострома'],
            // ['name' => 'г. Нижневартовск'],
            // ['name' => 'г. Новороссийск'],
            // ['name' => 'г. Йошкар-Ола'],
            // ['name' => 'г. Химки'],
            // ['name' => 'г. Таганрог'],
            // ['name' => 'г. Комсомольск-на-Амуре'],
            // ['name' => 'г. Сыктывкар'],
            // ['name' => 'г. Нальчик'],
            // ['name' => 'г. Нижнекамск'],
            // ['name' => 'г. Шахты'],
            // ['name' => 'г. Дзержинск'],
            // ['name' => 'г. Братск'],
            // ['name' => 'г. Орск'],
            // ['name' => 'г. Энгельс'],
            // ['name' => 'г. Ангарск'],
            // ['name' => 'г. Благовещенск'],
            // ['name' => 'г. Старый Оскол'],
            // ['name' => 'г. Королёв'],
            // ['name' => 'г. Великий Новгород'],
            // ['name' => 'г. Мытищи'],
            // ['name' => 'г. Псков'],
            // ['name' => 'г. Люберцы'],
            // ['name' => 'г. Бийск'],
            // ['name' => 'г. Южно-Сахалинск'],
            // ['name' => 'г. Прокопьевск'],
            // ['name' => 'г. Армавир'],
            // ['name' => 'г. Балаково'],
            // ['name' => 'г. Рыбинск'],
            // ['name' => 'г. Абакан'],
            // ['name' => 'г. Северодвинск'],
            // ['name' => 'г. Петропавловск-Камчатский'],
            // ['name' => 'г. Норильск'],
            // ['name' => 'г. Уссурийск'],
            // ['name' => 'г. Волгодонск'],
            // ['name' => 'г. Сызрань'],
            // ['name' => 'г. Каменск - Уральский'],
            // ['name' => 'г. Новочеркасск'],
            // ['name' => 'г. Златоуст'],
            // ['name' => 'г. Красногорск'],
            // ['name' => 'г. Электросталь'],
            // ['name' => 'г. Альметьевск'],
            // ['name' => 'г. Салават'],
            // ['name' => 'г. Миасс'],
            // ['name' => 'г. Керчь'],
            // ['name' => 'г. Находка'],
            // ['name' => 'г. Копейск'],
            // ['name' => 'г. Пятигорск'],
            // ['name' => 'г. Рубцовск'],
            // ['name' => 'г. Березники'],
            // ['name' => 'г. Коломна'],
            // ['name' => 'г. Майкоп'],
            // ['name' => 'г. Хасавюрт'],
            // ['name' => 'г. Одинцово'],
            // ['name' => 'г. Ковров'],
            // ['name' => 'г. Кисловодск'],
            // ['name' => 'г. Домодедово'],
            // ['name' => 'г. Нефтекамск'],
            // ['name' => 'г. Нефтеюганск'],
            // ['name' => 'г. Батайск'],
            // ['name' => 'г. Новочебоксарск'],
            // ['name' => 'г. Серпухов'],
            // ['name' => 'г. Щёлково'],
            // ['name' => 'г. Новомосковск'],
            // ['name' => 'г. Дербент'],
            // ['name' => 'г. Первоуральск'],
            // ['name' => 'г. Черкесск'],
            // ['name' => 'г. Орехово-Зуево'],
            // ['name' => 'г. Назрань'],
            // ['name' => 'г. Невинномысск'],
            // ['name' => 'г. Кызыл'],
            // ['name' => 'г. Каспийск'],
            // ['name' => 'г. Раменское'],
            // ['name' => 'г. Димитровград'],
            // ['name' => 'г. Обнинск'],
            // ['name' => 'г. Новый Уренгой'],
            // ['name' => 'г. Октябрьский'],
            // ['name' => 'г. Камышин'],
            // ['name' => 'г. Муром'],
            // ['name' => 'г. Долгопрудный'],
            // ['name' => 'г. Ессентуки'],
            // ['name' => 'г. Новошахтинск'],
            // ['name' => 'г. Жуковский'],
            // ['name' => 'г. Северск'],
            // ['name' => 'г. Ноябрьск'],
            // ['name' => 'г. Евпатория'],
            // ['name' => 'г. Артем'],
            // ['name' => 'г. Пушкино'],
            // ['name' => 'г. Ачинск'],
            // ['name' => 'г. Елец'],
            // ['name' => 'г. Арзамас'],
            // ['name' => 'г. Реутов'],
            // ['name' => 'г. Бердск'],
            // ['name' => 'г. Сергиев Посад'],
            // ['name' => 'г. Элиста'],
            // ['name' => 'г. Ногинск'],
            // ['name' => 'г. Новокуйбышевск'],
            // ['name' => 'г. Железногорск']
        ];

        foreach ($items as $item) {
            CityModel::insert([
                'name' => $item['name'],
                'state' => $item['state']
            ]);
        }
    }
}
