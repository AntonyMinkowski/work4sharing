<?php

use Illuminate\Database\Seeder;

class FuelTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
        [
            'name' => 'part-time',
            'slug' => 'part-time'
        ],[
            'name' => 'full-time',
            'slug' => 'full-time'
        ],[
            'name' => 'fixed-term',
            'slug' => 'fixed-term'
        ],[
            'name' => 'contract',
            'slug' => 'contract'
        ],[  
            'name' => 'shift',
            'slug' => 'shift'
        ],[
            'name' => 'piece-rate',
            'slug' => 'piece-rate'
        ]];

        foreach ($items as $item) {
            DB::table('fuel_types')->insert([
                'name' => $item['name'],
                'slug' => $item['slug']
            ]);
        }
    }
}
