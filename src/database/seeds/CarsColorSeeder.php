<?php

use Illuminate\Database\Seeder;
use App\Models\CarsColor;
class CarsColorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['name'=>'entry level award'],
            ['name'=>'entry level certificate (ELC)'],
            ['name'=>'entry level diploma'],
            ['name'=>'entry level English for speakers of other languages (ESOL)'],
            ['name'=>'entry level essential skills'],
            ['name'=>'entry level functional skills'],
            ['name'=>'entry level Skills for Life'],
            ['name'=>'level 1 first certificate'],
            ['name'=>'level 1 GCSE - grades 3, 2, 1 or grades D, E, F, G'],
            ['name'=>'level 1 award'],
            ['name'=>'level 1 certificate'],
            ['name'=>'level 1 diploma'],
            ['name'=>'level 1 ESOL'],
            ['name'=>'level 1 essential skills'],
            ['name'=>'level 1 functional skills'],
            ['name'=>'level 1 national vocational qualification (NVQ)'],
            ['name'=>'level 1 music grades 1, 2 and 3'],
            ['name'=>'level 2 CSE - grade 1'],
            ['name'=>'level 2 GCSE - grades 9, 8, 7, 6, 5, 4 or grades A*, A, B, C'],
            ['name'=>'level 2 intermediate apprenticeship'],
            ['name'=>'level 2 award'],
            ['name'=>'level 2 certificate'],
            ['name'=>'level 2 diploma'],
            ['name'=>'level 2 ESOL'],
            ['name'=>'level 2 essential skills'],
            ['name'=>'level 2 functional skills'],
            ['name'=>'level 2 national certificate'],
            ['name'=>'level 2 national diploma'],
            ['name'=>'level 2 NVQ'],
            ['name'=>'level 2 music grades 4 and 5'],
            ['name'=>'level 2 O level - grade A, B or C'],
            ['name'=>'level 3 A level'],
            ['name'=>'level 3 access to higher education diploma'],
            ['name'=>'level 3 advanced apprenticeship'],
            ['name'=>'level 3 applied general'],
            ['name'=>'level 3 AS level'],
            ['name'=>'level 3 international Baccalaureate diploma'],
            ['name'=>'level 3 award'],
            ['name'=>'level 3 certificate'],
            ['name'=>'level 3 diploma'],
            ['name'=>'level 3 ESOL'],
            ['name'=>'level 3 national certificate'],
            ['name'=>'level 3 national diploma'],
            ['name'=>'level 3 NVQ'],
            ['name'=>'level 3 music grades 6, 7 and 8'],
            ['name'=>'level 3 tech level'],
            ['name'=>'level 4 certificate of higher education (CertHE)'],
            ['name'=>'level 4 higher apprenticeship'],
            ['name'=>'level 4 higher national certificate (HNC)'],
            ['name'=>'level 4 award'],
            ['name'=>'level 4 certificate'],
            ['name'=>'level 4 diploma'],
            ['name'=>'level 4 NVQ'],
            ['name'=>'level 5 diploma of higher education (DipHE)'],
            ['name'=>'level 5 foundation degree'],
            ['name'=>'level 5 higher national diploma (HND)'],
            ['name'=>'level 5 award'],
            ['name'=>'level 5 certificate'],
            ['name'=>'level 5 diploma'],
            ['name'=>'level 5 NVQ'],
            ['name'=>'level 6 degree apprenticeship'],
            ['name'=>'level 6 degree with honours '],
            ['name'=>'level 6 graduate certificate'],
            ['name'=>'level 6 graduate diploma'],
            ['name'=>'level 6 award'],
            ['name'=>'level 6 certificate'],
            ['name'=>'level 6 diploma'],
            ['name'=>'level 6 NVQ'],
            ['name'=>'level 6 ordinary degree without honours'],
            ['name'=>'level 7 integrated master’s degree'],
            ['name'=>'level 7 award'],
            ['name'=>'level 7 certificate'],
            ['name'=>'level 7 diploma'],
            ['name'=>'level 7 NVQ'],
            ['name'=>'level 7 master’s degree'],
            ['name'=>'level 7 postgraduate certificate'],
            ['name'=>'level 7 postgraduate certificate in education (PGCE)'],
            ['name'=>'level 7 postgraduate diploma'],
            ['name'=>'level 8 doctorate'],
            ['name'=>'level 8 award'],
            ['name'=>'level 8 certificate'],
            ['name'=>'level 8 diploma']
        ];

        foreach ($items as $item) {
            CarsColor::insert([
                'name' => $item['name']
            ]);
        }
    }
}
