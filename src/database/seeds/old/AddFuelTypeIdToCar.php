<?php

use Illuminate\Database\Seeder;

class AddFuelTypeIdToCar extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (\App\Models\Car::all() as $car) {
            $car->fuel_type_id = rand(1, 4);
            $car->save();
        }
    }
}
