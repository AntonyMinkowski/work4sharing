<?php

use Illuminate\Database\Seeder;

class UserEventPushType extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_event_push_type')->insert([
            'name' => 'На телефон смс',
        ]);

        DB::table('user_event_push_type')->insert([
            'name' => 'На электронку письмо',
        ]);

        DB::table('user_event_push_type')->insert([
            'name' => 'В приложение',
        ]);
    }
}
