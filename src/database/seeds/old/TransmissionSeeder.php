<?php

use Illuminate\Database\Seeder;

class TransmissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('car_transmissions')->insert([
            'name' => 'Задний привод',
            'slug' => 'RWD'
        ]);

        DB::table('car_transmissions')->insert([
            'name' => 'Передний привод',
            'slug' => 'FWD'
        ]);

        DB::table('car_transmissions')->insert([
            'name' => 'Полный привод(4WD)',
            'slug' => '4WD'
        ]);

        DB::table('car_transmissions')->insert([
            'name' => 'Полный привод(AWD)',
            'slug' => 'AWD'
        ]);
    }
}
