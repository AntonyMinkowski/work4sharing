<?php

use Illuminate\Database\Seeder;

class StaffRole extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('role')->insert([
            'name' => 'Админ',
        ]);

        DB::table('role')->insert([
            'name' => 'ТОП',
        ]);

        DB::table('role')->insert([
            'name' => 'Служба безопасности',
        ]);

        DB::table('role')->insert([
            'name' => 'Аккаунт-менеджер',
        ]);

        DB::table('role')->insert([
            'name' => 'Маркетолог',
        ]);

        DB::table('role')->insert([
            'name' => 'Бухгалтерия',
        ]);

    }
}
