<?php

use Illuminate\Database\Seeder;
use App\Models\RentPlace;

class RentPlaceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RentPlace::insert([
            'name' => 'аэропорт Шереметьево',
        ]);
        RentPlace::insert([
            'name' => 'аэропорт Внуково',
        ]);
        RentPlace::insert([
            'name' => 'аэропорт Домодедово',
        ]);
        RentPlace::insert([
            'name' => 'аэропорт Жуковский',
        ]);
        RentPlace::insert([
            'name' => 'Казанский вокзал',
        ]);
        RentPlace::insert([
            'name' => 'Ярославский вокзал',
        ]);
        RentPlace::insert([
            'name' => 'Павелецкий вокзал',
        ]);
        RentPlace::insert([
            'name' => 'Белорусский вокзал',
        ]);
    }
}
