<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CustomerVerifyStatusSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['stage_name' => 'На проверке'],
            ['stage_name' => 'Допущен'],
            ['stage_name' => 'Не допущен'],
            ['stage_name' => 'Не читаемый паспорт'],
            ['stage_name' => 'Не читаемые водительские права'],
            ['stage_name' => 'Не паспорт на селфи'],
        ];
        
        foreach ($items as $item) {
            DB::table('customer_verify_status')->insert($item);
        }
    }
}
