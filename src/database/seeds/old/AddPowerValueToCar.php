<?php

use Illuminate\Database\Seeder;

class AddPowerValueToCar extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (\App\Models\Car::all() as $car) {
            $car->power = rand(100, 300);
            $car->engine_volume = rand(1, 5);
            $car->save();
        }
    }
}
