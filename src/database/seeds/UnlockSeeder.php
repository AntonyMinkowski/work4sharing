<?php

use Illuminate\Database\Seeder;
use App\Models\CarUnlockType;

class UnlockSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['name' => 'direct employee'],
            ['name' => 'direct company'],
            ['name' => 'Service Connect'],
        ];

        foreach ($brands as $brand) {
            CarUnlockType::insert([
                'name' => $item['name'],
            ]);
        }
    }
}
