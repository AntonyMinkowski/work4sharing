<?php

use Illuminate\Database\Seeder;
use App\Models\Extra;

class ExtraSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [

            // ['name' => 'Чистка и мойка после поездки',
            // 'desc' => 'После поездки вы можете сдать автомобиль как есть, владелец сам позаботится о мойке и чистке салона'
            // ],[
            // 'name' => 'Десткое кресло',
            // 'desc' => 'Владелец добавит в автомобиль детское кресло'
            // ],[
            // 'name' => 'Полная заправка перед поездкой',
            // 'desc' => 'Перед поездкой владелец заправит полный бак автомобиля'
            // ],[
            // 'name' => 'GPS навигатор',
            // 'desc' => 'Владелец добавит в автомобиль GPS навигатор'
            // ],[
            // 'name' => 'Адаптер зарядки',
            // 'desc' => ''
            // ],[
            // 'name' => 'Точка доступа WI-FI',
            // 'desc' => 'Владелец добавит в автомобиль Точку доступа WI-FI'
            // ],[
            // 'name' => 'Неограченные километры',
            // 'desc' => 'Вы сможете перемещаться на автомобиле без ограничений по суточному или недельному пробегу. Если не подключить эту опцию, вам необходимо будет оплатить каждый километр пробега сверх указанного']
            [
            'name' => 'computer',
            'desc' => 'company computer (laptop)'
            ],       [
            'name' => 'printer',
            'desc' => 'company printer'
            ],       [
            'name' => 'phone',
            'desc' => 'company phone'
            ],       [
            'name' => 'car',
            'desc' => 'company car'
            ],       [
            'name' => 'meeting room',
            'desc' => 'company meeting room'
            ],            
        ];

        foreach ($items as $item) {
            Extra::insert([
                'name' => $item['name'],
                'desc' => $item['desc']
            ]);
        }
    }
}
