<?php

use Illuminate\Database\Seeder;

class FeatureSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
        //     'en_name' => 'air',
        //     'gn_name' => 'Кондиционер',
        // ],[
        //     'en_name' => 'bluetooth',
        //     'gn_name' => 'Bluetooth',
        // ],[
        //     'en_name' => 'audio_input',
        //     'gn_name' => 'AUX',
        // ],[
        //     'en_name' => 'usb_charger',
        //     'gn_name' => 'USB зарядка',
        // ],[
        //     'en_name' => 'child_seat',
        //     'gn_name' => 'Детское сидение',
        // ],[
        //     'en_name' => 'cruise_control',
        //     'gn_name' => 'Круиз контроль',
        // ],[
        //     'en_name' => 'gps',
        //     'gn_name' => 'GPS навигация',
        // ],[
        //     'en_name' => 'heated_seats',
        //     'gn_name' => 'Подогрев сидений',            


            ['en_name' => 'English',
            'gn_name' => 'Englisch',
        ],[
            'en_name' => 'German',
            'gn_name' => 'Deutsch',
        ],[
            'en_name' => 'Work Independently',
            'gn_name' => 'Eigenständig',
        ],[
            'en_name' => 'Communication',
            'gn_name' => 'Kommunikation',
        ],[
            'en_name' => 'Planning',
            'gn_name' => 'Planung',
        ],[
            'en_name' => 'Flexibility',
            'gn_name' => 'Flexibilität',
        ],[
            'en_name' => 'Project Management',
            'gn_name' => 'Projektabwicklung',
        ],[
            'en_name' => 'Consulting',
            'gn_name' => 'Beratung',
        ],[
            'en_name' => 'Negotiation',
            'gn_name' => 'Verhandlungsführung',
        ],[
            'en_name' => 'IT',
            'gn_name' => 'IT-Bereich',
        ],[
            'en_name' => 'Monitoring',
            'gn_name' => 'Surveillance',
        ],[
            'en_name' => 'Analysis',
            'gn_name' => 'Analyse',
        ],[
            'en_name' => 'Optimization',
            'gn_name' => 'Optimierung',
        ],[
            'en_name' => 'Drawing',
            'gn_name' => 'Zeichnen',
        ],[
            'en_name' => 'Management',
            'gn_name' => 'Management',
        ],[
            'en_name' => 'Engagement',
            'gn_name' => 'Engagement',
        ],[
            'en_name' => 'Documentaries',
            'gn_name' => 'Dokumentationen',
        ],[
            'en_name' => 'Initiative',
            'gn_name' => 'Eigeninitiative',
        ],[
            'en_name' => 'Sales',
            'gn_name' => 'Umsatzverantwortung',
        ],[
            'en_name' => 'IT Services',
            'gn_name' => 'IT-Service',
        ],[
            'en_name' => 'Coaching',
            'gn_name' => 'Coaching',
        ],[
            'en_name' => 'Coordination',
            'gn_name' => 'Koordination',
        ],[
            'en_name' => 'MS Excel',
            'gn_name' => 'MS Excel',
        ],[
            'en_name' => 'Innovation',
            'gn_name' => 'Innovation',
        ],[
            'en_name' => 'Customer Services',
            'gn_name' => 'Kundenservice',
        ]];

        foreach ($items as $item) {
            DB::table('cars_feature')->insert([
                'en_name' => $item['en_name'],
                'gn_name' => $item['gn_name']
            ]);
        }
    }
}
