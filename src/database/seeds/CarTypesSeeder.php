<?php

use Illuminate\Database\Seeder;
use App\Models\CarType;

class CarTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['name' => 'Aviation'],
            ['name' => 'Automotive industry'],
            ['name' => 'Agro-industry'],
            ['name' => 'Banking sector'],
            ['name' => 'Biotechnology'],
            ['name' => 'Military service and power structures'],
            ['name' => 'Hotels and catering'],
            ['name' => 'Investments'],
            ['name' => 'Consulting'],
            ['name' => 'Entertainment'],
            ['name' => 'Culture / Art'],
            ['name' => 'Media'],
            ['name' => 'The medicine'],
            ['name' => 'Education'],
            ['name' => 'Real estate and construction'],
            ['name' => 'Information Technology'],
            ['name' => 'Consumer goods'],
            ['name' => 'Robotics and mechanical engineering'],
            ['name' => 'Retail'],
            ['name' => 'Sport'],
            ['name' => 'Insurance'],
            ['name' => 'Telecommunications'],
            ['name' => 'Transport'],
            ['name' => 'Tourism'],
            ['name' => 'Heavy industry'],
            ['name' => 'Pharmaceuticals and medical equipment'],
            ['name' => 'Energetics'],
            ['name' => 'E-commerce']
        ];

        foreach ($items as $item) {
            CarType::insert([
                'name' => $item['name']
            ]);
        }
    }
}
