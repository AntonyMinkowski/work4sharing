<?php

use Illuminate\Database\Seeder;
use App\Models\Course;

class CourseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
            'en_name' => 'Work Independently',
            'gn_name' => 'Eigenständig',
            'link' => 'https://www.coursera.org/learn/positive-psychiatry',
            'link2' => 'https://www.coursera.org/learn/learning-how-to-learn?index=prod_all_products_term_optimization',
            ],
            [            
            'en_name' => 'Communication',
            'gn_name' => 'Kommunikation',
            'link' => 'https://www.edx.org/course/communication-skills-and-teamwork-2',
            'link2' => 'https://www.edx.org/course/effective-communication-for-todays-leader',
            ],    [            
            'en_name' => 'Planning',
            'gn_name' => 'Planung',
            'link' => 'https://www.coursera.org/learn/uva-darden-strategic-planning-execution',
            'link2'=> 'https://www.coursera.org/learn/work-smarter-not-harder',
            ],     [            
            'en_name' => 'Flexibility',
            'gn_name' => 'Flexibilität',
            'link' => 'https://www.coursera.org/specializations/managing-innovation-design-thinking',
            'link2'=> 'https://www.coursera.org/specializations/healthcare-administration-management',
            ],     [            
            'en_name' => 'Project Management',
            'gn_name' => 'Projektabwicklung',
            'link' => 'https://www.coursera.org/learn/uva-darden-project-management',
            'link2'=> 'https://www.edx.org/course/project-management-for-development',
            ],     [            
            'en_name' => 'Consulting',
            'gn_name' => 'Beratung',
            'link' => 'https://www.coursera.org/specializations/uva-darden-bcg-pricing-strategy',
            'link2'=> 'https://www.coursera.org/specializations/strategic-management',
            ],     [            
            'en_name' => 'Negotiation',
            'gn_name' => 'Verhandlungsführung',
            'link' => 'https://www.coursera.org/learn/negotiation-skills',
            'link2'=> 'https://www.edx.org/course/negotiation-strategies-and-styles',
            ],     [            
            'en_name' => 'IT',
            'gn_name' => 'IT-Bereich',
            'link' => '',
            'link2'=> '',
            ],     [            
            'en_name' => 'Monitoring',
            'gn_name' => 'Surveillance',
            'link' => 'https://www.coursera.org/learn/incident-response-recovery-risks-sscp',
            'link2'=> 'https://www.edx.org/course/results-based-project-management-monitoring-and-ev',
            ],     [            
            'en_name' => 'Analysis',
            'gn_name' => 'Analyse',
            'link' => 'https://www.coursera.org/learn/analysis-business-problem-iese',
            'link2'=> 'https://www.edx.org/course/data-analysis-essentials',
            ],     [            
            'en_name' => 'Optimization',
            'gn_name' => 'Optimierung',
            'link' => 'https://www.coursera.org/learn/seo-strategies',
            'link2'=> 'https://www.edx.org/course/convex-optimization',
            ],     [            
            'en_name' => 'Drawing',
            'gn_name' => 'Zeichnen',
            'link' => 'https://www.coursera.org/learn/interactive-computer-graphics',
            'link2'=> 'https://www.edx.org/course/3d-modeling-from-architectural-drawings',
            ],     [            
            'en_name' => 'Management',
            'gn_name' => 'Management',
            'link' => 'https://www.coursera.org/specializations/project-management',
            'link2'=> 'https://www.edx.org/micromasters/louvainx-management',
            ],     [            
            'en_name' => 'Engagement',
            'gn_name' => 'Engagement',
            'link' => 'https://www.coursera.org/learn/nurture-market-strategies',
            'link2'=> 'https://www.edx.org/course/community-engagement-collaborating-for-change',
            ], [            
            'en_name' => 'Documentation',
            'gn_name' => 'Dokumentationen',
            'link' => 'https://www.coursera.org/learn/planning-auditing-maintaining-enterprise-systems',
            'link2'=> 'https://www.edx.org/course/ma-structuring-the-deal',
            ], [            
            'en_name' => 'Initiative',
            'gn_name' => 'Eigeninitiative',
            'link' => 'https://www.coursera.org/learn/strategic-innovation-innovation-at-the-frontier',
            'link2'=> 'https://www.edx.org/course/leading-the-organization',
            ], [            
            'en_name' => 'Sales',
            'gn_name' => 'Umsatzverantwortung',
            'link' => 'https://www.coursera.org/learn/build-sales-career',
            'link2'=> 'https://www.edx.org/professional-certificate/queensx-enterprise-sales',
            ], [            
            'en_name' => 'IT-Service',
            'gn_name' => 'IT Services',
            'link' => 'https://www.coursera.org/learn/system-administration-it-infrastructure-services',
            'link2'=> 'https://www.edx.org/course/cloud-computing-infrastructure',
            ],   [            
            'en_name' => 'Coaching',
            'gn_name' => 'Coaching',
            'link' => 'https://www.coursera.org/specializations/coaching-skills-manager',
            'link2'=> 'https://www.edx.org/course/coaching-skills-for-educators',
            ],   [            
            'en_name' => 'Coordination',
            'gn_name' => 'Koordination',
            'link' => 'https://www.coursera.org/specializations/agile-development',
            'link2'=> 'https://www.edx.org/course/driving-speed-through-agile-planning-2',
            ],   [            
            'en_name' => 'MS Excel',
            'gn_name' => 'MS Excel',
            'link' => 'https://www.coursera.org/specializations/excel',
            'link2'=> 'https://www.edx.org/course/microsoft-office-fundamentals-outlook-word-and-excel-5',
            ],   [            
            'en_name' => 'Innovation',
            'gn_name' => 'Innovation',
            'link' => 'https://www.coursera.org/specializations/managing-innovation-design-thinking',
            'link2'=> 'https://www.edx.org/course/design-thinking-and-creativity-for-innovation',
            ], [            
            'en_name' => 'Customer Services',
            'gn_name' => 'Kundenservice',
            'link' => 'https://www.coursera.org/learn/ibm-customer-engagement-specialist',
            'link2'=> 'https://www.edx.org/course/it-support-fundamentals',
            ],           
        ];

        foreach ($items as $item) {
            Course::insert([
                'en_name' => $item['en_name'],
                'gn_name' => $item['gn_name'],
                'link' => $item['link'],
                'link2' => $item['link2']
                // 'desc' => $item['desc']
            ]);
        }
    }
}
     
     
       
    
            
        
      
        
         