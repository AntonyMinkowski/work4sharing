<?php

use Illuminate\Database\Seeder;
use App\Models\CarTransmission;

class CarTransmissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
                'name' => 'remote',
                'slug' => 'Remote'
            ],         
            [
                'name' => 'office',
                'slug' => 'Office'
            ],
        ];

        foreach ($items as $item) {
            CarTransmission::insert([
                'name' => $item['name'],
                'slug' => $item['slug']
            ]);
        }
    }
}
