<?php

use Illuminate\Database\Seeder;
use App\Models\ModelsCar;
use App\Models\BrandsCar;

class ModelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['name' => 'junior'],
            ['name' => 'middle'],
            ['name' => 'senior'],
            ['name' => 'lead'],
        ];

        // $brands = BrandsCar::all();

        // foreach ($brands as $brand) {
            foreach ($items as $item) {
                ModelsCar::insert([
                    'name' => $item['name'],
                    // 'brand_id' => $brand['id'],
                ]);
            }
        // }
    }
}
