<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $classes = [
            City::class,
            // ReRunPriceSeeder::class,
            FuelTypeSeeder::class,
            CarTransmissionsSeeder::class,
            // BookingStatusSeeder::class,
            BrandCarsSeeder::class,
            ModelSeeder::class,
            // CarBansSeeder::class,
            CarsColorSeeder::class,
            // CarsRefusalSeeder::class,
            CarTypesSeeder::class,
            // CustomerVerifyStatusSeed::class,
            ExtraSeeder::class,
            CourseSeeder::class,
            ExtraTimeSeeder::class,
            CourseTimeSeeder::class,
            FeatureSeeder::class,
            // MileageInclusiveSeed::class,
            // MileageInclusiveSeeder::class,
            // RentPlaceSeeder::class,
            // RentTimeSeeder::class,
            // StaffModule::class,
            // StaffRole::class,
            // TimeAlertOwenRentSeed::class,
            // UserEventPushType::class,
            // UsersRefusalSeeder::class
        ];

        foreach ($classes as $class) {
            $this->call($class);
        }
    }
}
