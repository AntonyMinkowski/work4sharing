<?php
/**
 * Created by PhpStorm.
 * User: maksim
 * Date: 02.02.19
 * Time: 16:08
 */

namespace Library\Connectors;

use Illuminate\Support\Facades\DB;

class navixy
{
    private $hash = "";

    private function Auth () {
        $resp = file_get_contents("http://api.navixy.com/v2/user/auth?login=mezhonis@verberconsult.com&password=123456");

        $data = json_decode($resp, 1);

        if ($data['success']) {
            $this->hash = $data['hash'];

            DB::table('navixy')
                ->where('user_id', '0')
                ->update(['hash' => $this->hash]
                );
        }


    }

    private function getHash() {
        $getHash = DB::table('navixy')
            ->where('user_id', '0')
            ->get();

        return $getHash[0]->hash;
    }

    public function getLocationCar ($tracker_id) {
        $data = array(
            'tracker_id' => $tracker_id,
            'hash' => $this->getHash()
        );

        $query = "https://api.navixy.com/v2/tracker/get_state?" . http_build_query($data);


        $ch = curl_init($query);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, false);
        $resp = curl_exec($ch);
        curl_close($ch);

        $data = json_decode($resp, 1);

        if (!$data['success']) {
            if ($data['status']['code'] == 4 || $data['status']['code'] == 3) {
                $this->Auth();
                return $this->getLocationCar($tracker_id);
            }
        } else {
            $dataCar = array();

            $dataCar['location']['lat'] = $data['state']['gps']['location']['lat'];
            $dataCar['location']['lng'] = $data['state']['gps']['location']['lng'];
            $dataCar['speed'] = $data['state']['gps']['speed'];
            $dataCar['connection_status'] = $data['state']['connection_status'];

            return $dataCar;
        }
    }

    public function getTracking ($tracker_id, $from, $to) {

        $data = array(
            'tracker_id' => $tracker_id,
            'from' => $from,
            'to' => $to,
            'hash' => $this->getHash()
        );

        $query = "https://api.navixy.com/v2/track/list?" . http_build_query($data);

        $ch = curl_init($query);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, false);
        $resp = curl_exec($ch);
        curl_close($ch);

        $data = json_decode($resp, 1);

        if (!$data['success']) {
            if ($data['status']['code'] == 4) {
                $this->Auth();
                return $this->getTracking($tracker_id);
            }
        } else {
            $trackingLength = 0.0;
            foreach ($data['list'] as $key => $value) {
                $trackingLength = $trackingLength + $value['length'];
            }

            $data['trackingLength'] = $trackingLength;

            return $data;
        }
    }
}
