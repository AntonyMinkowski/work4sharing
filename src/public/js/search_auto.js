$(document).ready(
    $(function() {
        $(document).on("click", ".checkbox_filter", (function(evt) {
            if (this.children[0].checked !== true) {
                this.children[0].setAttribute("checked", "checked");
                this.children[0].checked = true;
                $(this).addClass("active");
            } else {
                this.children[0].setAttribute("checked", "");
                this.children[0].checked = false;
                $(this).removeClass("active");
            }
        }));
    })
);
