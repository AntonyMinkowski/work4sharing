<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Forms Reset Language Lines
    |--------------------------------------------------------------------------
    |
    */

    // '1' => 'О нас',
    // '2' => 'Мы создаем современное сообщество, которое делает автомобиль доступным и полезным для каждого участника.',
    // '3' => 'Москва, пр-д Хлебозаводский, д. 7, стр. 9',
    // '4' => 'Узнай больше',
    // '5' => 'Главная',
    // '6' => 'О нас',
    // '7' => 'Арендовать',
    // '8' => 'Поделиться авто',
    // '9' => 'Контакты',
    // '10' => 'Гайд для владельца',
    // '11' => 'Гайд для арендатора',
    // '12' => '2019 (c) Россия, Москва',
    // '13' => 'Пользовательское соглашение',
    // '14' => 'Политика конфиденциальности',
    // '15' => 'Поиск',
    // // '16' => 'Сообщения',
    // '17' => 'Мои поездки',
    // '18' => 'Мои авто',
    // '19' => 'Войти',
    // '20' => 'Регистрация',
    // '21' => 'Профиль',
    // '22' => 'Выйти',


    '1' => 'About us',
    '2' => 'Work4Sharing - Data-driven online platform helping German companies to find friendly partner for sharing employees during economic stagnation.
We suggest service for employers stressed by losing business activity and unable to pay full salaries because of Covid 19 -to share their employees with the partner companies which need temporary specialists of the same position or can be short term trained for close position.',
    // '2' => 'We create modern a community that makes the car accessible and useful for everyone involved.',
    '3' => '7 Khlebozavodsky Ave., p. 9, Moscow',
    '4' => 'Learn more',
    '5' => 'Home',
    '6' => 'About us',
    '7' => 'Rent an employee',
    '8' => 'Share an employee',
    '9' => 'Contacts',
    '10' => 'Guide for the share company',
    '11' => 'Guide for the rent company',
    '12' => '209 (c) Russia, Moscow',
    '13' => 'User agreement',
    '14' => 'Privacy policy',
    '15' => 'Search',
    // '16' => 'Messages',
    '17' => 'My hiring',
    '18' => 'My employees',
    '19' => 'The log in',
    '20' => 'Registration',
    '21' => 'Profile',
    '22' => 'Exit',
];




