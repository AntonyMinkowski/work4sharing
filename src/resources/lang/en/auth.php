<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

    '1' => 'Login',
    '2' => 'type E-mail',
    '3' => 'type E-mail',
    '4' => 'type password',
    '5' => 'type password',
    '6' => 'Remember me',
    '7' => 'Remind password',
    '8' => 'Еще нет аккаунта?',
    '9' => 'Sign up',


    '10' => 'Добавьте фото паспорта страницы с фотографией',
    '11' => 'Добавьте фото паспорта страницы с пропиской',
    '12' => 'Добавьте фото водительского удостоверения с лицевой стороны',
    '13' => 'Добавьте фото паспорта страницы с обратной стороны',
    '14' => 'Добавьте своё селфи с паспортом',
    '15' => 'Ваш профиль находится на проверке',



    '16' => 'Sign up',
    '17' => 'Username',
    '18' => 'E-mail',
    '19' => 'type E-mail',
    '31' => 'type username',
    '20' => 'Password',
    '21' => 'type password',
    '22' => 'repeat password',
    '23' => 'type password',
    '24' => 'Sign up',
    '25' => 'Register',
    '26' => 'Username',
    '27' => 'E-Mail Address',
    '28' => 'Password',
    '29' => 'Confirm Password',
    '30' => 'Register',


    '32' => 'Подтверждение Email адреса',
    '33' => 'Письмо было отправлено на твой Email.',
    '34' => 'На твой Email пришло письмо со ссылкой. Пожалуйста проверь почту и перейди по ссылке из письма. Письмо не пришло?',
    '35' => 'Нажми чтобы отправить снова.',
    

    '36' => 'reset password',
    '37' => 'reset password',
    '38' => 'type E-mail',
    '39' => 'type E-mail',
    '40' => 'Reset',


    '41' => 'reset password',
    '42' => 'reset password',
    '43' => 'E-mail',
    '44' => 'E-mail',
    '45' => 'new password',
    '46' => 'new password',
    '47' => 'new password',
    '48' => 'new password',
    '49' => 'reset password',
];
