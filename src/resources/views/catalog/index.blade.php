@extends('layouts.app')

@push('scripts')
    <script src="{{ asset('js/search_auto.js') }}"></script>
    <script src="{{ asset('js/get_models.js') }}"></script>
@endpush

@section('content')
<div id="catalog">
  <catalog 
  @isset($from)
  from="{{$from}}"
  @endisset
  @isset($to)
    to="{{$to}}"
  @endisset
  @isset($coordinates)
  coordinates="{{$coordinates}}"
  @endisset
  ></catalog>
</div>

@endsection
