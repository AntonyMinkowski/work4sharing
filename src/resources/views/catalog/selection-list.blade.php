@extends('layouts.app') 
@section('content')

<div class="container">
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Фильтры</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
      aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">

        <li class="nav-item">
          <select class="custom-select" id="brends" onchange="getModels(this);">
          <option selected>Марка</option>
          @foreach ($brandsCars as $item)
            <option value="{{$item->id}}">{{$item->name}}</option>
          @endforeach
        </select>
        </li>
        <li class="nav-item">
          <select class="custom-select" id="models" onchange="setModels(this);">
          <option selected>Модель</option>
        </select>
        </li>
      </ul>
      <div class="btn btn-outline-success my-2 my-sm-0" id="globalsearch">Найти</div>
    </div>
  </nav>

  @foreach ($selectionforcars as $item)
  <a href="/carslist/{{$item->id}}">
    <div class="tile">
      <div class="tile_text">
        {{$item->name}}
      </div>
    </div>
  </a>

  @endforeach

</div>
@endsection