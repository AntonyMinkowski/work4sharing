<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        .day {
            width: 20px;
            justify-content: center;
            border: chocolate 1px solid;
            margin: 2px;
        }
        .dayName {
            width: 20px;
            justify-content: center;
            border: coral 1px solid;
            margin: 2px;
            background-color: coral;
        }
    </style>
</head>

<body>
    <table>
        <tr>
            <td class="dayName">ПН</td>
            <td class="dayName">ВТ</td>
            <td class="dayName">СР</td>
            <td class="dayName">ЧТ</td>
            <td class="dayName">ПТ</td>
            <td class="dayName">СБ</td>
            <td class="dayName">ВС</td>
        </tr>
        <tr>
            <td class="day">1</td>
            <td class="day">2</td>
            <td class="day">3</td>
            <td class="day">4</td>
            <td class="day">5</td>
            <td class="day">6</td>
            <td class="day">7</td>
        </tr>
    </table>
</body>

</html>