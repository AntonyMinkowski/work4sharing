<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="Description" content="Сделаем автомобиль доступным и полезным для каждого."/>

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ config('app.name', 'Laravel') }}</title>



	<!-- Styles -->
	<link href="{{ asset('css/home.css') }}" rel="stylesheet">

	@stack('scripts')
</head>

<body class="@yield('body_class')">
	<div id="menu" class="slideout-menu">
		<ul>
			<li class="{{ request()->is('/') ? 'active' : '' }}"><a href="/">{{trans('layout.5')}}</a></li>
			<li class="{{ Request::routeIs('how') ? 'active' : '' }}"><a href="{{route('how')}}">{{trans('layout.6')}}</a></li>
			<!-- <li class="{{ Request::routeIs('booking') ? 'active' : '' }}"><a href="{{route('booking')}}">{{trans('layout.7')}}</a></li> -->
			<li class="{{ Request::routeIs('car.create.show') ? 'active' : '' }}"><a href="{{route('car.create.show')}}">{{trans('layout.8')}}</a></li>
			<li class="{{ Request::routeIs('contact') ? 'active' : '' }}"><a href="{{route('contact')}}">{{trans('layout.9')}}</a></li>
		</ul>
	</div>

	<div id="panel" data-slideout-ignore>
		<!--BEGIN header-->
		<header class="header">
			<div class="container">
				<div class="row align-items-center h-100 flex-row-reverse flex-sm-row">
					<div class="col-xl-9 col-md-8 col-6">
						<a href="/" class="logo"><img src="image/logo.png" style="width:200px" alt=""></a>
						<!-- <a href="/" class="logo"><img src="image/logo-white.svg" alt=""></a> -->
					</div>
					<div class="col-6 d-block d-sm-none">
						<div class="open-menu"><i class="icon icon-menu"></i></div>
					</div>
					<div class="col-xl-3 col-md-4 d-none d-sm-block">
						<nav class="header__menu">
							<ul>
						<!-- 		{{--<li class="{{ request()->is('/') ? 'active' : '' }}"><a href="/">Главная</a></li>--}}
								{{--<li class="{{ Request::routeIs('how') ? 'active' : '' }}"><a href="{{route('how')}}">О нас</a></li>--}} -->
								<!-- <li class="{{ Request::routeIs('booking') ? 'active' : '' }}"><a href="{{route('booking')}}">{{trans('layout.7')}}</a></li> -->
								<li style="border: 1px solid white; padding:10px 20px; border-radius:15px;" class="{{ Request::routeIs('car.create.show') ? 'active' : '' }}"><a href="{{route('car.create.show')}}">{{trans('layout.8')}}</a></li>
						<!-- 		{{--<li class="{{ Request::routeIs('contact') ? 'active' : '' }}"><a href="{{route('contact')}}">Контакты</a></li>--}} -->
								@if (Auth::check())
								<li class="menu">
										<a href="#"><i style="font-size: 41px;" class="icon icon-user"></i></a>
										<div class="menu__hover">
											<a href="/profile">{{trans('layout.21')}}</a>
											<a href="/logout">{{trans('layout.22')}}</a>
										</div>
								</li>
								@else
								<li class="menu">
										<a href="#"><i class="icon icon-user"></i></a>
										<div class="menu__hover">
											<a href="{{route('login')}}">{{trans('layout.19')}}</a>
											<a href="{{route('register')}}">{{trans('layout.20')}}</a>
										</div>
								</li>
								@endif
							
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</header>
		<!--END header-->
		<main>
			@yield('content')
		</main>


		    <!--BEGIN footer-->
    <footer class="footer">
        <div class="footer__top">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 d-flex flex-column">
                        <h2>{{trans('layout.1')}}</h2>
                        <p>{{trans('layout.2')}} </p>
                        <p class="last">{{trans('layout.3')}}</p>
                    </div>
                    <div class="col-md-4 offset-md-2 d-flex flex-wrap justify-content-between">
                        <h2 class="w-100">{{trans('layout.4')}}</h2>
                        <ul>
                            <li class="{{ request()->is('/') ? 'active' : '' }}"><a href="/">{{trans('layout.5')}}</a></li>
                            <li class="{{ Request::routeIs('how') ? 'active' : '' }}" 
                            	><a href="{{route('how')}}">{{trans('layout.6')}}</a></li>
                            <li class="{{ Request::routeIs('booking') ? 'active' : '' }}"><a href="{{route('booking')}}">{{trans('layout.7')}}</a></li>
                            <li class="{{ Request::routeIs('handOver') ? 'active' : '' }}"><a href="{{route('handOver')}}">{{trans('layout.8')}}</a></li>
                            <li class="{{ Request::routeIs('contact') ? 'active' : '' }}"><a href="{{route('contact')}}">{{trans('layout.9')}}</a></li>
                        </ul>
                        <ul>
                            <li><a href="{{ route('info') }}">{{trans('layout.10')}}</a></li>
                            <li><a href="#">{{trans('layout.11')}}</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer__bottom">
            <div class="container">
                <div class="row">
                    <div class="col d-flex justify-content-between flex-column flex-sm-row">
                        <div class="logo"><img width="400px" src="/image/logo2.png" alt=""></div>
                        <div class="coopyright">{{trans('layout.12')}}</div><a href="/soauto-oferta.pdf">{{trans('layout.13')}}</a><a target="_blank" href="/politika-konf-soauto.pdf">{{trans('layout.14')}}</a>
                        <div class="social"><a href="https://www.facebook.com/soautoru"><i class="icon icon-facebook"></i></a><a href="https://www.instagram.com/so.auto"><i class="icon icon-instagram"></i></a><a href="https://vk.com/soautoru"><i class="icon icon-vk"></i></a></div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
		<!--END footer-->
	</div>
	<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
	<script src="{{ asset('js/home.js') }}" defer></script>

</body>

</html>
