@extends('layouts.app')

@push('scripts')
    <script src="{{ asset('js/register_owner.js') }}"></script>
@endpush

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('form.register_owner_title') }}</div>
                    <div class="card-body">
                            @if(!$personal->passport_face || !$personal->passport_visa)
                                <div>
                                    <form method="post" action="{{ route('user.set_passport') }}" id="upload_passport" role="form" enctype=multipart/form-data >
                                        @csrf
                                        @unless ($personal->passport_face)
                                        <p>{{trans('auth.10')}}
                                            <br>
                                            <input type="file" name="passport_face">
                                        </p>
                                        @endunless
                                        @unless ($personal->passport_visa)
                                        <p>{{trans('auth.11')}}
                                            <br>
                                            <input type="file" name="passport_visa">
                                        </p>
                                        @endunless
                                        <button id="upload_passport_btn" class="btn btn-success">
                                            {{ __('form.send_passport_images') }}
                                        </button>
                                    </form>
                                </div>
                            <hr>
                            @endif

                            @if(!$personal->driver_lisense_first || !$personal->driver_license_second)
                            <div>
                                <form method="post" action="{{ route('user.set_driving_licence') }}" id="upload_driving_lisense" role="form" enctype=multipart/form-data >
                                    @csrf
                                    @unless ($personal->driver_lisense_first)
                                    <p>{{trans('auth.12')}}
                                        
                                        <br>
                                        <input type="file" name="driver_lisense_first">
                                    </p>
                                    @endunless
                                    @unless ($personal->driver_license_second)
                                    <p>{{trans('auth.13')}}
                                        
                                        <br>
                                        <input type="file" name="driver_license_second">
                                    </p>
                                    @endunless
                                    <button id="upload_passport_btn" class="btn btn-success">
                                        {{ __('form.send_passport_images') }}
                                    </button>
                                </form>
                            </div>
                            <hr>
                            @endif

                            @unless($personal->selfie)
                                <div>
                                    <form method="post" action="{{ route('user.update_selfie') }}" id="upload_selfie" role="form" enctype=multipart/form-data >
                                        @csrf
                                        <p>{{trans('auth.14')}}
                                            
                                            <br>
                                            <input type="file" name="selfie">
                                        </p>
                                        <button id="upload_passport_btn" class="btn btn-success">
                                            {{ __('form.send_passport_images') }}
                                        </button>
                                    </form>
                                </div>
                            @endunless
                            <div class="verify @if (Auth::user()->validate) d-none @endif">{{trans('auth.15')}}
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
