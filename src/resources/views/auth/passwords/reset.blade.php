@extends('layouts.app')

@section('content')

<section class="login">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-8 d-flex align-items-center flex-column">
				<h1 class="login-title">{{ trans('auth.41') }}</h1>

				<form method="POST" action="{{ route('password.request') }}" aria-label="{{ trans('auth.42') }}" style="width: 100%" class="login-form d-flex align-items-center flex-column">
					@csrf
					<input type="hidden" name="token" value="{{ $token }}">

					<div class="login-form__adress">
						<p class="login-form__text">{{ trans('auth.43') }}</p>
						<input id="#" type="email" class="login-form__input{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" placeholder="{{ trans('auth.44') }}" required value="{{ $email ?? old('email') }}">

						@if ($errors->has('email'))
							<p class="login-form__text login-form__text_red">{{ $errors->first('email') }}</p>
						@endif

					</div>

					<div class="login-form__adress">
						<p class="login-form__text">{{ trans('auth.45') }}</p>
						<input id="#" type="password" class="login-form__input{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="{{ trans('auth.46') }}" required >

						@if ($errors->has('password'))
							<p class="login-form__text login-form__text_red">{{ $errors->first('password') }}</p>
						@endif

					</div>

					<div class="login-form__adress">
						<p class="login-form__text">{{ trans('auth.47') }}</p>
						<input id="#" type="password" class="login-form__input" name="password_confirmation" placeholder="{{ trans('auth.48') }}" required>
					</div>

					<button type="submit" class="btn login-form__button">{{ trans('auth.49') }}</button>
				</form>
			</div>
		</div>
	</div>
</section>

@endsection
