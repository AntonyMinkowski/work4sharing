@extends('layouts.app')

@section('content')

<section class="login">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-8 d-flex align-items-center flex-column">
				<h1 class="login-title">{{ trans('auth.36') }}</h1>
				@if (session('status'))
					<div class="alert alert-success" role="alert">
						{{ session('status') }}
					</div>
				@endif
				<form method="POST" action="{{ route('password.email') }}" aria-label="{{ trans('auth.37') }}" style="width: 100%" class="login-form d-flex align-items-center flex-column">
					@csrf
					<div class="login-form__adress">
						<p class="login-form__text">{{ trans('auth.38') }}</p>
						<input id="#" type="email" class="login-form__input{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" placeholder="{{ trans('auth.39') }}" required 
						value="{{ old('email') }}">
		
						@if ($errors->has('email'))
							<span class="invalid-feedback" role="alert">
								<strong>{{ $errors->first('email') }}</strong>
							</span>
						@endif
		
					</div>
					<button type="submit" class="btn login-form__button">{{ trans('auth.40') }}</button>
				</form>
			</div>
		</div>
	</div>
</section>



{{-- <div class="container">
	<div class="row justify-content-center">
		<div class="col-md-8">
			<div class="card">
				<div class="card-header">{{ trans('auth.2') }}</div>

				<div class="card-body">
					@if (session('status'))
						<div class="alert alert-success" role="alert">
							{{ session('status') }}
						</div>
					@endif

					<form method="POST" action="{{ route('password.email') }}" aria-label="{{ trans('auth.2') }}{{ __('Сброс пароля') }}">
						@csrf

						<div class="form-group row">
							<label for="email" class="col-md-4 col-form-label text-md-right">{{ trans('auth.2') }}{{ __('E-Mail адрес') }}</label>

							<div class="col-md-6">
								<input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

								@if ($errors->has('email'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('email') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group row mb-0">
							<div class="col-md-6 offset-md-4">
								<button type="submit" class="btn btn-primary">
									{{ trans('auth.2') }}{{ __('Сбросить') }}
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div> --}}
@endsection
