@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            <div class="verify__block d-flex justify-content-around align-items-center mb-4">
                <h2 class="rent__title">{{ trans('auth.32') }}</h2>
                @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ trans('auth.33') }}
                        </div>
                    @endif
                <p>{{ trans('auth.34') }}
                <br><a href="{{ route('verification.resend') }}">
                {{ trans('auth.35') }}</a>.</p>
            </div>
               
        </div>
    </div>
</div>
@endsection
