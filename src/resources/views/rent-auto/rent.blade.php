@extends('layouts.app')
@section('content')

<div id="car">
    <car
        id="{{$selectionCars[0]->id}}"
        @isset($from)
            from="{{$from}}"
        @endisset
        @isset($to)
            to="{{$to}}"
        @endisset
        @isset($location)
            location="{{$location}}"
        @endisset
    ></car>
</div>

<section class="rent-car">
    <div class="container">
        <div class="row">
            <div class="col">
                <h1 class="about-car__title">
                    @if (isset($selectionCars[0]->brand) && isset($selectionCars[0]->model)) 
                        {{ $selectionCars[0]->brand }} {{ $selectionCars[0]->model }}
                    @endif
                </h1>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-lg-7">
                <div class="img-card1 d-flex" style="overflow: hidden;">
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <?php $i = 0; ?>
                            @foreach ($images as $item)
                                <div class="carousel-item @if($i == 0) {{ 'active' }} @endif">
                                    <div class="img-slider" style="background: url(https://hb.bizmrg.com/soautomedia/{{$item->img_url}}) center center no-repeat;"></div>
                                </div>
                                <?php $i++; ?>
                            @endforeach
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
                <div class="row" style="margin-top: 35px;">
                    <div class="container-icons">
                        <div class="content-card1__wrapper">
                            <div class="content-card1__inner d-flex align-items-center">
                                <div class="content-card1__icon content-card1__icon_transmission car-rent-icon"></div>
                                <p class="content-card1__text car-rent-text">
                                    @isset ($selectionCars[0]->gear){{$selectionCars[0]->gear}}@endisset
                                </p>
                            </div>
                            <div class="content-card1__inner d-flex align-items-center">
                                <div class="content-card1__icon content-card1__icon_event car-rent-icon"></div>
                                <p class="content-card1__text car-rent-text">
                                    @isset ($selectionCars[0]->seat){{$selectionCars[0]->year_of_issue}}@endisset
                                </p>
                            </div>
                        </div>
                        <div class="content-card1__wrapper">
                            <div class="content-card1__inner d-flex align-items-center">
                                <div class="content-card1__icon content-card1__icon_car car-rent-icon"></div>
                                <p class="content-card1__text car-rent-text">
                                    @isset ($selectionCars[0]->seat){{$selectionCars[0]->car_type}}@endisset
                                </p>
                            </div>
                            <div class="content-card1__inner d-flex align-items-center">
                                <div class="content-card1__icon content-card1__icon_people car-rent-icon"></div>
                                <p class="content-card1__text car-rent-text">
                                    @isset ($selectionCars[0]->seat){{$selectionCars[0]->seat}}@endisset
                                </p>
                            </div>
                        </div>
                        <div class="content-card1__wrapper">
                            <div class="content-card1__inner d-flex align-items-center">
                            <div class="content-card1__icon content-card1__icon_door car-rent-icon"></div>
                                <p class="content-card1__text car-rent-text">
                                    @isset ($selectionCars[0]->doors){{$selectionCars[0]->doors}} @endisset
                                </p>
                            </div>  
                            {{-- Пустой блок для заполнения пробела --}}
                            <div class="content-card1__inner d-flex align-items-center" style="height: 28px;">
                            </div>
                        </div>
                    </div>
                </div>
                @isset ($selectionCars[0]->description)
                    <div class="row" style="margin-top: 35px;">
                        <div class="col-12" style="padding: 0;">
                            <h5>Описание авто:</h5>
                            <p>{{$selectionCars[0]->description}}</p> 
                        </div>
                    </div>
                @endisset
            </div>
            <div class="col-12 col-lg-5">
                <div class="container-info">
                    <form method="POST" action="/checkout/@isset ($selectionCars[0]->id){{$selectionCars[0]->id}}@endisset" id="formCheckout">
                        @csrf
                        <div class="row first-column-rent">
                            <div class="col-6">
                                <div class="form-cars__wrap">
                                    <label for="rent_from" class="rent_from_title">
                                        Начало аренды
                                    </label>
                                    <div class="row">
                                        <div class="col-12">
                                            <input 
                                                style="padding: 10px 0 6px 0"
                                                id="minMaxFrom"
                                                name="{{ "from" }}"
                                                class="login-form__input datepicker-here"
                                                data-timepicker="true"
                                                data-date-format="yyyy-mm-dd"
                                                type="text"
                                                required 
                                                autocomplete="off" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-cars__wrap">
                                    <label for="rent_to" class="rent_from_title">
                                        Конец аренды
                                    </label>
                                    <div class="row">
                                        <div class="col-12">
                                            <input 
                                                style="padding: 10px 0 6px 0"
                                                id="minMaxTo"
                                                name="{{ "to" }}"
                                                class="login-form__input datepicker-here"
                                                data-timepicker="true"
                                                data-date-format="yyyy-mm-dd"
                                                type="text"
                                                required 
                                                autocomplete="off" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <span id="error_booking" style="color: red; display: none">Вы выбрали уже забронированные даты</span>
                        @if (isset($bookingDates[0]->from) && isset($bookingDates[0]->to)) 
                            <div class="row" style="margin-top: 15px;">
                                <div class="col-12">
                                    <h5>Даты которые заняты</h5>
                                    <ul>
                                        @foreach ($bookingDates as $bookingDate)
                                        <li>
                                            <span>{{ $bookingDate->from }} - {{ $bookingDate->to }}</span>
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        @endif
                        <div class="row" style="margin-top: 35px;">
                            <div class="form-cars__wrap"  style="width: 100%">
                                <div class="col-12">
                                    <label for="rent_from" class="rent_from_title">
                                        Место получения и возврата
                                    </label>
                                    <div class="form-cars__wrap" >
                                        <select id="location_delivery"
                                        style="max-width: 102%; width: 102%"
                                        onchange="printSumDelivery(this)"
                                        type="checkbox"
                                        required
                                        class="form-cars__select"
                                        name="location_delivery" >
                                            <option>Ничего не выбрано</option>
                                            @isset ($selectionCars[0]->location)
                                                <option
                                                class="form-cars__option" value="{{$selectionCars[0]->location}}">
                                                    {{$selectionCars[0]->location}}
                                                </option>
                                            @endisset
                                            {{-- @foreach ($rentPlace as $item)
                                                <option
                                                class="form-cars__option" value="{{$item->id}}">
                                                    {{$item->name}}
                                                </option>
                                            @endforeach --}}
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 35px;">
                            <div class="col-8">
                                <h5>Включенная дистанция</h5>
                                <p>6,5 руб/км доплата за превышение</p>
                            </div>
                            <div class="col-4">
                                <span id="rusult-calculation-mileage" class="value-distanse">Введите даты</span>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 35px;">
                            <div class="col" style="display: flex;align-items: center;">
                                <div>
                                    @isset ($selectionCars[0]->cost_day)
                                        <span style="font-weight: 600;">{{$selectionCars[0]->cost_day}} </span>
                                    @endisset
                                    руб./сут
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 35px;">
                            <div class="col">
                                <button type="submit" id="booking-submit" class="btn filter-btn" style="padding: 0.375rem 0.75rem;box-shadow: none;">
                                    Забронировать
                                </button>
                            </div>
                        </div>
                        <input type="text" name="selectionCars[]" value="{{$selectionCars}}" hidden>
                        <input type="text" name="included_distance" id="included_distance" hidden>
                        <input type="text" name="number_days" id="number_days" hidden>
                    </form>
                    <div class="row" id="container-sum-delivery" style="display: none; margin-top: 35px; justify-content: space-between;">
                        <h5>Стоимость доставки:</h5>
                        <span id="sum-delivery" style="font-weight: 600;">Не выбрано</span>
                    </div>
                    <div class="row" style="margin-top: 35px;">
                        <div class="distance-included-day">
                            <div>В сутки:</div>
                            <div>@isset ($mileage->day) {{$mileage->day}} @endisset</div>
                        </div>
                        <div class="distance-included-week">
                            <div>В неделю:</div>
                            <div>@isset ($mileage->week) {{$mileage->week}} @endisset</div>
                        </div>
                        <div class="distance-included-month">
                            <div>В месяц:</div>
                            <div>@isset ($mileage->month) {{$mileage->month}} @endisset</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="container-more-settings">
            <div class="row">
                <ul class="list-more-settings">
                    @isset($nameFeature)
                        <?php $i = 0; ?>
                        @foreach ($nameFeature as $key => $value)
                            <li>                
                                <span class="square-more-setting">&#9632;</span>
                                {{$value}}
                            </li>
                            <?php 
                                if ($i % 5 == 0) 
                                {
                                echo `</ul>
                                </div>
                                <div class="row">
                                    <ul class="list-more-settings">`;
                                } 
                                $i++; 
                            ?>
                        @endforeach
                    @endisset
                </ul>
            </div>
        </div>
        <hr>
        @if (count($extraItems) != 0)
            <div class="container-additionally">
                <h1>Дополнительные услуги</h1>
                <div class="row">	
                    <div class="list-additionally">
                            <?php $i = 0; ?>
                            @foreach ($extraItems as $extra)
                                <span class="square-more-setting">&#9632;</span>
                                <div>   
                                    <p>{{$extra->extra_name}}</p> 
                                    <p>Цена: {{$extra->extra_cost}}</p> 
                                    <p>Длительность: {{$extra->extra_time}}</p> 
                                </div>
                                <?php 
                                    if ($i % 5 == 0) 
                                    {
                                    echo `</div>
                                    </div>
                                    <div class="row">
                                        <div class="list-more-settings">`;
                                    } 
                                    $i++; 
                                ?>
                            @endforeach
                    </div>
                </div>
            </div>
        @endif
    </div>
</section>



<script src="https://cdnjs.cloudflare.com/ajax/libs/air-datepicker/2.2.3/js/datepicker.min.js"></script>



<script type="text/javascript">    
    
    let mileage_day = <?php if(isset($mileage->day)) { 
        echo $mileage->day; 
    } else { 
        echo "''"; 
    } ?>;
    let mileage_week = <?php if(isset($mileage->week)) { 
        echo $mileage->week; 
    } else { 
        echo "''"; 
    }  ?>;
    let mileage_month = <?php if(isset($mileage->month)) { 
        echo $mileage->month; 
    } else {
         echo "''"; 
    }  ?>;

    let booking_dates = <?php if(isset($bookingDatesCalcul)) { echo $bookingDatesCalcul; } else { echo "''"; } ?>;

    /* Проверем выбраны ли даты которые уже заняты */
    function isBookingDate () {
        var from = $('#minMaxFrom').val();
        var to = $('#minMaxTo').val();

        $('#booking-submit').attr("disabled", false);
        $('#error_booking').css({'display' : 'none'})   

        if (from && to) {
            from = new Date(from)
            to = new Date(to)

            if (booking_dates !== undefined && booking_dates !== '') {
                booking_dates.forEach(function(booking_date) {
                    var booking_to = new Date(booking_date.to)
                    var booking_from = new Date(booking_date.from)

                    if ( (from >= booking_from  && from <= booking_to.setDate(booking_to.getDate() + 1)) ||
                        (to >= booking_from  && to <= booking_to.setDate(booking_to.getDate() + 1)) ||
                        (from < booking_from && to >= booking_to.setDate(booking_to.getDate() + 1)) )  {

                        $('#booking-submit').attr("disabled", true);
                        $('#error_booking').css({'display' : 'inline'})
                    }
                });
            }

        }
    }
    
    /* Обсчитываем цену за аренду и подсчёт включённого км */
    function mileageCalculation () {
        var from = $('#minMaxFrom').val();
        var to = $('#minMaxTo').val();
    
        
        if (from && to) {
            var from_obj = new Date(from);
            var to_obj = new Date(to);
            
            var result =  Math.floor(( Date.parse(to_obj) - Date.parse(from_obj) ) / 86400000) + 1; 

            // Сверяем дни, и в зависимоти от результата отдаем результат
            if (result > 0) {
                if (result < 7) {

                    var html = result * mileage_day
                } else if (result == 7) {

                    var html = mileage_week
                } else if (result > 7 && result <= 14) {

                    var html = mileage_week
                    var result = result - 7

                    var html_day = result * mileage_day

                    var html = html + html_day
                } else if (result > 14 && result <= 30) {

                    var html = mileage_month
                } else if (result > 30 && result <= 36) {

                    var html = mileage_month
                    var result = result - 30


                    var html_day = result * mileage_day
                    var html = html + html_day
                } else if (result == 37) {

                    var html = mileage_month
                    var result = result - 30

                    var html_week = mileage_week
                    var html = html + html_week
                } else if (result > 37 && result < 44) {

                    var html = mileage_month
                    var result = result - 30

                    var html_week = mileage_week
                    var result = result - 7

                    var html_day = result * mileage_day

                    var html = html + html_week +  html_day
                } else if (result >= 44 && result < 61) {

                    var html = 2 * mileage_month
                } else if (result >= 61 && result < 67) {

                    var html = 2 * mileage_month
                    var result = result - 60

                    var html_day = result * mileage_day

                    var html = html +  html_day
                } else if (result >= 67 && result < 74) {

                    var html = 2 * mileage_month
                    var result = result - 60

                    var html_week = mileage_week
                    var result = result - 7

                    var html_day = result * mileage_day

                    var html = html + html_week +  html_day
                } else if (result >= 74 && result <= 90) {

                    var html = 3 * mileage_month
                }
            } else {
                var html = 'Дата отрицательна'
            }

            $('#number_days').val(result)
            $('#included_distance').val(html)
            $('#rusult-calculation-mileage').text(html + ' км.')
        }
    }

    let rentPlace = <?php echo $rentPlace; ?>

    function printSumDelivery (location_delivery) {
        location_delivery = $('#location_delivery').val();

        $('#sum-delivery').text('0')
        $('#container-sum-delivery').css({'display': 'none'});
        rentPlace.forEach(place => {
            if (place.id == location_delivery) {
                $('#sum-delivery').text(place.cost)

                $('#container-sum-delivery').css({'display': 'flex'});
            }
        });
    }
    

/* Нужно сверить минимальное значение арненды с тем что выбрал юзер */
let min_rent_time = '<?php echo $selectionCars[0]->min_rent_time;?>'
let max_rent_time = '<?php echo $selectionCars[0]->max_rent_time;?>'

let date_from = '<?php echo $selectionCars[0]->from;?>'
let date_to = '<?php echo $selectionCars[0]->to;?>'

// Передаем дату 'от' и 'до' в объект new Date
var date_from_obj = new Date(date_from);
var date_to_obj = new Date(date_to);

var date_from_ninety_obj = new Date(date_from);
var date_from_ninety = new Date(date_from_ninety_obj.setSeconds(date_from_ninety_obj.getSeconds() + 7862400)) // 7776000 это 91 днь в секундах

// Формируем минимальное и максимальное значение арненды
var min_rent_necessary = new Date(date_from_obj.setSeconds(date_from_obj.getSeconds() + min_rent_time))
var max_rent_necessary = new Date(date_from_obj.setSeconds(date_from_obj.getSeconds() + max_rent_time))

/* 
*  Проверяем если максимально разрешенное время аренды 
*  больше date_rent_to, тогда присваиваем date_rent в max_rent_necessary 
*/
if (date_to_obj.getTime() < max_rent_necessary.getTime()) {
    var max_rent_necessary = date_to_obj
}

/* 
*  Проверяем, если нужное значение больше 90 дней, присваиваем ему ограничение.
*/
if (date_from_ninety.getTime() < max_rent_necessary.getTime()) {
    var max_rent_necessary = date_from_ninety
}

/* 
*  Проверяем, если минимальная дата в прошлом, присваиваем ей сегодняшнее
*/
if (min_rent_necessary < new Date()) {
    var min_rent_necessary = new Date()
}

function checkDate () {
  var from = $("#minMaxFrom").val();
  var to = $("#minMaxTo").val();

  if (from && to && to < from) {
    $("#minMaxTo").val('')
  }
}

    Date.prototype.addDays = function (days) {
        var date = new Date(this.valueOf());
        date.setDate(date.getDate() + days);
        return date;
    };
    let dateFrom = $('#minMaxFrom').datepicker({
        onSelect: function (formattedDate, date, inst) {
            console.log('otkelbay');
        isBookingDate();
        mileageCalculation();
        checkDate ();
            dateTo.update('minDate', date.addDays(1))
    },
    autoClose: true,
    minDate: min_rent_necessary,
    maxDate: max_rent_necessary
    }).data('datepicker');

    let dateTo = $('#minMaxTo').datepicker({
        onSelect: function (formattedDate, date, inst) {
        isBookingDate();
        mileageCalculation();
        checkDate ();
    },
    autoClose: true,
    minDate: min_rent_necessary,
    maxDate: max_rent_necessary
    }).data('datepicker');
</script>
@endsection
