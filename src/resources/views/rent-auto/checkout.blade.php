@extends('layouts.app')
@section('content')
<div class="container mb-5 checkout" id="checkout">
	<div class="row">
		<div class="col-12 col-lg-7 order-2 order-lg-1" id="checkout-lists">
		@if (Auth::check())
			@if (
			($car['assessed_value'] < 1000000 && $user->getAgeAttribute() >= 21 && $user['driving_experience'] >= 2)
				||
				($car['assessed_value'] >= 1000000 && $user->getAgeAttribute() >= 25 && $user['driving_experience'] >=
				5) || $user['driving_experience'] == ''
				)
				@if (!$user->confirmed)

				@endif
				@if ($user->confirmed)
				<h2 class="about-car__title mt-3">
					{{trans('checkout.1')}}
				</h2>
				<ul class="checkout-lists">
					<li class="checkout-list">
						<div class="alert-primary" style="padding: 20px; ">
						{{trans('checkout.2')}}</div>

						<div class="title-list" id="сhoose-extra-options">
							{{-- <span class="marker">&#8226;</span> --}}

						</div>
						<div class="container-сhoose-extra-options checkout__block d-block">

<!-- EXTRA SERVICES -->
							<div class="container-form flex-column">
								@if (count($extras) > 0)
								<p style="font-family: 'Circe 300';">{{trans('checkout.3')}}</p>
								<p style="font-family: 'Circe 300';">{{trans('checkout.4')}}</p>
								<br>
								<br>
								<br>

								@foreach ($extras as $item)
										<div class="row">
												<div class="col-lg-12 col-12 mb-2"  style="font-family: 'Druk Wide Medium Cy 500'; font-size: 25px">
													{{$item->extra->name}}
													<p class="extra-desc">{{$item->extra->desc}}</p>
												</div>
											</div>
									<div class="extra_id d-none">{{$item->id}}</div>
									<br>
									<div class="row">
										<div class="col-sm-6" data-cost="{{$item->extra_cost}}">
												<span class="conditions"><p style="font-family: 'Circe 300';font-size: 30px">{{$item->extra_cost}} trans('checkout.5') @if ($item->time->name == trans('checkout.6'))
											/сутки</p></span>
											<p id="time{{$item->id}}" style="display: none">1</p>
											@endif
											
										</div>
										<div class="col-sm-6" style="margin-bottom: 20px">
										<button type="submit" data-cost="{{$item->extra_cost}}" data-id="{{$item->id}}" class="checkout-btn extra-btn btn">{{ trans('checkout.7') }}</button>
										</div>
									</div>
									<br>

								@endforeach
								@else
								<p style="font-family: 'Circe 300';">{{ trans('checkout.8') }}</p>
								<br>

								@endisset
							</div>

<!-- COURSES -->

				{{-- 			<div class="container-form flex-column">
								@if (count($courses) > 0)
								<p style="font-family: 'Circe 300';">{{ trans('checkout.9') }}</p>
								<p style="font-family: 'Circe 300';">{{ trans('checkout.10') }}</p>
								<br>
								<br>
								<br>

								@foreach ($courses as $item)
										<div class="row">
												<div class="col-lg-12 col-12 mb-2"  style="font-family: 'Druk Wide Medium Cy 500'; font-size: 25px">
													{{$item->course->name}}
													<p class="course-desc">{{$item->course->desc}}</p>
												</div>
											</div>
									<div class="course_id d-none">{{$item->id}}</div>
									<br>
									<div class="row">
										<div class="col-sm-6" data-cost="{{$item->course_cost}}">
												<span class="conditions"><p style="font-family: 'Circe 300';font-size: 30px">
												{{$item->course_cost}} trans('checkout.11') @if ($item->time->name == trans('checkout.12'))
											/сутки</p></span>
											<p id="time{{$item->id}}" style="display: none">1</p>
											@endif
											
										</div>
										<div class="col-sm-6" style="margin-bottom: 20px">
										<button type="submit" data-cost="{{$item->course_cost}}" data-id="{{$item->id}}" class="checkout-btn course-btn btn">{{ trans('checkout.13') }}</button>
										</div>
									</div>
									<br>

								@endforeach
								@else
								<p style="font-family: 'Circe 300';">{{ trans('checkout.14') }}</p>
								<br>

								@endisset
							</div> --}}


							<div class="container-form flex-column">
								<h5 style="font-family: 'Circe 300';">{{ trans('checkout.15') }}</h5>
								<h5 style="font-family: 'Circe 300';">{{ trans('checkout.16') }}</h5>
								<br>
								<h5 style="font-family: 'Circe 300';">{{ trans('checkout.17') }}</h5>
								<h5 style="font-family: 'Circe 300';">- {{ trans('checkout.18') }}</h5>
								<h5 style="font-family: 'Circe 300';">- {{ trans('checkout.19') }}</h5>
							</div>






						</div>
					</li>
				</ul>
				@endif
				@else
				<h1 class="about-car__title">{{ trans('checkout.20') }}</h1>
				@endif
				@else
				<h2 class="about-car__title mt-3">
					{{ trans('checkout.21') }}
				</h2>
				<ul class="checkout-lists first-step">
					<li class="checkout-list">
						{{-- <div class="alert-primary" style="padding: 20px; ">{{ trans('checkout.22') }}</div> --}}



						<div class="title-list" id="confirm-phone">



							<h5 style="font-family: 'Circe 300';">{{ trans('checkout.23') }}</h5>
						</div>

						<div class="container-confirm-phone checkout__block d-block">
							<div class="container-form">
								<form method="GET" action="{{route('sendValidateSms')}}" id="sendValidateSms"
									class="validateForm">
									@csrf
									<div class="login-form__pass">
										<input type="text" name="phone" class="login-form__input phone" id="phone"
											placeholder="{{ trans('checkout.54') }}" required>
									</div>
									<p id="numberExist" style="display:none">{{ trans('checkout.24') }}</p>
									<button type="submit" class="checkout-btn btn">{{ trans('checkout.25') }}</button>
								</form>
								<form method="POST" id="validatePhone" action="{{route('validatePhone')}}"
									class="validateForm disabled">
									@csrf
									<div class="login-form__pass">
										<input type="number" name="code" class="login-form__input" id="phone_code"
											placeholder="{{ trans('checkout.55') }}" required>
									</div>
									<p></p>
									<button type="submit" class="checkout-btn btn">{{ trans('checkout.26') }}</button>
								</form>
								<p class="confirm-text d-none">{{ trans('checkout.27') }}</p>
							</div>
						</div>

						<p style="font-family: 'Circe 300';">{{ trans('checkout.28') }}</p>
						<p style="font-family: 'Circe 300';">{{ trans('checkout.29') }}</p>
						<p style="font-family: 'Circe 300';">{{ trans('checkout.30') }}</p>
						<p style="font-family: 'Circe 300';">{{ trans('checkout.31') }}</p>
						<p style="font-family: 'Circe 300';">{{ trans('checkout.32') }}</p>
						<p style="font-family: 'Circe 300';">{{ trans('checkout.33') }}</p>
						<p style="font-family: 'Circe 300';">{{ trans('checkout.34') }}</p>

						<div class="row" style="margin-bottom: 10px">
							<div class="col-md-6">
								<p class="mt-5" style="margin-bottom:70px">{{ trans('checkout.35') }}</p>
								<div class="checkout-lists d-flex justify-content-between">
									<button type="submit" class="btn login-button ">{{ trans('checkout.36') }}</button>
									{{-- <a href="{{route('checkout.goToRegister')}}" onclick="ym(53583991, 'reachGoal',
									'register');"
									class="btn login-form__button">{{ trans('checkout.37') }}</a>--}}
								</div>
							</div>

						</div>



						{{-- @else
						<p class="mb-4">{{ trans('checkout.38') }}</p>
						@endif --}}
					</li>
				</ul>
				{{-- <h1 class="about-car__title mt-3">{{ trans('checkout.39') }}</h1> --}}

				@endif
		</div>
		<div class="col-12">
			<h1 style="	font-family: 'Druk Wide Medium Cy 500';letter-spacing: 0.03em;margin-bottom:30px"
				class="mt-3">{{ trans('checkout.40') }}</h1>
		</div>
	</div>
</div>



<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="loading"><img src="/image/loading.gif" alt=""></div>
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">{{ trans('checkout.41') }}</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<form method="POST" id="checkout-login" action="{{ route('login') }}" aria-label="{{ __('Login') }}"
					class="login-form">
					@csrf
					<div class="login-form__adress">
						<p class="login-form__text">{{ trans('checkout.42')}}</p>
						<input id="#" type="email"
							class="login-form__input{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
							placeholder="{{ trans('checkout.56')}}" required value="{{ old('email') }}">

						@if ($errors->has('email'))
						<span class="invalid-feedback" role="alert">
							<strong>{{ $errors->first('email') }}</strong>
						</span>
						@endif

					</div>
					<div class="login-form__pass">
						<p class="login-form__text">{{ trans('checkout.43')}}</p>
						<input id="password" type="password"
							class="login-form__input{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password"
							minlength="6" placeholder="{{ trans('checkout.57')}}" required>
						<div id="show-pass" onclick="showPass('password')"></div>
						@if ($errors->has('password'))
						<span class="invalid-feedback" role="alert">
							<strong>{{ $errors->first('password') }}</strong>
						</span>
						@endif

						<div class="checkbox-login">
							<input id="check" type="checkbox" name="remember" value="check"
								{{ old('remember') ? 'checked' : '' }} class="check">
							<label for="check">{{ trans('checkout.44')}}</label>

							@if ($errors->has('password'))
							<span class="invalid-feedback" role="alert">
								<strong>{{ $errors->first('password') }}</strong>
							</span>
							@endif

						</div>
					</div>
					<button type="submit" class="btn login-form__button">{{ trans('checkout.45')}}</button>
					<a href="{{ route('password.request') }}" class="login-form__link">
					{{ trans('checkout.46')}}</a>
				</form>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="policy" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="loading"><img src="/image/loading.gif" alt=""></div>
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">{{ trans('checkout.47')}}</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<p>{{ trans('checkout.48')}}</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-close creating-cars-btn mt-0 yes"
					data-dismiss="modal">{{ trans('checkout.49')}}</button>
				<button type="button" class="btn btn-close creating-cars-btn mt-0 no" data-dismiss="modal">{{ trans('checkout.50')}}</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="not" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="loading"><img src="/image/loading.gif" alt=""></div>
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">{{ trans('checkout.51')}}</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<p>{{ trans('checkout.52')}}</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-close creating-cars-btn mt-0"
					data-dismiss="modal">{{ trans('checkout.53')}}</button>
			</div>
		</div>
	</div>
</div>
<script>
	function inputFileUpload(nameInput) {

			var fileInput  = document.querySelector(".input-file-" + nameInput),  
					button     = document.querySelector(".input-file-trigger-" + nameInput),
					the_return = document.querySelector(".file-return-" + nameInput);
					
			the_return.innerHTML = fileInput.value.split('fakepath\\')[1]; // Разделить по 'fakepath\', взять вторую часть

	}

$(document).ready(function() {

	document.querySelector("html").classList.add('js');

	var car_id = '<?php echo $car->id; ?>';
	let isCode = false;

	$('body').on('click', '.checkout-title', function(){
			$(this).closest('.checkout-list').find('.checkout__block').toggleClass('d-block');
	});
});
</script>
@endsection