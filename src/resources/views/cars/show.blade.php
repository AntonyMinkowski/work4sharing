@extends('layouts.app')

@section('content')

<style>
.yellowclass {
	color:#f6d200;
}
.circle {
	width: 48px;
    height: 48px;
    border-radius: 48px;
    background-color: #000;
    color:white;
    padding: 5px;
}
</style>

<section class="changing-card">
	<div class="container">
		@if ($register_step !== 2)
		<h1 class="changing-card-title">
			{{trans('cars.show')}}
		</h1>
		<div class="row">
			<div class="col">
				<ul class="card-list">

<!-- just previous info  -->

					{{-- @if (!$step_0)
						<li>
							<h1 class="changing-card-title {{ $step_0 ? 'yellowclass' : '' }}">
								STEP <span class="circle">1</span>
							</h1>
						</li>
						<li class="card-list__item card-list__info d-flex">
							<div class="card-list__wrap ">
								<h2 class="card-list__title">
									{{trans('cars.show_brand')}}
								</h2>
								<p class="card-list__text">
									{{ $data['brand']['name'] }}
								</p>
							</div>
							<div class="card-list__wrap">
								<h2 class="card-list__title">
									{{trans('cars.show_model')}}
								</h2>
								<p class="card-list__text">
										{{ $data['model']['name'] }}
								</p>
							</div>
							<div class="card-list__wrap">
								<h2 class="card-list__title">
									{{trans('cars.show_type')}}
								</h2>
								<p class="card-list__text">
									{{ $data['type']['name'] }}
								</p>
							</div>
						</li>
						<li class="card-list__item"></li>
					@endif --}}

<!-- STEP 1 (age/experience/education/salary/amount of free days???) -->

					@if (!$step_1 && $step_0)
						<li>
							<h1 class="changing-card-title">
								STEP <span class="circle">2</span>
							</h1>
						</li>
						<li class="card-list__item d-flex justify-content-between">
							<form id="saveYearSeatsDoors" method="POST" action="/car/{{$data['id']}}/update" class="d-flex justify-content-between saveYearSeatsDoors flex-wrap" style="width: 100%;">
								@csrf
								<div class="form-production-date">
									<h2 class="form-production-date__title">
										{{trans('cars.show_birth')}}
									</h2>
									<input type="number" min="1930" max="2019" class="login-form__input" name="year_of_issue" placeholder="{{trans('cars.show_birth')}}" required>
								</div>
								<div class="form-number-persons">
									<h2 class="form-number-persons__title">
										{{trans('cars.show_children')}}
									</h2>
									<select name="seat" class="form-number-persons__select" required>
										<option class="form-number-persons__option" value="">
											{{trans('cars.nothing_selected')}}
										</option>
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
										<option value="6">6</option>
										<option value="7">7</option>
										<option value="8">8</option>
									</select>
								</div>
								<div class="form-production-date">
									<h2 class="form-production-date__title">
										{{trans('cars.show_experience')}}
									</h2>
									<input type="number" class="login-form__input" name="mileage" placeholder="{{trans('cars.show_experience')}}" required>
								</div>
								<div class="form-production-date">
									<h2 class="form-production-date__title">
										{{trans('cars.show_salary')}}
									</h2>
									<input type="number" class="login-form__input" name="assessed_value" placeholder="{{trans('cars.show_salary')}}" required>
								</div>
								<div class="form-production-date">
										<h2 class="form-production-date__title">
											{{trans('cars.show_color')}}
										</h2>
										<select id="color_id" name="color_id" class="form-production-date__select" required>
												<option class="form-production-date__option" value="">
													{{trans('cars.nothing_selected')}}
												</option>


												@foreach ($colors as $color)
												<option class="form-production-date__option" value="{{$color->id}}">
														{{$color->name}}
													</option>
												@endforeach
										</select>
									</div>
								<button type="submit" class="btn card-list__btn mt-4">
									{{trans('cars.show_save')}}
								</button>
							</form>
						</li>
					@endif

<!-- STEP 2 (salary/connection) -->

					@if (!$step_2 && $step_1)
						<li>
							<h1 class="changing-card-title {{ $step_2 ?? 'yellowclass' }}">
								STEP <span class="circle">3</span>
							</h1>
						</li>
						<li class="card-list__item card__cost card-list__item__cost d-flex justify-content-between">
							<form id="saveTypeCost" method="POST" action="/car/{{$data['id']}}/unlock_type_cost" class="saveTypeCost d-flex justify-content-between" style="width: 100%;">
								@csrf
								<div class="form-production-date">
									<h2 class="form-production-date__title">
										{{trans('cars.show_salary_per_day')}}
									</h2>
									<input type="number" class="login-form__input" name="cost_day" placeholder="{{trans('cars.show_input_salary_per_day')}}" required>
								</div>
								<div class="number-dor">
									<h2 class="number-dor__title">
										{{trans('cars.show_booking_type')}}
									</h2>
									<div class="group-ridio" id="connection_type">
											@foreach ($unlock_type as $type)
												<input name="type" class="form-check-label" type="radio" value="{{ $type->id }}"
												@if($type->check) checked @endif
												id="{{ $type->id }}">
												<label for="{{ $type->id }}">{{ $type->name }}</label>
											@endforeach
									</div>
								</div>
								<div class="number-dor d-none">
									<h2 class="form-production-dor__title">
										{{trans('cars.show_time_before')}}
									</h2>
									<select name="time_alert" class="form-number-persons__select">
										<option class="form-number-persons__option" value="">
											{{trans('cars.nothing_selected')}}
										</option>
										{{-- @foreach($timeAlertList as $id => $name)
											<option value="{{$id}}" {{ ($data['time_alert'] ?? null) == $id ? "selected" : "" }}> {{$name}}</option>
										@endforeach --}}
									</select>
								</div>
								<button type="submit" class="btn card-list__btn">
									{{trans('cars.show_save')}}
								</button>
							</form>
						</li>
					@endif

<!-- STEP 3 (SKILLS) -->

					{{-- @if (!$step_3 && $step_2)
						<li>
							<h1 class="changing-card-title">
								STEP <span class="circle">3</span>
							</h1>
						</li>
						<li class="card__featured card-list__item d-flex">
							<form id="saveFeatured" class="w-100" method="POST" action="{{$data['id']}}/feature">
								@csrf
								<input type="hidden" name="redirect" value="1">
								<div class="row">
									<div class="col-12">
										<ul class="extra-wrap">
											@foreach ($data['feature'] as $title => $check)
												@isset($dataFeature[$title])
												<li class="extra-wrap__item">
													<div class="checkbox">
														<input id="{{$title}}" type="checkbox" name="{{$title}}" value="1" class="check"
														@if ($check)
															checked
														@endif
														>
														<label for="{{$title}}">
															{{ $dataFeature[$title] }}
														</label>
													</div>
												</li>
												@endisset
											@endforeach
										</ul>
									</div>
								</div>
								<div class="row">
									<div class="col d-flex justify-content-end">
										<button type="submit" class="btn card-list__btn d-flex justify-content-center">
											{{trans('cars.show_confirmation_send')}}
										</button>
									</div>
								</div>
							</form>
						</li>
					@endif --}}

<!-- STEP 4 (free time) -->

					@if ($step_4 === null && $step_2)
						<li>
							<h1 class="changing-card-title">
								STEP <span class="circle">4</span>
							</h1>
						</li>
						<li class="card-list__item card__mileage card-list__item__cost d-none justify-content-between">
							<form id="mileage_inclusive" method="POST" action="/car/{{$data['id']}}/mileage_inclusive" class="saveTypeCost d-flex justify-content-between" style="width: 100%;">
								@csrf
								<input type="hidden" name="car_id" value="{{$data['id']}}">
								<div class="number-dor">
									<h2 class="form-production-date__title">
										{{trans('cars.show_daily_load')}}
									</h2>
									<input type="number" class="login-form__input" name="day" placeholder="{{trans('cars.show_load_unit')}}" required>
								</div>
								<div class="number-dor">
									<h2 class="form-production-date__title">
										{{trans('cars.show_weekly_load')}}
									</h2>
									<input type="number" class="login-form__input" name="week" placeholder="{{trans('cars.show_load_unit')}}" required>
								</div>
								<div class="number-dor">
									<h2 class="form-production-date__title">
										{{trans('cars.show_monthly_load')}}
									</h2>
									<input type="number" class="login-form__input" name="month" placeholder="{{trans('cars.show_load_unit')}}" required>
								</div>
								<button type="submit" class="btn card-list__btn">
									{{trans('cars.show_save')}}
								</button>
							</form>
						</li> 

						<li class="card-list__item card__rent card-list__item__cost d-flex 
							justify-content-between flex-wrap">
								<!-- <div class="w-100">
									<h2 class="card-list__title card__photo__title mb-5">{{trans('cars.show_duration')}}</h2>
								</div> -->
							<form id="rent_time" method="POST" action="/car/{{$data['id']}}/rent_time" class="saveTypeCost d-flex justify-content-between" style="width: 100%;">
								@csrf
								<input type="hidden" name="car_id" value="{{$data['id']}}">
								<div class="number-dor">
									<h2 class="form-production-date__title">
										{{trans('cars.show_duration_min')}}
									</h2>
									<input type="number" class="login-form__input" name="min_rent_time" placeholder="{{trans('cars.show_duration_hint')}}" required>
								</div>
								<div class="number-dor">
									<h2 class="form-production-date__title">
										{{trans('cars.show_duration_max')}}
									</h2>
									<input type="number" class="login-form__input" name="max_rent_time" placeholder="{{trans('cars.show_duration_hint')}}" required>
								</div>
								<button type="submit" class="btn card-list__btn">
									{{trans('cars.show_save')}}
								</button>
							</form>
						</li>
					@endif 


<!-- FINAL (submit) -->


					@if (!$step_5 && $step_4 !== null && $step_2 && $step_1 && $step_0)
						<li>
							<h1 class="changing-card-title">
								STEP <span class="circle">5</span>
							</h1>
						</li>
						<li class="card__featured card-list__item d-flex">
							<div class="row next w-100 mt-4 d-flex" style="margin-top: 1.5rem !important;">
								<form id="redirect" method="POST" class="col d-flex justify-content-center" action="{{route('car.redirect', $data['id'])}}">
									@csrf
									<div class="checkbox w-50">
										<input id="policy" type="checkbox" name="policy" value="1" class="check"
										>
										<label for="policy">
											{{trans('cars.show_agreement')}}
											<a href="/agentskiy-dogovor-soauto.pdf">{{trans('cars.show_agreement_link')}}</a>

										</label>
										<div class="error d-none">
											<p>{{trans('cars.show_agreement_request')}}</p>
										</div>
									</div>
									<button class="btn" href="">{{trans('cars.show_agreement_finish')}}</button>
								</form>
							</div>
						</li>
					@endif
				</ul>
			</div>
		</div>
		@else
			<!-- YOU HAVE TO WAIT APPROVEMENT !!! -->

			<h1>{{trans('cars.show_error_double')}}</h1>


			<a href="/profile/mycars/{{$data['id']}}">{{trans('cars.show_error_finish')}}</a>
		@endif
	</div>
</section>



<!-- MODAL -->


{{--<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
		<div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel">{{trans('cars.show_error')}}</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			</button>
		</div>
		<div class="modal-body">
			<p>{{trans('cars.show_body_title')}}</p>
			<ul>
				<li>{{trans('cars.show_body_document1')}}</li>
				<li>{{trans('cars.show_body_document2')}}</li>
				<li>{{trans('cars.show_body_document3')}}</li>
				<li>{{trans('cars.show_body_document4')}}</li>
			</ul>
			<p>{{trans('cars.show_body_text1')}}</p>
			<p>{{trans('cars.show_body_text2')}}</p>
			<p>{{trans('cars.show_body_text3')}}</p>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-close creating-cars-btn mt-0" data-dismiss="modal">{{trans('cars.show_body_good')}}</button>
		</div>
		</div>
	</div>
</div>--}}

<!-- MODAL 2 -->

<div class="modal fade" id="photos-modal" tabindex="-1" role="dialog" aria-labelledby="photoModal" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="photoModal">{{trans('cars.show_modal')}}</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<p>{{trans('cars.show_photo')}}</p>
				<ul>
					<li>{{trans('cars.show_photo1')}}</li>
					<li>{{trans('cars.show_photo2')}}</li>
					<li>{{trans('cars.show_photo3')}}</li>
				</ul>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-close creating-cars-btn mt-0" data-dismiss="modal">{{trans('cars.show_photo_good')}}</button>
			</div>
		</div>
	</div>
</div>


<script>

	var car_id = <?= $data['id'] ?>;

</script>
@endsection
{{-- @section('bottom_scripts')
	<script>
		@if ($step_5)
		// $step_7 == null && 
		$(document).ready(function () {
			$('#photos-modal').modal('show');
		});
		@endif
	</script>
@endsection --}}













