@extends('layouts.app')

@push('scripts')
    <script src="{{ asset('js/search_auto.js') }}"></script>
    <script src="{{ asset('js/get_models.js') }}"></script>
@endpush

@section('content')

<section class="filter">
    <div class="container">
      <h1 class="filter-title">
        {{trans('cars.list')}}
      </h1>
      <section class="filter">
          <form class="form-cars" id="search_auto" method="GET" role="form" enctype=multipart/form-data>
            <div class="row">
              <div class="col-3">
                @component('components.select', [
                    'name' => 'brand',
                    'items' => $brandsCars
                ])
                    @slot('title')
                        {{trans('cars.list_brand')}}
                    @endslot
                @endcomponent
              </div>
              <div class="col-3">
                @component('components.select', [
                    'name' => 'model',
                    'items' => $models ?: [],
                ])
                    @slot('title')
                        {{trans('cars.list_model')}}
                    @endslot
                @endcomponent
              </div>
              <div class="col-3">
                <div class="form-cars__wrap">
                  <h2 class="form-cars__title">
                    {{trans('cars.list_engine')}}
                  </h2>
                  <select class="form-cars__select">
                    <option class="form-cars__option">
                      {{trans('cars.nothing_selected')}}
                    </option>
                  </select>
                </div>
              </div>
              <div class="col-3">
                <div class="form-cars__wrap">
                  <h2 class="form-cars__title">
                    {{trans('cars.list_type')}}
                  </h2>
                  <select class="form-cars__select">
                    <option class="form-cars__option">
                      {{trans('cars.nothing_selected')}}
                    </option>
                  </select>
                </div>
              </div>
            </div>
            <div class="show-filter" style="display:flex;justify-content:space-between">
              <a href="javascript:look('filter');" id="a-filter" class="show-filter__link">
                {{trans('cars.list_all_filters')}}
              </a>
              <button type="submit" id="filter-btn" class="btn filter-btn">
                {{trans('cars.list_search')}}
              </button>
            </div>
              <div id="filter" style="display:none;">
                <div class="row">
                  <div class="col-12">
                    <h2 class="extra-title">
                      {{trans('cars.list_additionally')}}
                    </h2>
                  </div>
                </div>
                <div class="row">
                  @foreach ($features->chunk(1) as $chunk)
                    <div class="col-2">
                      @foreach ($chunk as $key => $check)
                        <div class="checkbox">
                          <input id="{{ $key }}" type="checkbox" name="{{ $key }}" value="1" class="check"
                            @if ($check)
                              checked
                            @endif>
                            <label for="{{ $key }}">
                              {{ __('form.checkbox.'.$key) }}
                            </label>
                        </div>
                      @endforeach
                    </div>
                  @endforeach
                  </div>
                  <div class="row flex-center" style="margin-top: 40px;">
                    <div>
                      <div class="center-block btn-group btn-group-toggle col-12 col-sm-6 col-md-4 offset-sm-2">
                        <label class="btn btn-secondary @if($check) active @endif" aria-pressed="false">
                        <input
                            type="date"
                            @if(true)
                            checked
                            @endif
                            id="{{ "rent_from" }}"
                            name="{{ "from" }}"
                            autocomplete="off"
                            value="{{ request()->get('from') }}">
                          {{ __('form.rent_from') }}
                        </label>
                        <label class="btn btn-secondary @if($check) active @endif" aria-pressed="false">
                          <input
                            type="date"
                            @if(false)
                            checked
                            @endif
                            id="{{ "rent_to" }}"
                            name="{{ "to" }}"
                            autocomplete="off"
                            value="{{ request()->get('to') }}">
                          {{ __('form.rent_to') }}
                        </label>
                      </div>
                    </div>
                  </div>
                  <div class="row flex-center" style="margin-top: 40px;">
                    <div>
                      <div class="center-block btn-group btn-group-toggle col-12 col-sm-6 col-md-4 offset-sm-2">
                        <label class="btn btn-secondary @if($check) active @endif" aria-pressed="false">
                          <input
                              type="number"
                              min="0"
                              id="{{ "rent_from" }}"
                              name="{{ "from_cost_day" }}"
                              value="{{ request()->get('from_cost_day') }}"
                              placeholder="{{trans('cars.from')}}">
                        </label>
                        <label class="btn btn-secondary @if($check) active @endif" aria-pressed="false">
                          <input
                              type="number"
                              min="0"
                              id="{{ "rent_to" }}"
                              name="{{ "to_cost_day" }}"
                              value="{{ request()->get('to_cost_day') }}"
                              placeholder="{{trans('cars.to')}}">
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col d-flex justify-content-end">
                    <button type="submit" id="filter-btn2" class="btn filter-btn2 d-none">
                      {{trans('cars.list_search')}}
                    </button>
                  </div>
                </div>
              </div>
            </form>
          </section>
          <section class="car">
            <div class="container">
              <div class="row">
                @foreach ($selectionCars as $item)
                <div class="col-4">
                  <div class="car-wrapper" style="
                  /*background: url(https://hb.bizmrg.com/soautomedia/{{$item->get('image')}}) */
                  background: url(images/user.png) 
                  no-repeat center center;">
                    <a href="/carcard/{{$item->get('id')}}" class="car-wrapper__link d-flex flex-column justify-content-center align-items-center">
                    </a>
                    <div class="car-wrapper__subtitle text-center">
                      {{trans('cars.booking_available')}}
                    </div>
                    <h1 class="car-wrapper__title">
                      {{$item->get('brand')}} {{$item->get('model')}}
                    </h1>
                    <div class="car-wrapper__item d-flex justify-content-between">
                      <div class="car-wrapper__wrap-left">
                        <div class="car-wrapper__inner d-flex align-items-center">
                          <div class="car-wrapper__icon car-wrapper__icon_ticket"></div>
                          <p class="car-wrapper__text">
                            <span class="car-wrapper__span">
                              {{$item->get('cost_day')}}
                            </span>
                              {{trans('cars.booking_unit')}}
                          </p>
                        </div>
                        <div class="car-wrapper__inner d-flex align-items-center">
                          <div class="car-wrapper__icon car-wrapper__icon_transmission"></div>
                          <p class="car-wrapper__text">
                              {{$item->get('gear')}}
                          </p>
                        </div>
                      </div>
                      <div class="car-wrapper__wrap-right">
                        <div class="car-wrapper__inner d-flex align-items-center">
                          <div class="car-wrapper__icon car-wrapper__icon_event"></div>
                          <p class="car-wrapper__text">
                            {{$item->get('year_of_issue')}}
                          </p>
                        </div>
                        <div class="car-wrapper__inner d-flex align-items-center">
                          <div class="car-wrapper__icon car-wrapper__icon_car"></div>
                          <p class="car-wrapper__text">
                            {{$item->get('car_type')}}
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                {{-- @if (Route::has('login')) @auth
                <a href="#" class="btn btn-primary">{{trans('cars.list_booking')}}</a> @else
                <a href="/login" onclick="alert('{{trans('cars.list_not_auth')}}')" class="btn btn-primary">{{trans('cars.list_booking')}}</a> @endauth @endif --}}
            </div>
      @endforeach
    </div>
  </div>


@if ($paginator->lastPage() > 1)
<nav aria-label="pagination">
    <ul class="pagination pagination-lg">
        @for($page = max($paginator->currentPage() - 7, 1); $page <= min($paginator->currentPage() + 7, $paginator->lastPage()); $page++)
            @if($paginator->currentPage() == $page)
                <li class="page-item disabled">
                    <a class="page-link" href="{{ $paginator->url($page) }}" tabindex="-1">{{ $page }}</a>
                </li>
            @else
                <li class="page-item">
                    <a class="page-link" href="{{ $paginator->url($page) }}">{{ $page }}</a>
                </li>
            @endif
        @endfor

    </ul>
</nav>
@endif

</div>
@endsection
