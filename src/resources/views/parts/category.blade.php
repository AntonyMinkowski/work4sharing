<section class="category">
    <div class="container">
        <div class="row">
            <div class="col d-flex flex-wrap">
                <a href="{{route('car.catalog.index')}}" class="category__block black">
                    <h3>{{trans('category.1')}}</h3>
                    <p>{{trans('category.2')}}<br>{{trans('car')}}</p><img src="/image/category/black.png" alt="">
                </a>
                <a href="{{route('car.catalog.index')}}" class="category__block yellow">
                    <h3>{{trans('category.3')}}</h3>
                    <p>{{trans('category.4')}}</p><img src="/image/category/yellow.png" alt="">
                </a>
                <a href="{{route('car.catalog.index')}}" class="category__block white">
                    <h3>{{trans('category.5')}}</h3>
                    <p>{{trans('category.6')}}</p><img src="/image/category/white.png" alt="">
                </a>
                <a href="{{route('car.catalog.index')}}" class="category__block red">
                    <h3>{{trans('category.7')}}</h3>
                    <p>{{trans('category.8')}}</p><img src="/image/category/red.png" alt="">
                </a>
                <a href="{{route('car.catalog.index')}}" class="category__block blue">
                    <h3>{{trans('category.9')}}</h3>
                    <p>{{trans('category.10')}}</p><img src="/image/category/blue.png" alt="">
                </a>
                <a href="{{route('car.catalog.index')}}" class="category__block grey">
                    <h3>{{trans('category.11')}}</h3>
                    <p>{{trans('category.12')}}</p><img src="/image/category/grey.png" alt="">
                </a>
            </div>
        </div>
    </div>
</section>