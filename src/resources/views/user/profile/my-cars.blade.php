@extends('layouts.app') 
@section('content')

<section class="my-car">
	<div class="container">
		<div class="row">
			<div class="col">
				<h1 class="my-car-title">
					{{trans('profile.blade.1')}}
				</h1>
			</div>
		</div>
		<div class="row">
			@if (empty($myCars->toArray()))
			<div class="col-12">
				<h2 class="mb-4">{{trans('profile.blade.2')}}</h2>
				<button class='btn btn-add-car more-cars'>+ {{trans('profile.blade.3')}}</button>

			</div>
			@else
				<div class="col-sm-12" style="margin-bottom: 5em">
						<button class='btn btn-add-car more-cars'>+ {{trans('profile.blade.3')}}</button>
				</div>
			@foreach ($myCars as $item)
			<div class="col-12 col-md-6 col-lg-4">
				<div class="car-wrapper">
					@if (isset($arrayImage[$item->id]))
					<!-- https://hb.bizmrg.com/soautomedia/{{$arrayImage[$item->id]['name']}} -->
						<img src="
						https://s3-us-west-2.amazonaws.com/tourismchilliwack/wp-content/uploads/2017/04/14063401/headshot-placeholder.jpg
						" alt="" class="car-wrapper__img">
					@else
						<img src="{{ asset('image/no_image.jpg') }}" alt="" class="car-wrapper__img">
					@endif

					@if ($item->available)
						<a href="/profile/mycars/{{$item->id}}" class="car-wrapper__link d-flex flex-column justify-content-center align-items-center"></a>
						<div class="car-wrapper__subtitle car-wrapper__subtitle_green text-center">{{trans('profile.blade.4')}}</div>
					@elseif($item->available === null)
						<a href="/profile/mycars/{{$item->id}}" class="car-wrapper__link d-flex flex-column justify-content-center align-items-center"></a>
					@elseif(!$item->available)
						<a class="car-wrapper__link car-wrapper__link_error d-flex flex-column justify-content-center align-items-center"><i class="fas fa-ban"></i>{{trans('profile.blade.5')}}</a>
					@endif


					@if ($item->validate === null)
						<div class="car-wrapper__subtitle car-wrapper__subtitle_yellow text-center">{{trans('profile.blade.6')}}</div>
					@elseif($item->validate && $item->available)
						<div class="car-wrapper__subtitle car-wrapper__subtitle_green text-center">{{trans('profile.blade.7')}}</div>
					@elseif(!$item->validate)
						<div class="car-wrapper__subtitle car-wrapper__subtitle_gray text-center">{{trans('profile.blade.8')}}</div>
					@endif


					
					<div class="car-wrapper__box">
						<h1 class="car-wrapper__title">{{$item->brand}} {{$item->model}}</h1>
						<div class="car-wrapper__item d-flex">
							<div class="car-wrapper__wrap-left p-0 w-50">
								<div class="car-wrapper__inner d-flex align-items-center">
									<div class="car-wrapper__icon car-wrapper__icon_ticket"></div>
									<p class="car-wrapper__text">
										<span class="car-wrapper__span">{{$item->cost_day}} </span>{{trans('profile.blade.9')}}
									</p>
								</div>
								<div class="car-wrapper__inner d-flex align-items-center">
									<div class="car-wrapper__icon car-wrapper__icon_transmission"></div>
									<p class="car-wrapper__text">{{$item->gear}}</p>
								</div>
							</div>
							<div class="car-wrapper__wrap-right">
								<div class="car-wrapper__inner d-flex align-items-center">
									<div class="car-wrapper__icon car-wrapper__icon_event"></div>
									<p class="car-wrapper__text">{{$item->year_of_issue}}</p>
								</div>
								<div class="car-wrapper__inner d-flex align-items-center">
									<div class="car-wrapper__icon car-wrapper__icon_car"></div>
									<p class="car-wrapper__text">{{$item->car_type}}</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			@endforeach
			@endif
		</div>
	</div>
</section>


<!-- MODAL -->

<div class="modal fade" id="modal-agent" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
		  <div class="modal-content">
			<div class="modal-header">
			  <h5 class="modal-title" id="exampleModalLabel">{{trans('profile.blade.10')}}</h5>
			  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  </button>
			</div>
			<div class="modal-body">
			  <h1>{{trans('profile.blade.11')}}</h1>
			  <h2><a href="/docs/agent_dogovor.pdf">{{trans('profile.blade.12')}}</a></h2>
			  <h2><a href="/docs/agent_dogovor_yur.pdf">{{trans('profile.blade.13')}}</a></h2>
			</div>
			<div class="modal-footer">
					<button type="button" data-dismiss="modal" aria-label="Close" class="btn">{{trans('profile.blade.14')}}</button>

					<a href="{{ url('/car/create') }}" class="btn btn-add-car more-cars">
						{{trans('profile.blade.15')}}
					</a>
			</div>
		  </div>
		</div>
	  </div>


<!--  -->



{{-- <div class="container">
	<div class="row">
		<div class="col-sm-4">
			<p>Мои автомобили</p>
		</div>
		<div class="clo-sm-4"></div>
		<div class="clo-sm-4">
			<a class="dropdown-item" href="{{ route('car.create.show') }}">
				<div class="btn btn-primary">
					{{ __('form.create') }}
				</div>

			</a>
		</div>

	</div>
	<div class="row">
		<div class="col-12">
			@foreach ($myCars as $item)
			<a href="/profile/mycars/{{$item->id}}">
				<div class="card" style="width: 18rem; float: left; margin-left: 10px; margin-top: 10px">
					@if (isset($arrayImage[$item->id]))
					<!-- https://hb.bizmrg.com/soautomedia/{{$arrayImage[$item->id]['name']}} -->
					<img src="
					https://s3-us-west-2.amazonaws.com/tourismchilliwack/wp-content/uploads/2017/04/14063401/headshot-placeholder.jpg
					" style="height: 160px;" class="card-img-top" alt="..."> @else
					<img src="{{ asset('image/no_image.jpg') }}" style="height: 160px;" class="card-img-top" alt="..."> @endif
					<div class="card-body">
						<h5 class="card-title">{{$item->brand}} {{$item->model}}</h5>
					</div>
				
			</a>
			<div class="row">

			</div>
			<div class="row">
				<div class="col-sm-6">
					<p class="cardtext">Год выпуска: {{$item->year_of_issue}}</p>
					<p class="cardtext">КПП: {{$item->gear}}</p>
					<p class="cardtext">Тип кузова: {{$item->car_type}}</p>
				</div>
				<div class="col-sm-6">
					<p class="cardtext">Год выпуска: {{$item->year_of_issue}}</p>
					<p class="cardtext">КПП: {{$item->gear}}</p>
					<p class="cardtext">Тип кузова: {{$item->car_type}}</p>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					@if ($item->validate)
					<p class="text-left" style="color: green">Автомобиль допущен</p>
					@else
					<p class="text-left" style="color: red">Автомобиль на проверке</p>
					@endif
					
					@if ($item->available)
					<p class="text-left" style="color: green">Автомобиль в аренде</p>
					@else
					<p class="text-left" style="color: red">Автомобиль снят с аренды</p>
					@endif
				</div>
			</div>

		</div>





		@endforeach
		</div>
	</div>
</div> --}}

<script>
	$(function () {
    	$('.btn-add-car').click(function(){
      		$('#modal-agent').modal('toggle');
		});
	});
	

</script>

@endsection

