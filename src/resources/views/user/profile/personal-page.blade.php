@extends('layouts.app') 
@section('content')

<body>
  <div class="container profile">
    <div class="row" style="margin-top: 50px">
      <div class="col-sm-12">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="account-tab" data-toggle="tab" href="#account" role="tab" aria-controls="account" aria-selected="true">{{trans('profile.personal.1')}}</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="history-tab" data-toggle="tab" href="#history" role="tab" aria-controls="history" aria-selected="false">{{trans('profile.personal.2')}}</a>
          </li>          
          <li class="nav-item">
            <a class="nav-link" id="notify-tab" data-toggle="tab" href="#notify" role="tab" aria-controls="notify" aria-selected="false">{{trans('profile.personal.38')}}</a>
          </li>          
          <li class="nav-item">
            <a class="nav-link" id="company-tab" data-toggle="tab" href="#company" role="tab" aria-controls="company" aria-selected="false">{{trans('profile.personal.39')}}</a>
          </li>
        </ul>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12 tab-content" id="myTabContent">
        <div class="tab-pane fade all show active mb-5" id="account" role="tabpanel" aria-labelledby="account-tab">
          <section class="info">
            <form method="POST" action="profile/saveInfo">
              @csrf
              <div class="row" style="padding-top: 40px;">
               <div class="col-12 col-sm-3">
                  <div class="form-cars__wrap">
                    <h2 class="form-cars__title">
                      {{trans('profile.personal.0')}}
                    </h2>
                    <input type="text" class="login-form__input" placeholder="name" name="name" id="name" value="{{ $user->name }}" required>
                  </div>
                </div>
              </div>
              <div class="row" style="padding-top: 100px;">
                <div class="col-12 col-sm-3">
                  <div class="form-cars__wrap">
                    <h2 class="form-cars__title">
                      {{trans('profile.personal.3')}}
                    </h2>
                    <p class="login-form__input">{{ $user->email }}</p>
                  </div>
                </div>
                <div class="col-12 col-sm-3">
                  <div class="form-cars__wrap">
                    <h2 class="form-cars__title">
                      {{trans('profile.personal.4')}}
                    </h2>
                    <input type="text" class="login-form__input" placeholder="phone" name="phone" id="phone" value="{{ $user->phone }}" required>
                  </div>
                </div>
                <div class="col-12 col-sm-3">
                  <div class="form-cars__wrap">
                    <h2 class="form-cars__title">
                        {{trans('profile.personal.5')}}
                    </h2>
                    <input type="text" class="login-form__input" placeholder="country" name="country" id="country" value="{{ $user->country }}">  
                  </div>
                </div>
                <div class="col-12 col-sm-3">
                  <div class="form-cars__wrap">
                    <h2 class="form-cars__title">
                        {{trans('profile.personal.6')}}
                    </h2>
                    <input type="text" class="login-form__input" placeholder="city" name="city" id="city" value="{{ $user->city }}">
                  </div>
                </div>
              </div>
              <div class="row" style="padding-top: 40px;">
                <div class="col-12 col-sm-3">
                  <div class="form-cars__wrap">
                    <h2 class="form-cars__title">
                        {{trans('profile.personal.7')}}
                    </h2>
                    <input type="text" class="login-form__input" placeholder="Fb" name="vk_link" id="vk_link" value="{{ $user->vk_link }}">
                  </div>
                </div>
                <div class="col-12 col-sm-3">
                  <div class="form-cars__wrap">
                    <h2 class="form-cars__title">
                        {{trans('profile.personal.8')}}
                    </h2>
                    <input type="text" class="login-form__input" placeholder="Instagram" name="inst_link" id="inst_link" value="{{ $user->inst_link }}">
                  </div>
                </div>
                <div class="col-12 col-sm-3">
                  <div class="form-cars__wrap">
                    <h2 class="form-cars__title">
                        {{trans('profile.personal.9')}}
                    </h2>
                    <input type="text" class="login-form__input" placeholder="Facebook" name="fb_link" id="fb_link" value="{{ $user->fb_link }}">
                  </div>
                </div>
              </div>
              <button type="submit" class="btn login-form__button" style="margin-top: 40px;">{{trans('profile.personal.10')}}</button>
            </form>
          </section>
          <section class="mt-5">
            <div class="container">
              <h1 class="login-title">{{trans('profile.personal.11')}}</h1>
              @if (session()->get( 'response' ))
              <p>{{ session()->get( 'response' ) }}</p>
              @endif
              <form method="POST" action="profile/updateAuthUserPassword" aria-label="{{ ('Login') }}" class="login-form d-flex justify-content-between align-items-center">
                @csrf
                <div class="login-form__pass">
                  <p class="login-form__text">{{ trans('profile.personal.12') }}</p>
                  <input id="#" type="password" class="login-form__input{{ $errors->has('password') ? ' is-invalid' : '' }}" name="current"
                   minlength="6" placeholder="{{ trans('profile.personal.12') }}" required>
                </div>
                <div class="login-form__pass">
                  <p class="login-form__text">{{ trans('profile.personal.13') }}</p>
                  <input id="#" type="password" class="login-form__input{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password"
                   minlength="6" placeholder="{{ trans('profile.personal.13') }}" required>
                </div>
                <div class="login-form__pass">
                  <p class="login-form__text">{{ trans('profile.personal.14') }}</p>
                  <input id="#" type="password" class="login-form__input{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password_confirmation"
                   minlength="6" placeholder="{{ trans('profile.personal.14') }}" required>
                </div>
                <button style="height: 60px; margin-bottom: 53px" type="submit" class="btn login-form__button">{{ trans('profile.personal.15') }}</button>
              </form>
            </div>

          </section>
          <div class="row" style="margin-top: 20px">
            <div class="col-sm-12">
            <div class="card">
              <div class="card-header">
                @if ($user->isActive == null) 
                  <a href="profile/isActive" style="color: red;">{{trans('profile.personal.30')}}</a>
                @else 
                  <a href="profile/isActive" style="color: green;">{{trans('profile.personal.31')}}</a>
                @endif
              </div>
            </div>
            </div>
          </div>
        </div>
        <div class="tab-pane fade show" id="history" role="tabpanel" aria-labelledby="history-tab">
          <h2>{{trans('profile.personal.32')}}</h2>
          <div class="table">
            <div class="tr">
              <div class="table_tr">{{trans('profile.personal.33')}}</div>
              <div class="table_tr">{{trans('profile.personal.34')}}</div>
              <div class="table_tr">{{trans('profile.personal.35')}}</div>
            </div>
            @foreach ($paymentBooking as $item)
            <div class="tr">
              <div class="th" scope="row">
                {{$item->updated_at}}
              </div>
              <div class="td">
                {{$item->total_amount_booking}}
              </div>
              <div class="td">
                @if ($item->result)
                  {{trans('profile.personal.36')}}
                @else
                  {{trans('profile.personal.37')}}
                @endif
              </div>
            </div>
            @endforeach
            
          </div>
        </div>        
        <div class="tab-pane fade show" id="notify" role="tabpanel" aria-labelledby="notify-tab">
          <h2>{{trans('profile.personal.38')}}</h2>
            <div class="row" style="margin-top: 20px">
              <div class="col-sm-12">
                <div class="card">
                  <div class="card-header">
                    {{trans('profile.personal.16')}}
                  </div>
                  <div class="card-body">
                    <form method="POST" action="profile/updatePreferredTypeKPP">
                      <div class="row align-items-center">
                        <div class="col-sm-8">
                          <div class="group-ridio">
                            @csrf
                            <input name="preferred_type_KPP" class="form-check-label" 
                            {{ $user->preferred_type_KPP == 'auto' ? 'checked' : '' }} type="radio" value="auto" id="auto">
                            <label for="auto">{{trans('profile.personal.17')}}</label>
                            <input name="preferred_type_KPP" class="form-check-label" 
                            {{ $user->preferred_type_KPP == 'mech' ? 'checked' : '' }} type="radio" value="mech" id="mech">
                            <label for="mech">{{trans('profile.personal.18')}}</label>
                          </div>
                        </div>
                      
                        <div class="col-sm-4">
                          <button type="submit" class="btn login-form__button">{{trans('profile.personal.19')}}</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            <div class="row" style="margin-top: 20px">
              <div class="col-sm-12">
                <div class="card">
                  <div class="card-header">{{trans('profile.personal.20')}}</div>
                  <div class="card-body">
                    <form method="POST" action="profile/saveNotifications">
                      <div class="row align-items-center">
                      <div class="col-sm-8">
                        <div class="checkbox">
                        @csrf
                        <input id="email" type="checkbox" name="email" {{ $user->email_notif ? 'checked' : '' }} class="check">
                          <label for="email">email</label>
                        <input id="sms" type="checkbox" name="sms" {{ $user->sms_notif ? 'checked' : '' }} class="check">
                          <label for="sms">sms</label>
                        <input id="app" type="checkbox" name="app" {{ $user->app_notif ? 'checked' : '' }} class="check">
                          <label for="app">app</label>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <button type="submit" class="btn login-form__button">{{trans('profile.personal.21')}}</button>
                      </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
        </div>
        <div class="tab-pane fade show" id="company" role="tabpanel" aria-labelledby="company-tab">
          <h2>{{trans('profile.personal.39')}}</h2>
          <div class="row" style="margin-top: 20px">
            <div class="col-sm-12">
              <div class="card">
                <div class="card-header">
                  {{trans('profile.personal.22')}}
                </div>
                <div class="card-body">
                  <form method="POST" action="profile/updateExperience">
                    <div class="row align-items-center">
                      <div class="col-sm-8">
                        @csrf
                        <div class="login-form__pass">
                          <p class="login-form__text">{{trans('profile.personal.23')}}</p>
                          <input type="number" class="login-form__input" min="0" name="driving_experience"
                                 value="{{ $user->driving_experience }}" required>
                        </div>
                      </div>

                      <div class="col-sm-4">
                        <button type="submit" class="btn login-form__button">{{trans('profile.personal.24')}}</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <div class="row" style="margin-top: 20px">
            <div class="col-sm-12">
              <div class="card">
                <div class="card-header">
                  {{trans('profile.personal.25')}}
                </div>
                <div class="card-body">
                  <form method="POST" action="profile/updateBirthday">
                    <div class="row align-items-center">
                      <div class="col-sm-8">
                        @csrf
                        <div class="login-form__pass">
                          <p class="login-form__text">{{trans('profile.personal.25')}}</p>
                          <input type="date" class="login-form__input" name="birthday"
                                 value="{{ $user->birthday }}" required>
                        </div>
                      </div>

                      <div class="col-sm-4">
                        <button type="submit" class="btn login-form__button">{{trans('profile.personal.26')}}</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <div class="row" style="margin-top: 20px">
            <div class="col-12">
              <ul class="card-list">
                <li class="card__photo card-list__item d-flex justify-content-between align-items-center">
                  <h2 class="card-list__title card-list__title_small">
                      {{trans('profile.personal.27')}}
                  </h2>
                  <div class="card-list__Upload">
                    <input type="file" id="avatar" name="avatar" value="">
                      @if ($user->avatar) 
                        <img id="blah" src="{{ $user->avatar }}" width="300" />
                      @endif
                  </div>
                </li>
              </ul>
            </div>
          </div>
          <div class="row" style="margin-top: 20px">
            <div class="col-sm-12">
            <div class="card">
              <div class="card-header">
              {{trans('profile.personal.28')}}
              </div>
              <div class="card-body">
              <p>{{trans('profile.personal.29')}}: </p>
              </div>
            </div>
            </div>
          </div>
        </div> 
		  </div>
		</div>
</div>


</body>
@endsection