@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Update phone') }}</div>

                <div class="card-body">
                    <form id="form_phone_check" method="POST" action="{{ route('user.phone.update') }}" aria-label="{{ __('Update phone') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="phone" class="col-md-4 col-form-label text-md-right">{{ __('Personal phone') }}</label>

                            <div class="col-md-6">
                                <input
                                    id="phone"
                                    type="tel"
                                    pattern="[0-9]{3}[0-9]{3}[0-9]{4}"
                                    class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}"
                                    name="phone"
                                    value="{{ old('phone') }}"
                                    required>
                                <span class="note">Format: 9001112233</span>

                                @if ($errors->has('phone'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <a id="sendSms" class="btn btn-primary">
                                    {{ __('Send Sms') }}
                                </a>
                            </div>
                        </div>
                        <br>

                        <div class="form-group row">
                            <label for="sms_code" class="col-md-4 col-form-label text-md-right">{{ __('Sms code') }}</label>

                            <div class="col-md-6">
                                <input id="sms_code" class="form-control{{ $errors->has('sms_code') ? ' is-invalid' : '' }}" name="sms_code" required>

                                @if ($errors->has('sms_code'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('sms_code') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Check code') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
