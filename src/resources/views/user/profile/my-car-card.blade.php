@extends('layouts.app')

@section('content')


@if (!$data['oferta'])
<div class="alert alert-danger d-flex justify-content-center align-items-center" role="alert">
  <span>{{trans('profile.card.1')}}</span>
  <a href="{{route('car.show', $data['id'])}}" class="btn ml-5">{{trans('profile.card.2')}}</a>
</div>
@endif

<section class="about-car">
  <div class="container">
    <div class="row">
      <div class="col">
        <h1 class="about-car__title">
          {{trans('profile.card.3')}}
        </h1>
      </div>
    </div>
    <div class="row">
      <div class="col-12 col-md-12 col-lg-6">
        <div class="img-card1 d-flex">
          <div class="img-card1__wrap">
            @foreach ($myCarsImages as $item)
            @if ($item->avatar == 1)
            <!-- https://hb.bizmrg.com/soautomedia/{{$item->name}} -->
            <a href="image/user.jpg

            " class="img-card1__link" data-fancybox="gallery">
            <!-- https://hb.bizmrg.com/soautomedia/{{$item->name}} -->
              <img src="image/user.jpg
              " alt="" class="img-card1__img">
            </a>
            @if (!$myCarData[0]->validate)
              <p>{{trans('profile.card.4')}}</p>     
            @endif
            @break
            @endif
            @endforeach
          </div>
          <div class="img-card1__wrap d-flex flex-column">
            <div class="img-card1__wrapper d-flex">
              @foreach ($myCarsImages as $item)
              @if (++$loop->index < 1) @continue @endif @if (++$loop->index > 7)
                <div class="img-card1__inner" style='display: none'>
                  <!-- https://hb.bizmrg.com/soautomedia/{{$item->name}} -->
                  <a href="image/user.jpg
                  " class="img-card1__link"
                    data-fancybox="gallery">
                    <!-- https://hb.bizmrg.com/soautomedia/{{$item->name}} -->
                    <img src="image/user.jpg
                    " alt="" class="img-card1__img-small">
                  </a>
                </div>
                @else
                <div class="img-card1__inner">
                  <!-- https://hb.bizmrg.com/soautomedia/{{$item->name}} -->
                  <a href="image/user.jpg
                  " class="img-card1__link"
                    data-fancybox="gallery">
                    <!-- https://hb.bizmrg.com/soautomedia/{{$item->name}} -->
                    <img src="image/user.jpg
                    " alt="" class="img-card1__img-small">
                  </a>
                </div>
                @endif

                @endforeach
            </div>
          </div>
        </div>
      </div>
      <div class="col-12 col-md-12 col-lg-6">
        <div class="content-card1">
          <div class="content-card1__wrap d-flex justify-content-between">
            <h1 class="content-card1__title">
              {{$myCarData[0]->brand}} {{$myCarData[0]->model}}
            </h1>
            <a href="{{ route('profile.EditMyCarController', $data['id']) }}" class="content-card1__link">
              <i class="fas fa-pencil-alt"></i>
              {{trans('profile.card.5')}}
            </a>
          </div>
          <div class="content-card1__wrap d-flex justify-content-between">
            <div class="content-card1__wrapper">
              <div class="content-card1__inner d-flex align-items-center">
                <div class="content-card1__icon content-card1__icon_industry"></div>
                <p class="content-card1__text">
                  {{$myCarData[0]->gear}}
                </p>
              </div>
              <div class="content-card1__inner d-flex align-items-center">
                <div class="content-card1__icon content-card1__icon_jobtitle"></div>
                <p class="content-card1__text">
                  {{$myCarData[0]->car_type}}
                </p>
              </div>
              <div class="content-card1__inner d-flex align-items-center">
                <div class="content-card1__icon content-card1__icon_experience"></div>
                <p class="content-card1__text">
                  <span class="content-card1__span">
                    {{$myCarData[0]->year_of_issue}}
                  </span>
                  {{trans('profile.card.44')}}
                </p>
              </div>
            </div>
            <div class="content-card1__wrapper">
              <div class="content-card1__inner d-flex align-items-center">
                <div class="content-card1__icon content-card1__icon_time"></div>
                <p class="content-card1__text">
                  {{$myCarData[0]->doors}}
                </p>
              </div>
              <div class="content-card1__inner d-flex align-items-center">
                <div class="content-card1__icon content-card1__icon_level"></div>
                <p class="content-card1__text">
                  {{$myCarData[0]->seat}}
                </p>
              </div>
              <div class="content-card1__inner d-flex align-items-center">
                <div class="content-card1__icon content-card1__icon_salary"></div>
                <p class="content-card1__text">
                  <span class="content-card1__span">
                    {{$myCarData[0]->cost_day}}
                  </span>
                  {{trans('profile.card.6')}}
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row mt-5">
      <div class="col-sm-12">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="all-tab" data-toggle="tab" href="#all" role="tab"
               aria-controls="all" aria-selected="true">{{trans('profile.card.7')}}</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="brone-tab" data-toggle="tab" href="#broned" role="tab"
               aria-controls="brone" aria-selected="false">{{trans('profile.card.8')}}</a>
          </li>
<!--           <li class="nav-item">
            <a class="nav-link" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home"
               aria-selected="false">{{trans('profile.card.9')}}</a>
          </li>          
          <li class="nav-item">
            <a class="nav-link" id="location-tab" data-toggle="tab" href="#location" role="tab" aria-controls="location"
               aria-selected="false">{{trans('profile.card.42')}}</a>
          </li>          
          <li class="nav-item">
            <a class="nav-link" id="feature-tab" data-toggle="tab" href="#feature" role="tab" aria-controls="feature"
               aria-selected="false">{{trans('profile.card.43')}}</a>
          </li> -->
        </ul>
      </div>
      <div class="col-sm-12">
        <div class="tab-content" id="myTabContent">

<!-- VACATIONS -->

          <div class="tab-pane fade all show active" id="all" role="tabpanel" aria-labelledby="all-tab">
            <div class="row">
              @foreach ($bookingDatesCurrent as $item)
                <div class="col-lg-6 col-md-6 col-12">
                  <div class="car-wrapper" style="background: none;padding: 0;">
                    <a href="{{ route('profile.my-car.booking', $item->id) }}"
                       class="car-wrapper__link d-flex flex-column justify-content-center align-items-center"></a>
                       <!-- https://hb.bizmrg.com/soautomedia/{{$item->cars->avatar->name ?? ''}} -->
                    <img src="image/user.jpg
                    " alt=""
                         class="car-wrapper__img">
                    <div class="car-wrapper__box">
                      <a href="{{ route('profile.my-car.booking', $item->id) }}">
                        <h1 class="car-wrapper__title">{{$item->cars->brand['name']}} {{$item->cars->model['name']}}
                        </h1>
                      </a>
                      <div class="car-wrapper__item d-flex">
                        <div class="car-wrapper__wrap-left p-0 w-100">
                          <div class="car-wrapper__inner d-flex">
                            <p class="car-wrapper__text_f">{{trans('profile.card.10')}}:</p>
                            <p class="car-wrapper__text_s  ml-2">{{$item->user_name}}</p>
                          </div>
                          <div class="car-wrapper__inner d-flex">
                            <p class="car-wrapper__text_f">{{trans('profile.card.11')}}:</p>
                            <p class="car-wrapper__text_s  ml-2" style="width: 50%;font-size: 13px;">
                              С: {{$item->datefrom}}
                              <br>По: {{$item->dateto}}</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              @endforeach
            </div>
          </div>

<!-- CURRENT WORK -->

          <div class="tab-pane fade show" id="broned" role="tabpanel" aria-labelledby="broned-tab">
            <div class="row">
              @foreach ($bookingDatesNotPaid as $item)
                <div class="col-lg-6 col-md-6 col-12">
                  <div class="car-wrapper" style="background: none;padding: 0;">
                    <a href="{{ route('profile.my-car.booking', $item->id) }}"
                       class="car-wrapper__link d-flex flex-column justify-content-center align-items-center"></a>
                       <!-- https://hb.bizmrg.com/soautomedia/{{$item->cars->avatar->name ?? ''}} -->
                    <img src="image/user.jpg
                    " alt=""
                         class="car-wrapper__img">
                    <div class="car-wrapper__box">
                      <a href="{{ route('profile.my-car.booking', $item->id) }}">
                        <h1 class="car-wrapper__title">{{$item->cars->brand['name']}} {{$item->cars->model['name']}}
                        </h1>
                      </a>
                      <div class="car-wrapper__item d-flex">
                        <div class="car-wrapper__wrap-left p-0 w-100">
                          <div class="car-wrapper__inner d-flex">
                            <p class="car-wrapper__text_f">{{trans('profile.card.12')}}</p>
                            <p class="car-wrapper__text_s ml-2">{{$item->user_name}}</p>
                          </div>
                          <div class="car-wrapper__inner d-flex">
                            <p class="car-wrapper__text_f">{{trans('profile.card.13')}}</p>
                            <p class="car-wrapper__text_s ml-2" style="width: 50%;font-size: 13px;">
                              С: {{$item->datefrom}}
                              <br>По: {{$item->dateto}}</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              @endforeach
            </div>
          </div>

<!--  -->

          <div class="tab-pane fade show" id="home" role="tabpanel" aria-labelledby="home-tab">
            <div class="row">
              @foreach ($bookingDatesHistory as $item)
                <div class="col-lg-6 col-md-6 col-12">
                  <div class="car-wrapper" style="background: none;padding: 0;">
                    <a href="{{ route('profile.my-car.booking', $item->id) }}"
                       class="car-wrapper__link d-flex flex-column justify-content-center align-items-center"></a>
                       <!-- https://hb.bizmrg.com/soautomedia/{{$item->cars->avatar->name ?? ''}} -->
                    <img src="image/user.jpg
                    " alt=""
                         class="car-wrapper__img">
                    <div class="car-wrapper__box">
                      <a href="{{ route('profile.my-car.booking', $item->id) }}">
                        <h1 class="car-wrapper__title">{{$item->cars->brand['name']}} {{$item->cars->model['name']}}
                        </h1>
                      </a>
                      <div class="car-wrapper__item d-flex">
                        <div class="car-wrapper__wrap-left p-0 w-100">
                          <div class="car-wrapper__inner d-flex">
                            <p class="car-wrapper__text_f">{{trans('profile.card.14')}}</p>
                            <p class="car-wrapper__text_s ml-2">{{$item->user_name}}</p>
                          </div>
                          <div class="car-wrapper__inner d-flex">
                            <p class="car-wrapper__text_f">{{trans('profile.card.15')}}</p>
                            <p class="car-wrapper__text_s ml-2" style="width: 50%;font-size: 13px;">
                              С: {{$item->datefrom}}
                              <br>По: {{$item->dateto}}</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              @endforeach
            </div>
          </div>

<!--  -->

          <div class="tab-pane fade show" id="location" role="tabpanel" aria-labelledby="location-tab">
              <div class="row">
                <div class="col">
                  <div class="rent p-3 rent-location d-flex justify-content-around align-items-center">
                    <form id="editLocation" method="POST" action="{{ route('car.location', $myCarData[0]->id) }}"
                      class="d-flex justify-content-around align-items-center">
                      @csrf
                      <h2 class="rent__title">
                        {{trans('profile.card.16')}}
                      </h2>
                      <div class="rent-form__start time-container">
                        <p class="rent-form__title">
                          {{trans('profile.card.17')}}
                        </p>
                        <input name="location" class="login-form__input" type="text" value="{{$data['location']}}" required
                          autocomplete="off">
                      </div>
                      <div class="rent-form__start time-container">
                        <p class="rent-form__title">
                          {{trans('profile.card.18')}}
                        </p>
                        {{-- <textarea name="location_description" class="login-form__input" type="text" required autocomplete="off">{{$data['location_description']}}
                        </textarea> --}}
                      </div>
                      <div class="rent__row">
                        <button type="submit" class="btn rent-btn">
                          {{trans('profile.card.19')}}
                        </button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
          </div>

<!--  -->

          <div class="tab-pane fade show" id="feature" role="tabpanel" aria-labelledby="feature-tab">
            <div class="card__featured card-list__item">
              <form id="saveFeatured" method="POST" action="/car/{{$data['id']}}/feature">
                @csrf
                <div class="row">
                  <div class="col-12">
                    <ul class="extra-wrap">
                      @foreach ($data['feature'] as $title => $check)
                        @if (isset($dataFeature[$title]))
                        <li class="extra-wrap__item">
                          <div class="checkbox">
                            <input id="{{$title}}" type="checkbox" name="{{$title}}" value="1" class="check" @if ($check) checked
                              @endif>
                            <label for="{{$title}}">
                              {{ $dataFeature[$title] }}
                            </label>
                          </div>
                        </li>
                        @endif
                      @endforeach
                    </ul>
                  </div>
                </div>
                <div class="row">
                  <div class="col d-flex justify-content-end">
                    <button type="submit" class="btn card-list__btn d-flex justify-content-center">
                      {{trans('profile.card.33')}}
                    </button>
              </form>
            </div>
          </div>
        </div>
      </div>


    </div>




  @foreach ($date_rent_cars as $item)

    @if (new DateTime($item->to) < new DateTime) @continue @endif <div class="row">
      <div class="col">
        <div class="rent p-3 d-flex justify-content-center align-items-center flex-wrap">

          @if ($item->active == 1)
          <h2 class="rent__title w-100 text-center">
            {{trans('profile.card.20')}}<u>{{$item->from}}</u>{{trans('profile.card.21')}}  <u>{{$item->to}}</u>
          </h2>
          @else
          <h2 class="rent__title w-100 text-center">
            {{trans('profile.card.22')}}<u>{{$item->from}}</u> {{trans('profile.card.23')}} <u>{{$item->to}}</u>
          </h2>
          @endif

          <form id="editRentCar" method="POST" action="{{ route('car.editRentCar', $item->car_id) }}"
            class="d-flex justify-content-around align-items-center">
            @csrf
            <input type="hidden" name="id" value="{{$item->id}}">
            <input type="hidden" name="active" value="{{$item->active}}">
            <div class="rent__row">
              @if ($item->active == 1)
              <button type="submit" class="btn rent-btn">
                {{trans('profile.card.24')}}
              </button>
              @else
              <button type="submit" class="btn rent-btn">
                {{trans('profile.card.25')}}
              </button>
              @endif
            </div>
          </form>

          <form id="deleteRentCar" method="DELETE" action="{{ route('car.deleteRentCar', $item->car_id) }}"
            class="d-flex justify-content-around align-items-center">
            @csrf
            <input type="hidden" name="id" value="{{$item->id}}">
            <input type="hidden" name="active" value="{{$item->active}}">
            <div class="rent__row">
              <button type="submit" class="btn rent-btn">
                {{trans('profile.card.26')}}
              </button>
            </div>
          </form>


        </div>

      </div>
  </div>
  @endforeach





@if ($myCarData[0]->validate && $data['location'])
<!--  && $data['location_description']  -->
  <div class="row">
    <div class="col">
      <div class="rent d-flex justify-content-around align-items-center">
        <form id="goToRentCar" method="POST" action="/profile/torentcar"
          class="d-flex justify-content-around align-items-center">
          @csrf
          <input type="hidden" name="car_id" value="{{$myCarData[0]->id}}">
          <input type="hidden" name="active" value="1">
          <h2 class="rent__title">
            {{trans('profile.card.27')}}
          </h2>
          <div class="rent-form__start time-container">
            <p class="rent-form__title">
              {{trans('profile.card.28')}}
            </p>
            <input name="from" class="rent-form__select datepicker-here" data-timepicker="true" type="text" required
              autocomplete="off">
          </div>
          <div class="rent-form__end">
            <p class="rent-form__title">
              {{trans('profile.card.29')}}
            </p>
            <input name="to" class="rent-form__select datepicker-here" data-timepicker="true" type="text" required
              autocomplete="off">
          </div>
          {{-- <div class="rent-form__input">
                <p class="rent-form__title">
                  {{trans('profile.card.30')}}
                </p>
                @if (!$install_tracker)
                  <input type="text" class="login-form__input" name="locationCar" placeholder="{{trans('profile.card.99')}}" required>
                @endif
              </div> --}}


          <div class="rent__row">
            <button type="submit" class="btn rent-btn">
              Сдать
            </button>
          </div>
        </form>
      </div>
    </div>
  </div>
  @elseif(!$myCarData[0]->validate)
  <p>{{trans('profile.card.31')}}</p>
  @elseif(!$data['location'] && !$data['location_description'])
  <p>{{trans('profile.card.32')}}</p>
  @endif 






  </div>
  </form>
  </div>

  </div>
  </div>
</section>



<!-- MODAL -->

<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">{{trans('profile.card.34')}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        </button>
      </div>
      <div class="modal-body">
        <h1>{{trans('profile.card.35')}}</h1>
        <h2>{{trans('profile.card.36')}}</h2>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-close creating-cars-btn mt-0" data-dismiss="modal">{{trans('profile.card.37')}}</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-location" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">{{trans('profile.card.38')}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        </button>
      </div>
      <div class="modal-body">
        <h1>{{trans('profile.card.39')}}</h1>
        <h2>{{trans('profile.card.40')}}</h2>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-close btn-location creating-cars-btn mt-0" data-dismiss="modal">{{trans('profile.card.41')}}</button>
      </div>
    </div>
  </div>
</div>

<script>
  var registrationDone = {{$myCarData[0]->oferta}};
  var car_id = {{$myCarData[0]->id}};
  var locationFill = {{$myCarData[0]->validate and !($data['location'])}};// && $data['location_description']
  $(function () {

    console.log(registrationDone);
    if (registrationDone == 1) {
      console.log(Cookies.get('modal'));
    
    if (Cookies.get('modal') == 'show') {
      $('#modal').modal('show');
      Cookies.remove('modal')
    }

    $('.btn-close').click(function(){
      $('#modal').modal('toggle');
    });
    } else {
      $('#modal').modal('show');
      $('#modal .modal-body').text('Вы не завершили регистрацию авто. Пожалуйста, заполните оставшиеся данные. Перейти на ');
      $('#modal .modal-body').append('<a href="/car/' + car_id + '">страницу</a>');
    }
    if (locationFill) {
      $('#modal-location').modal('show');
    }
    $('.btn-location').click(function(){
      $('#modal-location').modal('toggle');
    });

  });
</script>

{{-- <script>
      var map;
    function initMap() {
        var myLatLng = {lat: 48.7638916, lng: 44.546945};

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 18,
            center: myLatLng
        });

        setInterval(function () {
            $.ajax({
                url: "/profile/getMyCarLocation/" + car_id,
                success: function(msg){

                    console.log(msg.location);

                    var marker = new google.maps.Marker({
                        position: msg.location,
                        map: map,
                        title: 'Hello World!'
                    });
                }
            });
        }, 3000);


    }

    </script> --}}
@endsection