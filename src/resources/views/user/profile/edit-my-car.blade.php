@extends('layouts.app')
@section('content')

<section class="about-car edit-car">
  <div class="container">
    <div class="row">
      <div class="col">
          <a href="/profile/mycars/{{$car->id}}" class="content-card1__link back_to_cars">
            {{trans('profile.edit.1')}}
          </a>
          <h1 class="about-car__title">
            {{trans('profile.edit.2')}} {{$car->brand->name}} {{$car->models_name}}
          </h1>
          <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#all" role="tab" aria-controls="all" aria-selected="true">{{trans('profile.edit.3')}}</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="false">{{trans('profile.edit.4')}}</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">{{trans('profile.edit.5')}}</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="deposit-tab" data-toggle="tab" href="#deposit" role="tab" aria-controls="deposit" aria-selected="false">{{trans('profile.edit.6')}}</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="type-tab" data-toggle="tab" href="#type" role="tab" aria-controls="type" aria-selected="false">{{trans('profile.edit.7')}}</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="services-tab" data-toggle="tab" href="#services" role="tab" aria-controls="services" aria-selected="false">{{trans('profile.edit.8')}}</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="insurance-tab" data-toggle="tab" href="#insurance" role="tab"
                 aria-controls="insurance" aria-selected="false">{{trans('profile.edit.9')}}</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="ban-tab" data-toggle="tab" href="#ban" role="tab" aria-controls="ban" aria-selected="false">{{trans('profile.edit.10')}}</a>
            </li>
          </ul>



          <div class="tab-content" id="myTabContent">


<!-- PROFILE -->

            <div class="tab-pane fade all show active" id="all" role="tabpanel" aria-labelledby="all-tab">
              {{--<form id="editBrand" method="POST" action="{{ route('car.brand', $car->id) }}">--}}
              {{--@csrf--}}
              {{--<input type="hidden" name="cars_id" value="{{$car->id}}">--}}
              {{--<div class="d-flex justify-content-between">--}}
              {{--<div class="form-production-date description_text">--}}
              {{--<h2 class="form-production-date__title">--}}
              {{--Брэнд авто--}}
              {{--</h2>--}}
              {{--<select id="brand_id" name="brand_id" class="form-cars__select">--}}
              {{--<option disabled selected>Ничего не выбрано</option>--}}
              {{--@foreach (\App\Models\BrandsCar::all() as $item)--}}
              {{--<option--}}
              {{--@if ($car['brand_id'] == $item['id'])--}}
              {{--selected--}}
              {{--@endif--}}
              {{--value="{{ $item['id'] }}">--}}
              {{--{{ $item['name'] }}--}}
              {{--</option>--}}
              {{--@endforeach--}}
              {{--</select>--}}
              {{--</div>--}}
              {{--<button type="submit" class="btn card-list__btn">--}}
              {{--Сохранить--}}
              {{--</button>--}}
              {{--</div>--}}
              {{--</form>--}}



              <div class="card card-default">
                <div class="card-header">
                  <h2>{{trans('profile.edit.57')}}</h2>
                </div>
                <div class="card-body">
                  <form id="editModel" method="POST" action="{{ route('car.model', $car->id) }}">
                    @csrf
                    <input type="hidden" name="cars_id" value="{{$car->id}}">
                    <div class="d-flex justify-content-between">
                      <div class="form-production-date description_text">
                        <h2 class="form-production-date__title">
                          {{trans('profile.edit.11')}}
                        </h2>
                        <select id="model_id" name="model_id" class="form-cars__select">
                          <option disabled selected>Ничего не выбрано</option>
                          @foreach (App\Models\ModelsCar::where('brand_id',$car['brand_id'])->get() as $item)

                            <option data-other="{{$item->is_other}}"
                                    @if ($car['model_id'] == $item['id'])
                                    selected
                                    @endif
                                    value="{{ $item['id'] }}">
                              {{ $item['name'] }}

                            </option>
                          @endforeach
                        </select>
                        @php
                          $model = \App\Models\ModelsCar::find($car['model_id'])
                        @endphp
                        <div class="row mt-3" id="new-model-name"
                             style="@if(!$model or !$model->is_other) display: none; @endif">
                          <div class="col-sm-12">

                            <h2 class="form-production-date__title">
                              {{trans('profile.edit.12')}}
                            </h2>
                            <input class="login-form__input" name="model_name" value="{{$car->model_name ?? 'Другая'}}"
                                   required>
                          </div>

                        </div>

                      </div>
                      <button type="submit" class="btn card-list__btn">
                        {{trans('profile.edit.13')}}
                      </button>
                    </div>
                  </form>
                  <form id="editGos" method="POST" action="{{ route('car.editGos', $car->id) }}">
                      @csrf
                      <input type="hidden" name="cars_id" value="{{$car->id}}">
                      <div class="d-flex justify-content-between">
                        <div class="form-production-date description_text">
                          <h2 class="form-production-date__title">
                            {{trans('profile.edit.14')}}
                          </h2>
                          <textarea type="number" class="login-form__input" name="gos_nomer" value="{{$car->gos_nomer}}" required>{{$car->gos_nomer}}</textarea>
                        </div>
                        <button type="submit" class="btn card-list__btn">
                          {{trans('profile.edit.15')}}
                        </button>
                      </div>
                  </form>
                  <form id="editDescription" method="POST" action="{{ route('car.description') }}">
                    @csrf
                    <input type="hidden" name="cars_id" value="{{$car->id}}">
                    <div class="d-flex justify-content-between">
                      <div class="form-production-date description_text">
                        <h2 class="form-production-date__title">
                          {{trans('profile.edit.16')}}
                        </h2>
                        <textarea type="number" class="login-form__input" name="description" value="{{$car->description['description']}}" required>{{$car->description['description']}}</textarea>
                      </div>
                      <button type="submit" class="btn card-list__btn">
                        {{trans('profile.edit.17')}}
                      </button>
                    </div>
                  </form>
                  <form id="editCity" method="POST" action="{{ route('car.editCity', $car->id) }}">
                    @csrf
                    <input type="hidden" name="cars_id" value="{{$car->id}}">
                    <div class="d-flex justify-content-between">
                      <div class="form-production-date description_text">
                        <h2 class="form-production-date__title">
                          {{trans('profile.edit.18')}}
                        </h2>
                        <select name="city" id="city" class="form-cars__select">
                        @foreach ($city as $item)
                          <option 
                            @if ($car['city'] == $item['id'])
                              selected
                            @endif
                          value="{{$item->id}}">{{$item->name}}</option>
                        @endforeach
                      </select>   
                      </div>
                      <button type="submit" class="btn card-list__btn">
                        {{trans('profile.edit.19')}}
                      </button>
                    </div>
                  </form>
                  <form id="editCarType" method="POST" action="{{ route('car.editCarType', $car->id) }}">
                    @csrf
                    <input type="hidden" name="cars_id" value="{{$car->id}}">
                    <div class="d-flex justify-content-between">
                      <div class="form-production-date description_text">
                        <h2 class="form-production-date__title">
                          {{trans('profile.edit.20')}}
                        </h2>
                        <select name="cartype" id="cartype" class="form-cars__select">
                        @foreach ($carType as $item)
                          <option 
                            @if ($car['car_type'] == $item['id'])
                              selected
                            @endif
                          value="{{$item->id}}">{{$item->name}}</option>
                        @endforeach
                      </select>   
                      </div>
                      <button type="submit" class="btn card-list__btn">
                        {{trans('profile.edit.21')}}
                      </button>
                    </div>
                  </form>
                  <form id="editCostDay" method="POST" action="{{ route('car.editCostDay', $car->id) }}">
                    @csrf
                    <div class="d-flex justify-content-between">
                      <div class="form-production-date">
                        <h2 class="form-production-date__title">
                          {{trans('profile.edit.22')}}
                        </h2>
                        <input type="number" class="login-form__input" name="cost_day" value="{{$car->cost_day}}" required>
                      </div>
                      <button type="submit" class="btn card-list__btn">
                        {{trans('profile.edit.23')}}
                      </button>
                    </div>
                  </form>
                  <form id="editGear" method="POST" action="{{ route('car.gear', $car->id) }}">
                      @csrf
                      <input type="hidden" name="cars_id" value="{{$car->id}}">
                      <div class="d-flex justify-content-between">
                        <div class="form-production-date description_text">
                          <h2 class="form-production-date__title">
                            {{trans('profile.edit.24')}}
                          </h2>
                          <select id="gear_id" name="gear_id" class="form-cars__select">
                            <option disabled selected>{{trans('profile.edit.25')}}</option>
                              @foreach ($gear as $item)
                                  <option
                                  @if ($car['gear_id'] == $item['id'])
                                      selected
                                  @endif
                                  value="{{ $item['id'] }}">
                                      {{ $item['name'] }}
                                  </option>
                              @endforeach
                            </select>
                        </div>
                        <button type="submit" class="btn card-list__btn">
                          {{trans('profile.edit.26')}}
                        </button>
                      </div>
                  </form>
                  <form id="editTransmission" method="POST" action="{{ route('car.transmission', $car->id) }}">
                    @csrf
                    <input type="hidden" name="cars_id" value="{{$car->id}}">
                    <div class="d-flex justify-content-between">
                      <div class="form-production-date description_text">
                        <h2 class="form-production-date__title">
                          {{trans('profile.edit.27')}}
                        </h2>
                        <select id="transmission_id" name="transmission_id" class="form-cars__select">
                          <option disabled selected>{{trans('profile.edit.28')}}</option>
                          @foreach (\App\Models\CarTransmission::all() as $item)
                            <option
                                    @if ($car['transmission_id'] == $item['id'])
                                    selected
                                    @endif
                                    value="{{ $item['id'] }}">
                              {{ $item['name'] }}
                            </option>
                          @endforeach
                        </select>
                      </div>
                      <button type="submit" class="btn card-list__btn">
                        {{trans('profile.edit.29')}}
                      </button>
                    </div>
                  </form>
                  <form id="editTransmission" method="POST" action="{{ route('car.fuel', $car->id) }}">
                    @csrf
                    <input type="hidden" name="cars_id" value="{{$car->id}}">
                    <div class="d-flex justify-content-between">
                      <div class="form-production-date description_text">
                        <h2 class="form-production-date__title">
                          {{trans('profile.edit.30')}}
                        </h2>
                        <select id="transmission_id" name="fuel_type_id" class="form-cars__select">
                          <option disabled selected>{{trans('profile.edit.31')}}</option>
                          @foreach (\App\Models\FuelType::all() as $item)
                            <option
                                    @if ($car['fuel_type_id'] == $item['id'])
                                    selected
                                    @endif
                                    value="{{ $item['id'] }}">
                              {{ $item['name'] }}
                            </option>
                          @endforeach
                        </select>
                      </div>
                      <button type="submit" class="btn card-list__btn">
                        {{trans('profile.edit.32')}}
                      </button>
                    </div>
                  </form>
                  <form id="editPower" method="POST" action="{{ route('car.editPower', $car->id) }}">
                        @csrf
                        <div class="d-flex justify-content-between">
                            <div class="form-production-date">
                                <h2 class="form-production-date__title">
                                    {{trans('profile.edit.33')}}
                                </h2>
                                <input type="number" class="login-form__input" name="power" value="{{$car->power}}"
                                       required>
                            </div>
                            <button type="submit" class="btn card-list__btn">
                                {{trans('profile.edit.34')}}
                            </button>
                        </div>
                  </form>
                  <form id="editAssessedValue" method="POST" action="{{ route('car.editCarPrice', $car->id) }}">
                    @csrf
                    <div class="d-flex justify-content-between">
                      <div class="form-production-date">
                        <h2 class="form-production-date__title">
                          {{trans('profile.edit.35')}}
                        </h2>
                        <input type="number" class="login-form__input" name="assessed_value" value="{{$car->assessed_value}}"
                               required>
                      </div>
                      <button type="submit" class="btn card-list__btn">
                        {{trans('profile.edit.36')}}
                      </button>
                    </div>
                  </form>
                  <form id="editSeats" method="POST" action="{{ route('car.editSeats', $car->id) }}">
                    @csrf
                    <div class="d-flex justify-content-between">
                      <div class="form-production-date">
                        <h2 class="form-production-date__title">
                          {{trans('profile.edit.37')}}
                        </h2>
                        <input type="number" class="login-form__input" name="seat" value="{{$car->seat}}"
                               required>
                      </div>
                      <button type="submit" class="btn card-list__btn">
                        {{trans('profile.edit.38')}}
                      </button>
                    </div>
                  </form>
                  <form id="editVolume" method="POST" action="{{ route('car.editVolume', $car->id) }}">
                        @csrf
                        <div class="d-flex justify-content-between">
                            <div class="form-production-date">
                                <h2 class="form-production-date__title">
                                    {{trans('profile.edit.39')}}
                                </h2>
                                <input type="number" class="login-form__input" step="0.1" name="volume" value="{{$car->engine_volume}}"
                                       required>
                            </div>
                            <button type="submit" class="btn card-list__btn">
                                {{trans('profile.edit.40')}}
                            </button>
                        </div>
                  </form>
                  <form id="editMileageInclusive" method="POST" action="{{ route('car.editMileageInclusive', $car->id) }}">
                      @csrf
                      <div class="d-flex justify-content-between">
                        <div class="number-dor">
                          <h2 class="form-production-date__title">
                            {{trans('profile.edit.41')}}
                          </h2>
                          <input type="number" class="login-form__input" name="day" placeholder="км" value="{{$car->mileageInclusive['day']}}" required>
                        </div>
                        <div class="number-dor">
                          <h2 class="form-production-date__title">
                            {{trans('profile.edit.42')}}
                          </h2>
                          <input type="number" class="login-form__input" name="week" placeholder="км" value="{{$car->mileageInclusive['week']}}" required>
                        </div>
                        <div class="number-dor">
                          <h2 class="form-production-date__title">
                            {{trans('profile.edit.43')}}
                          </h2>
                          <input type="number" class="login-form__input" name="month" placeholder="км" value="{{$car->mileageInclusive['month']}}"  required>
                        </div>
                        <button type="submit" class="btn card-list__btn">
                          {{trans('profile.edit.44')}}
                        </button>
                      </div>
                  </form>
                  <form id="editRentTime" method="POST" action="{{ route('car.editRentTime', $car->id) }}">
                      @csrf
                      <div class="d-flex justify-content-between">
                        <div class="number-dor">
                          <h2 class="form-production-date__title">
                            {{trans('profile.edit.45')}}
                          </h2>
                          <input type="number" class="login-form__input" name="min_rent_time" placeholder="{{trans('profile.edit.89')}}" value="{{$car->rentTime['min_rent_time'] / 86400}}"  required>
                        </div>
                        <div class="number-dor">
                          <h2 class="form-production-date__title">
                            {{trans('profile.edit.46')}}
                          </h2>
                          <input type="number" class="login-form__input" name="max_rent_time" placeholder="{{trans('profile.edit.89')}}" value="{{$car->rentTime['max_rent_time'] / 86400}}"  required>
                        </div>
                        <button type="submit" class="btn card-list__btn">
                          {{trans('profile.edit.47')}}
                        </button>
                      </div>
                  </form>
                </div>
              </div>







              <!-- MAP -->
            <!-- <div class="row">
                <div class="col-sm-12">
                  <form id="editCoordinates" method="POST" action="{{ route('car.editCoordinates',$car->id) }}">
                    @csrf
                    <div class="d-flex justify-content-between">
                      <div class="form-production-date">
                        <h2 class="form-production-date__title">
                          {{trans('profile.edit.48')}}
                        </h2>
                        <input type="hidden" id="car_lat" name="lat" value="{{$car->lat}}">
                        <input type="hidden" id="car_lng" name="lng" value="{{$car->lng}}">
                      </div>
                      <button type="submit" class="btn card-list__btn">
                        {{trans('profile.edit.49')}}
                      </button>
                    </div>
                  </form>
                </div>
                <div class="col-sm-12" id="map" style="height: 300px">
                </div>
              </div> -->
            </div>

<!-- PHOTO -->

            <div class="tab-pane fade show" id="home" role="tabpanel" aria-labelledby="home-tab">
              <ul class="about-car__list">
                  @foreach ($images as $image)
                  @if ($image->avatar == 1)
                    <li class="hover">
                      <!-- https://hb.bizmrg.com/soautomedia/{{$image->name}} -->
                      <img src="https://s3-us-west-2.amazonaws.com/tourismchilliwack/wp-content/uploads/2017/04/14063401/headshot-placeholder.jpg
                      " alt="">
                      
                      <form method="POST" action="{{ route('profile.deleteImage', $car->id) }}" class="delete deletePhoto">
                        @csrf
                        <input type="hidden" name="image_id" value="{{$image->id}}">
                        <button type="submit">
                            <img src="/image/delete.svg" alt="">
                        </button>
                      </form>
                      {{-- <form method="POST" action="{{ route('profile.setHead', $car->id) }}" class="primary saveHeadImageId">
                        @csrf
                        <input type="hidden" name="car_id" value="{{$car->id}}">
                        <input type="hidden" name="image_id" value="{{$image->id}}">
                        <button type="submit">{{trans('profile.edit.50')}}</button>
                      </form> --}}
                    </li>
                    @break
                    @endif
                  @endforeach
      
                  @foreach ($images as $image)
                    @if ($image->avatar == 1)
                      @continue
                    @endif
                    <li>
                      <!-- https://hb.bizmrg.com/soautomedia/{{$image->name}} -->
                      <img src="https://s3-us-west-2.amazonaws.com/tourismchilliwack/wp-content/uploads/2017/04/14063401/headshot-placeholder.jpg
                      " alt="">
                      <form method="POST" action="{{ route('profile.deleteImage', $car->id) }}" class="delete deletePhoto">
                        @csrf
                        <input type="hidden" name="image_id" value="{{$image->id}}">
                        <button type="submit">
                            <img src="/image/delete.svg" alt="">
                        </button>
                      </form>
                      <form method="POST" action="{{ route('profile.setHead', $car->id) }}" class="primary saveHeadImageId">
                        @csrf
                        <input type="hidden" name="car_id" value="{{$car->id}}">
                        <input type="hidden" name="image_id" value="{{$image->id}}">
                        <button type="submit">{{trans('profile.edit.51')}}</button>
                      </form>
                    </li>
                  
                @endforeach
                
              </ul>
      
              <input type="file" id="editPhoto" name="photo" multiple>
            </div>

<!-- LOCATION ??? -->

            <div class="tab-pane fade conditionDelivery" id="profile" role="tabpanel" aria-labelledby="profile-tab">
              @foreach ($delivery_cars as $item)
              <div class="card card-default active">
                <div class="card-header">
                  <h2>{{trans('profile.edit.52')}}</h2>
                </div>
                <div class="card-body">
                  <form method="POST" action="{{ route('car.editConditionDelivery', $car->id) }}" class="editConditionsDelivery d-flex justify-content-between">
                    @csrf
                    <input type="hidden" name="car_id" value="{{$car->id}}">
                    <input type="hidden" class="data_id" name="data_id" value="{{$item->id}}">
                    <div class="form-production-date">
                      <h2 class="form-production-date__title">
                        {{trans('profile.edit.53')}}
                      </h2>
                      <select name="place" class="form-number-persons__select" required>
                        <option class="form-number-persons__option" value="" selected disabled>
                          {{trans('profile.edit.54')}}
                        </option>
                        @foreach ($rent_place as $place)
                        <option class="form-number-persons__option" value="{{$place->id}}"
                          @if ($item->place == $place->id)
                          selected
                          @endif
                          >
                          {{$place->name}}
                        </option>
                        @endforeach
                        
                      </select>
                    </div>
                    <div class="form-production-date">
                      <h2 class="form-production-date__title">
                        {{trans('profile.edit.55')}}
                      </h2>
                      <input type="number" class="login-form__input" name="cost" placeholder="{{trans('profile.edit.90')}}" required value="{{$item->cost}}">
                    </div>
                    
                    <button type="submit" class="btn card-list__btn">
                      {{trans('profile.edit.56')}}
                    </button>
                  </form>
                </div>
              </div>
              @endforeach
              <div class="card card-default">
                <div class="card-header">
                  <h2>{{trans('profile.edit.57')}}</h2>
                </div>
                <div class="card-body">
                  <form method="POST" action="{{ route('car.editConditionDelivery', $car->id) }}" class="editConditionsDelivery d-flex justify-content-between">
                    @csrf
                    <input type="hidden" name="car_id" value="{{$car->id}}">
                    <input type="hidden" class="data_id" name="data_id" value="0">
                    <div class="form-production-date">
                      <h2 class="form-production-date__title">
                        {{trans('profile.edit.58')}}
                      </h2>
                      <div class="response_place">
                      </div>
                      <select name="place" class="form-number-persons__select" required>
                        <option class="form-number-persons__option" value="" selected disabled>
                          {{trans('profile.edit.59')}}
                        </option>
                        @foreach ($rent_place as $place)
                        <option class="form-number-persons__option" value="{{$place->id}}">
                          {{$place->name}}
                        </option>
                        @endforeach
                        
                      </select>
                    </div>
                    <div class="form-production-date">
                      <h2 class="form-production-date__title">
                        {{trans('profile.edit.60')}}
                      </h2>
                      <div class="response_cost">
                      </div>
                      <input type="number" class="login-form__input" name="cost" placeholder="{{trans('profile.edit.90')}}" required>
                    </div>
                    
                    <button type="submit" class="btn card-list__btn">
                      {{trans('profile.edit.61')}}
                    </button>
                    
                  </form>
                </div>
              </div>
             
            </div>

<!-- DEPOSIT -->

            <div class="tab-pane fade" id="deposit" role="tabpanel" aria-labelledby="deposit-tab">
              <div class="card card-default">
                <div class="card-header">
                  <h2>{{trans('profile.edit.62')}}</h2>
                </div>
                <div class="card-body">
                  <form method="POST" action="{{ route('car.editDeposit', $car->id) }}" id="editDeposit" class="d-flex justify-content-between">
                    @csrf
                    <div class="form-production-date">
                      <h2 class="form-production-date__title">
                        {{trans('profile.edit.63')}}
                      </h2>
                      <div class="response_cost">
                      </div>
                      <input type="number" class="login-form__input" name="price" value="{{ $car->carsDeposit->price ?? 0 }}" placeholder="{{trans('profile.edit.90')}}" required>
                    </div>

                    <button type="submit" class="btn card-list__btn">
                      {{trans('profile.edit.64')}}
                    </button>

                  </form>
                </div>
              </div>

            </div>

<!-- CONNECT -->

            <div class="tab-pane fade" id="type" role="tabpanel" aria-labelledby="type-tab">
              <div class="card card-default">
                <div class="card-header">
                  <h2>{{trans('profile.edit.57')}}</h2>
                </div>
                <div class="card-body">
                  <form id="editType" class="d-flex justify-content-between align-items-center" method="POST" action="{{ route('car.type', $car->id) }}">
                    @csrf
                    <div class="group-ridio" id="connection_type">
                      @foreach ($unlock_type as $type)
                          <input name="type" class="form-check-label" type="radio" value="{{ $type->id }}"
                          @if($type->check) checked @endif
                          id="{{ $type->id }}">
                          <label for="{{ $type->id }}">{{ $type->name }}</label>
                        @endforeach
                    </div>
                    <button type="submit" class="btn card-list__btn">
                      {{trans('profile.edit.65')}}
                    </button>
                  </form>
                </div>
              </div>
            </div>

<!-- EXTRA SERVICES -->

            <div class="tab-pane fade" id="services" role="tabpanel" aria-labelledby="services-tab">

              <div class="card card-default">
                <div class="card-header">
                  <h2>{{trans('profile.edit.57')}}</h2>
                </div>
                <div class="card-body">

                  <div class="table">
                      <div class="tr">
                        <div class="table_tr">{{trans('profile.edit.66')}}</div>
                        <div class="table_tr">{{trans('profile.edit.67')}}</div>
                        <div class="table_tr">{{trans('profile.edit.68')}}</div>
                        <div class="table_tr"></div>
                      </div>
                      @foreach ($extra as $item)
                      
                      <form method="POST" id="editExtra-{{$item->id}}" class="tr editExtra" action="{{ route('car.extra') }}">
                        @csrf
                        @php
                         $car_extra = null;   
                        @endphp
                        @php
                          foreach ($extra_cars as $item_cars) {
                            if ($item->id == $item_cars['extra_id']) {
                              $car_extra = $item_cars;
                            }
                          }
                        @endphp
                        


                        
                        <input type="hidden" name="active" value="1">
                        <input type="hidden" name="cars_id" value="{{$car->id}}">
                        <input type="hidden" name="extra_id" value="{{$item->id}}">
                        <input type="hidden" name="extra_cars" value="{{$car_extra['id']}}">

                        <div class="th" scope="row">{{$item->name}}</div>
                        
                          <div class="td">
                            <select name="extra_time_id" class="form-number-persons__select" required>
                              <option class="form-number-persons__option" value="">
                                {{trans('profile.edit.69')}}
                              </option>
                              @foreach($extra_time as $item_time)
                                
                                <option value="{{$item_time->id}}" 
                                    @if ($item_time->id == $car_extra['extra_time_id'])
                                        selected
                                    @endif
                                > 
                                  {{$item_time->name}}
                                </option>
                              @endforeach
                            </select>
                          </div>
                          <div class="td">
                            <input type="text" class="login-form__input" name="extra_cost" placeholder="{{trans('profile.edit.90')}}" required value="{{$car_extra['extra_cost']}}">
                          </div>
                        <div class="td">
                          @if ($car_extra['active'])
                            <button type="submit" class="btn card-list__btn">
                              {{trans('profile.edit.70')}}
                            </button>
                          @else
                            <button type="submit" class="btn card-list__btn">
                              {{trans('profile.edit.71')}}
                            </button>
                          @endif
                          
                        </div>
                      </form>
                      @endforeach
                  </div>
                </div>
              </div>
            </div>



<!-- COURSES -->

            <div class="tab-pane fade" id="courses" role="tabpanel" aria-labelledby="courses-tab">

              <div class="card card-default">
                <div class="card-header">
                  <h2>{{trans('profile.edit.57')}}</h2>
                </div>
                <div class="card-body">

                  <div class="table">
                      <div class="tr">
                        <div class="table_tr">{{trans('profile.edit.66')}}</div>
                        <div class="table_tr">{{trans('profile.edit.67')}}</div>
                        <div class="table_tr">{{trans('profile.edit.68')}}</div>
                        <div class="table_tr"></div>
                      </div>
                      @foreach ($course as $item)
                      
                      <form method="POST" id="editcourse-{{$item->id}}" class="tr editcourse" action="{{ route('car.course') }}">
                        @csrf
                        @php
                         $car_course = null;   
                        @endphp
                        @php
                          foreach ($courses_cars as $item_cars) {
                            if ($item->id == $item_cars['course_id']) {
                              $car_course = $item_cars;
                            }
                          }
                        @endphp
                        


                        
                        <input type="hidden" name="active" value="1">
                        <input type="hidden" name="cars_id" value="{{$car->id}}">
                        <input type="hidden" name="course_id" value="{{$item->id}}">
                        <input type="hidden" name="courses_cars" value="{{$car_course['id']}}">

                        <div class="th" scope="row">{{$item->name}}</div>
                        
                          <div class="td">
                            <select name="course_time_id" class="form-number-persons__select" required>
                              <option class="form-number-persons__option" value="">
                                {{trans('profile.edit.69')}}
                              </option>
                              @foreach($course_time as $item_time)
                                
                                <option value="{{$item_time->id}}" 
                                    @if ($item_time->id == $car_course['course_time_id'])
                                        selected
                                    @endif
                                > 
                                  {{$item_time->name}}
                                </option>
                              @endforeach
                            </select>
                          </div>
                          <div class="td">
                            <input type="text" class="login-form__input" name="course_cost" placeholder="{{trans('profile.edit.90')}}" required value="{{$car_course['course_cost']}}">
                          </div>
                        <div class="td">
                          @if ($car_course['active'])
                            <button type="submit" class="btn card-list__btn">
                              {{trans('profile.edit.70')}}
                            </button>
                          @else
                            <button type="submit" class="btn card-list__btn">
                              {{trans('profile.edit.71')}}
                            </button>
                          @endif
                          
                        </div>
                      </form>
                      @endforeach
                  </div>
                </div>
              </div>
            </div>

<!-- INSURANCE -->

            <div class="tab-pane fade" id="insurance" role="tabpanel" aria-labelledby="insurance-tab">
              <div class="card card-default">
                <div class="card-header">
                  <h2>{{trans('profile.edit.73')}}</h2>
                </div>
                <div class="card-body">
                  <ul class="card-list">
                    <li class="card__osago card__osago_id card-list__item d-flex">
              
                      <h2 class="changing-card-title">{{trans('profile.edit.73')}}</h2>
                      <form method="POST" action="{{route('car.editOsagoKaskoDate', $car->id)}}" class="editOsagoKaskoDate flex w-100 mb-4 align-items-center">
                        @csrf
                        <input type="date" name="osago_date" class="input" value="{{$car->osago_date}}">
                        <button class="btn card-list__btn ml-4"  type="submit">{{trans('profile.edit.74')}}</button>
                      </form>
                      <h2 class="changing-card-title">{{trans('profile.edit.75')}}</h2>
                      @if ($osagoImage)
                        <img src="{{$osagoImage}}" alt="" style="height: 300px">
                      @else
                        <div class="card__osago__block">
                          <div class="number-dor">
                            <h2 class="number-dor__title">
                              {{trans('profile.edit.76')}}
                            </h2>
                            <div class="group-ridio">
                              <form method="POST" action="{{route('car.osago', $car->id)}}">
                                @csrf
                                <input type="radio" @if(!$car->osago_has) checked @endif id="osago_has_1" name="osago_has" value="0" required="">
                                <label for="osago_has_1">{{trans('profile.edit.77')}}</label>
                                <input type="radio" @if($car->osago_has) checked @endif  id="osago_has_2" name="osago_has" value="1">
                                <label for="osago_has_2">{{trans('profile.edit.78')}}</label>
                                <button type="submit" class="btn card-list__btn d-none">
                                  {{trans('profile.edit.79')}}
                                </button>
                              </form>
                            </div>
                          </div>
                        </div>
                        <div class="card__osago__block osago__need" style="opacity: 0">
                          <div class="card-list__Upload d-none">
                            <input type="file" id="osago" name="photo">
                          </div>
                        </div>
                      @endif
                    </li>
                  </ul>
                </div>
              </div>

              <div class="card card-default">
                <div class="card-header">
                  <h2>{{trans('profile.edit.80')}}</h2>
                </div>
                <div class="card-body">
                  <ul class="card-list">
                    <li class="card__kasko card__osago card-list__item d-flex">
                      <h2 class="changing-card-title">{{trans('profile.edit.80')}} </h2>
                      <form method="POST" action="{{route('car.editOsagoKaskoDate', $car->id)}}" class="editOsagoKaskoDate flex w-100 mb-4 align-items-center">
                        @csrf
                        <input type="date" name="kasko_date" class="input" value="{{$car->kasko_date}}">
                        <button class="btn card-list__btn ml-4"  type="submit">{{trans('profile.edit.81')}}</button>
                      </form>
                      <h2 class="changing-card-title">{{trans('profile.edit.82')}}</h2>
                      @if ($kaskoImage)
                        <img src="{{$kaskoImage}}" alt="" style="height: 300px">
                      @else
                        <div class="card__osago__block">
                          <div class="number-dor">
                            <h2 class="number-dor__title">
                              {{trans('profile.edit.83')}}
                            </h2>
                            <div class="group-ridio">
                              <form method="POST" action="{{route('car.kasko', $car->id)}}">
                                @csrf
                                <input type="radio" id="kasko_has_1" @if(!$car->kasko_has) checked @endif name="kasko_has" value="1">
                                <label for="kasko_has_1">{{trans('profile.edit.84')}}</label>
                                <input type="radio" id="kasko_has_2" @if($car->kasko_has) checked @endif name="kasko_has" value="0">
                                <label for="kasko_has_2">{{trans('profile.edit.85')}}</label>
                                <button type="submit" class="btn card-list__btn d-none">
                                  
                                </button>
                              </form>
                            </div>
                          </div>
                        </div>
                        <div class="card__osago__block kasko__need" style="opacity: 0">
                          <div class="card-list__Upload">
                            <input type="file" id="kasko" name="photo">
                          </div>
                        </div>
                      @endif
                    </li>
                  </ul>
                </div>
              </div>


            </div>

<!-- BANS -->

            <div class="tab-pane fade" id="ban" role="tabpanel" aria-labelledby="ban-tab">

               <div class="card card-default">
                <div class="card-header">
                  <h2>{{trans('profile.edit.57')}}</h2>
                </div>
                <div class="card-body">

                  <form id="editBan" class="ban" method="POST" action="{{ route('car.ban') }}">
                    @csrf
                    <input type="hidden" name="cars_id" value="{{ $car->id }}">

                    @foreach ($cars_bans as $item)
                      <div class="checkbox">
                        <input id="{{$item->id}}" type="checkbox" {{ $item->active ?? 'checked' }} name="{{$item->id}}" value="1" class="check">
                        <label for="{{$item->id}}">
                          {{$item->bans['name']}}
                        </label>
                      </div>     
                    @endforeach
                    <button class="btn card-list__btn" type="submit">{{trans('profile.edit.86')}}</button>
                  </form>
                </div>
              </div>
            </div>

          </div>
        
        
      </div>
    </div>
  </div>

</section>

<script>
  var car_id = parseInt("{{$car->id}}");
</script>


@endsection

@section('bottom_scripts')
  <script>
    let map, center, marker;
    let $carLat = $('#car_lat');
    let $carLng = $('#car_lng');
    center = {lat: {{$lat}}, lng: {{$lng}} };
    function initMap() {
      map = new google.maps.Map(document.getElementById('map'), {
        center: center,
        zoom: 18
      });
      marker = new google.maps.Marker({position: center, map: map, draggable: true});
      marker.addListener('dragend', (event) => {
        $carLat.val(event.latLng.lat());
        $carLng.val(event.latLng.lng());
      });
      @if($empty)
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
          console.log(position);
          var pos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
          };

          $carLat.val(position.coords.latitude);
          $carLng.val(position.coords.longitude);

          map.setCenter(pos);
          var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

          marker.setPosition(latlng);
          console.log('your address  found')
        }, function () {
          console.log('your address not found')
        });
      } else {
        console.log('you dont navigator')
      }
      @endif
    }

  </script>
  <script async defer
          src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCQP9BGaoXYrBjGiDpzMd_wxEkX-CyLA-o&callback=initMap">
  </script>
@endsection