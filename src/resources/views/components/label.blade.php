<div class="form-group row">
    <label for="{{ $title }}" class="col-md-4 col-form-label text-md-right">{{ $title }}</label>
    <label for="{{ $title }}" class="col-md-6 col-form-label text-md-left">{{ $slot }}</label>
</div>
