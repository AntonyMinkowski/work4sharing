@extends('layouts.home')
@section('body_class', 'page')

@section('content')
	<div class="error-404">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="heading">
						<h2 class="text-left">К сожалению, указанная страница не найдена.</h2>
					</div>
					<p>ВЕРНИТЕСЬ НА ГЛАВНУЮ СТРАНИЦУ И ПОПРОБУЙТЕ ЕЩЕ РАЗ</p>
					<div class="d-flex justify-content-center">
						<a href="/" class="btn btn-primary btn-purple">на главную</a>
					</div>
				
				</div>
			</div>
		</div>
	</div>
@endsection
