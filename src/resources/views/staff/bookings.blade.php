@extends('layouts.app')

@section('content')
<link rel="stylesheet" href="https://unpkg.com/bootstrap-table@1.14.2/dist/bootstrap-table.min.css">

<div class="container-fluid">
    @include('staff.parts.navbar',['route' => $route])
    <div class="row">
        <div class="col-sm-12">
            <table id="table" class="table table-striped"
                    data-toggle="table"
                    data-search="true"
                    data-filter-control="true">
                <thead>
                    <tr>
                        <th data-field="id" data-filter-control="input" data-sortable="true">#</th>
                        <th data-field="from" data-filter-control="input" data-sortable="true">От</th>
                        <th data-field="to" data-filter-control="input" data-sortable="true">До</th>
                        <th data-field="brand_model" data-filter-control="input" data-sortable="true">Марка, модель</th>
                        <th data-field="tenant_name" data-filter-control="input" data-sortable="true">Имя арендатора</th>
                        <th data-field="tenant_phone" data-filter-control="input" data-sortable="true">Телефон арендатора</th>
                        <th data-field="tenant_email" data-filter-control="input" data-sortable="true">Email арендатора</th>
                        <th data-field="owner_name" data-filter-control="input" data-sortable="true">Имя владельца</th>
                        <th data-field="owner_phone" data-filter-control="input" data-sortable="true">Телефон владельца</th>
                        <th data-field="owner_email" data-filter-control="input" data-sortable="true">Email владельца</th>
                        <th data-field="status_name" data-filter-control="input" data-sortable="true">Статус брони</th>
                    </tr>
                </thead>
                <tbody>
                    @isset($bookings)
                        @foreach ($bookings as $item)
                        <tr>
                            <td>{!! $item->id !!}</td>
                            <td>{!! $item->datefrom !!}</td>
                            <td>{!! $item->dateto !!}</td>
                            <td>{!! $item->brand_name !!} {!! $item->model_name !!}</td>
                            <td>{!! $item->tenant_name !!}</td>
                            <td>{!! $item->tenant_phone !!}</td>
                            <td>{!! $item->tenant_email !!}</td>
                            <td>{!! $item->owner_name !!}</td>
                            <td>{!! $item->owner_phone !!}</td>
                            <td>{!! $item->owner_email !!}</td>
                            <td>{!! $item->status_name !!}</td>
                        </tr>
                        @endforeach
                    @endisset
                </tbody>
            </table>
        </div>
    </div>
</div>

<script type="text/javascript" src="https://unpkg.com/bootstrap-table@1.14.2/dist/bootstrap-table.min.js"></script>
@endsection