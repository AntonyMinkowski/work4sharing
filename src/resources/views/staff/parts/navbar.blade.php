<div class="row">
    <div class="col-sm-12">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            @foreach ($route as $item)
                <li class="nav-item">
                    <a class="nav-link" href="{{$item->route}}" role="tab"> {{$item->name}} </a>
                </li>
            @endforeach
        </ul>
    </div>
</div>