$('body').on('submit','#passport',function(e){
  e.preventDefault();
  const form = $(this);
  const loading = $('.loading');
  loading.addClass('active')
  $.ajax({
    type: form.attr('method'),
    url: form.attr('action'),
    data: new FormData(form[0]),
    dataType: 'json',
    enctype: 'multipart/form-data',
    processData: false,
    contentType: false,
  }).done(function(res) {
    setTimeout(function(){
      loading.removeClass('active')
    }, 1000)
    $('#checkout-lists').load(` #checkout-lists > *`);
    ym(53583991, 'reachGoal', 'passport-save');
  }).fail(function(err) {
    alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
    setTimeout(function(){
      loading.removeClass('active')
    }, 1000)
  });
});

$('body').on('submit','#driving',function(e){
  e.preventDefault();
  const form = $(this);
  const loading = $('.loading');
  loading.addClass('active')
  $.ajax({
    type: form.attr('method'),
    url: form.attr('action'),
    data: new FormData(form[0]),
    dataType: 'json',
    enctype: 'multipart/form-data',
    processData: false,
    contentType: false,
  }).done(function(res) {
    setTimeout(function(){
      loading.removeClass('active')
    }, 1000)
    $('#checkout-lists').load(` #checkout-lists > *`);
    ym(53583991, 'reachGoal', 'vu-save');
  }).fail(function(err) {
    alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
    setTimeout(function(){
      loading.removeClass('active')
    }, 1000)
  });
});

$('body').on('submit','#selfie-photo',function(e){
  e.preventDefault();
  const form = $(this);
  const loading = $('.loading');
  loading.addClass('active')
  $.ajax({
    type: form.attr('method'),
    url: form.attr('action'),
    data: new FormData(form[0]),
    dataType: 'json',
    enctype: 'multipart/form-data',
    processData: false,
    contentType: false,
  }).done(function(res) {
    setTimeout(function(){
      loading.removeClass('active')
    }, 1000);
    $('#checkout-lists').load(` #checkout-lists > *`)
    $("html, body").animate({ scrollTop: 0 }, "slow");
    ym(53583991, 'reachGoal', 'selfi-save');
  }).fail(function(err) {
    alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
    setTimeout(function(){
      loading.removeClass('active')
    }, 1000)
  });
});