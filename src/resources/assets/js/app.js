/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
Array.prototype.remove = function() {
  var what, a = arguments, L = a.length, ax;
  while (L && this.length) {
      what = a[--L];
      while ((ax = this.indexOf(what)) !== -1) {
          this.splice(ax, 1);
      }
  }
  return this;
};

// $('.phone').mask('+7 (999) 999-99-99');
$('.seria').mask('9999');
$('.nomer').mask('999999');




import './main';

import './components/edit-auto'
import './components/create-auto'
import './components/catalog'
import './components/checkout'
import './components/card-booking'
import './components/verify'

import Vue from 'vue';

import Chat from './components/Chat.vue';
import Catalog from './vue/catalog/CatalogComponent';
import Car from './vue/catalog/CarComponent'
import UserPublic from './vue/user/UserPublicComponent'
import { PopoverPlugin } from 'bootstrap-vue'
import {store} from './vue/store/index'
import {VBModal} from 'bootstrap-vue'

Vue.directive('b-modal', VBModal)
Vue.use(PopoverPlugin)


Vue.prototype.trans = (key) => {
    return _.get(window.trans, key, key);
};


if (document.getElementById('chat-dialogues')) {

    const chat = new Vue({
        el: '#chat-dialogues',
        components: {
            'chat-dialogues': Chat
        }
    });

}

if (document.getElementById('catalog')) {
    const catalog = new Vue({
        el: '#catalog',
        store,
        components: {
            'catalog': Catalog
        }
    });
}
if (document.getElementById('car')) {
    const car = new Vue({
        el: '#car',
        store,
        components: {
            'car': Car
        }
    });
}

if (document.getElementById('user-public')) {
    const car = new Vue({
        el: '#user-public',
        store,
        components: {
            'userPublic': UserPublic
        }
    });
}

if ($('#modal-trip').length) {
    $('#modal-trip').modal('show');
}