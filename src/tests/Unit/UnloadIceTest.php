<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class UnloadIceest extends TestCase
{
    use WithoutMiddleware, RefreshDatabase;

    public function setUp()
    {
        parent::setUp();

        $user = factory(\App\User::class)->make([
            'id' => 1,
            'name' => 'Test',
            'email' => 'test@test.tu'
        ]);

        factory(\App\Models\Car::class)->create();

        Storage::fake('hot');
        Storage::fake('ice');

        Hash::shouldReceive('make')
            ->once()
            ->andReturn('value');

        Auth::shouldReceive('user')
            ->once()
            ->andReturn($user);
    }

    /**
     * Тест загрузки фото авто
     *
     * @return void
     */
    public function testSaveHotBacket()
    {
        $this->assertDatabaseMissing('sts_images', ['name' => 'value']);

        $response = $this->json(
            'POST',
            '/api/cars/add/doc',
            [
                'CarId' => 1,
                'photo' => UploadedFile::fake()->image('test.jpg')
            ]
        );

        $response->assertStatus(200);
        $response->assertJson(['carId' => 1, 'result' => true]);

        $this->assertDatabaseHas('sts_images', ['name' => 'value']);

        Storage::disk('ice')->assertExists('value');
        Storage::disk('hot')->assertMissing('value');
    }

    public function tearDown()
    {
        parent::tearDown();
    }
}
