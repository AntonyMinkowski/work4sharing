<?php

namespace Tests\Unit;

use Tests\TestCase;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\ClientInterface;
use Illuminate\Support\Facades\App;
use App\Services\SMSService\AbstractSms;
use App\Services\SMSService\SmsCClient;
use App\Services\SMSService\SmsCService;
use App\Services\SMSService\SmsCService\InterfaceSmsService;
use Psr\Http\Message\StreamInterface;

class SmsCClientTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        $body = $this->createMock(StreamInterface::class);
        $body->method('__toString')
             ->willReturn('{"id": 1, "cnt": 1}');
        $mock = new MockHandler([new Response(
                200,
                [],
                $body
        )]);
        $client = new Client(['handler' => $mock]);

        $this->app->singleton(SmsCClient::class, function() use ($client) {
            return new SmsCClient(
                $client,
                $this->app['config']->get('sms.services.smsc')
            );
        });

        $sms = $this->createMock(AbstractSms::class);
        $sms->method('getDataArray')
             ->willReturn(['phones' => 84464726304, 'mes' => 'Hello']);
        $this->sms = $sms;
    }

    public function testResolve()
    {
        $this->assertInstanceOf(SmsCClient::class, resolve(SmsCClient::class));
        $this->assertClassHasAttribute('client', SmsCClient::class);
        $this->assertAttributeInstanceOf(
            ClientInterface::class,
            'client',
            resolve(SmsCClient::class),
            sprintf(
                'базовый клиент должен поддерживать %s интерфейс',
                ClientInterface::class
            )
        );
    }

    public function testResponseSendSms()
    {
        /** @var SmsCClient $client */
        $client = resolve(SmsCClient::class);
        $this->assertInstanceOf(
            Response::class,
            $client->send($this->sms),
            'метод должен возвращать объект ' . Response::class
        );
    }

    public function testSendSms()
    {
        $base_client = $this->createMock(ClientInterface::class);
        $base_client->expects($this->once())
            ->method('request')
            ->willReturn(true);
        /** @var SmsCClient $client */
        $client = new SmsCClient(
            $base_client,
            $this->app['config']->get('sms.services.smsc')
        );

        $this->assertTrue(
            $client->send($this->sms),
            'метод должен вызвать метод request 1 раз'
        );
    }

    public function tearDown()
    {
        parent::tearDown();
    }
}
